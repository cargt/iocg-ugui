/*
 * task_webserver.c
 *
 *  Created on: Jan 8, 2018
 *      Author: mark
 */


/*
 * task_webserver.c
 *
 *  Created on: Nov 15, 2017
 *      Author: cargt
 */
#include "sdkconfig.h"

#include "string.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/timers.h"
#include "esp_log.h"

#include "task_network.h"

#include <libesphttpd/esp.h>
#include "libesphttpd/httpd.h"
#include "libesphttpd/httpdespfs.h"
#include "cgi.h"
#include "cgiwifi_esp32.h"
#include "cgistatus.h"
#include "libesphttpd/cgiflash.h"
#include "libesphttpd/auth.h"
#include "libesphttpd/espfs.h"
#include "libesphttpd/captdns.h"
#include "libesphttpd/webpages-espfs.h"
#include "libesphttpd/cgiwebsocket.h"
#include "cgi-test.h"

#include "cJSON.h"

#define delay(ms) (vTaskDelay(ms/portTICK_RATE_MS))

#define TASK_WEBSERVER_PRIO                11U
#define TASK_WEBSERVER_STACK_SIZE          3000U

#define TAG "WEBSERVER_TASK"

const int CONNECTED_BIT = BIT0;

#define OTA_FLASH_SIZE_K 1024
#define OTA_TAGNAME "generic"

CgiUploadFlashDef uploadParams={
	.type=CGIFLASH_TYPE_FW,
	.fw1Pos=0x1000,
	.fw2Pos=((OTA_FLASH_SIZE_K*1024)/2)+0x1000,
	.fwSize=((OTA_FLASH_SIZE_K*1024)/2)-0x1000,
	.tagName=OTA_TAGNAME
};


//=================================
// Private function prototypes
//=================================
static void websocketBcast();
static void myWebsocketRecv(Websock *ws, char *data, int len, int flags);
static void myWebsocketConnect(Websock *ws);
static void myEchoWebsocketRecv(Websock *ws, char *data, int len, int flags);
static void myEchoWebsocketConnect(Websock *ws);
static void myWebsocketScreenConnect(Websock *ws);
static void myWebsocketScreenRecv(Websock *ws, char *data, int len, int flags);
/*
This is the main url->function dispatching data struct.
In short, it's a struct with various URLs plus their handlers. The handlers can
be 'standard' CGI functions you wrote, or 'special' CGIs requiring an argument.
They can also be auth-functions. An asterisk will match any url starting with
everything before the asterisks; "*" matches everything. The list will be
handled top-down, so make sure to put more specific rules above the more
general ones. Authorization things (like authBasic) act as a 'barrier' and
should be placed above the URLs they protect.
*/
static const HttpdBuiltInUrl builtInUrls[]={
	ROUTE_CGI_ARG("*", cgiRedirectApClientToHostname, "esp8266.nonet"),
	ROUTE_REDIRECT("/", "/index.html"),
	ROUTE_TPL("/index.html", tplCounter),
	ROUTE_REDIRECT("/flash", "/flash/index.html"),
	ROUTE_REDIRECT("/flash/", "/flash/index.html"),
	ROUTE_CGI_ARG("/flash/next", cgiGetFirmwareNext, &uploadParams),
	ROUTE_CGI_ARG("/flash/upload", cgiUploadFirmware, &uploadParams),
	ROUTE_CGI("/flash/reboot", cgiRebootFirmware),

	//Routines to make the /wifi URL and everything beneath it work.
//Enable the line below to protect the WiFi configuration with an username/password combo.
//	{"/wifi/*", authBasic, myPassFn},

	ROUTE_REDIRECT("/wifi", "/wifi/wifi.html"),
	ROUTE_REDIRECT("/wifi/", "/wifi/wifi.html"),
	ROUTE_CGI("/wifi/wifiscan.cgi", cgiWiFiScanEsp32),
	ROUTE_TPL("/wifi/wifi.html", tplWlanEsp32),
	ROUTE_CGI("/wifi/connect.cgi", cgiWiFiConnectEsp32),
	ROUTE_CGI("/wifi/connstatus.cgi", cgiWiFiConnStatusEsp32),
	ROUTE_CGI("/wifi/setmode.cgi", cgiWiFiSetModeEsp32),

	ROUTE_REDIRECT("/websocket", "/websocket/index.html"),
	ROUTE_WS("/websocket/ws.cgi", myWebsocketConnect),
	ROUTE_WS("/websocket/wsscreen.cgi", myWebsocketScreenConnect),
	ROUTE_WS("/websocket/echo.cgi", myEchoWebsocketConnect),

    ROUTE_REDIRECT("/status", "/status/status.html"),
    ROUTE_REDIRECT("/status/", "/status/status.html"),
    ROUTE_CGI("/status/getstatus.cgi", cgiGetCANStatus),
    ROUTE_CGI("/status/getmessages.cgi", cgiGetCANMessages),

	ROUTE_REDIRECT("/test", "/test/index.html"),
	ROUTE_REDIRECT("/test", "/test/index.html"),
	ROUTE_CGI("/test/test.cgi", cgiTestbed),

	ROUTE_FILESYSTEM(),

	ROUTE_END()
};


//=================================
// Public functions
//=================================
int task_webserver_init(void)
{
    int ret = pdPASS;
    espFsInit((void*)(webpages_espfs_start));
    httpdInit(builtInUrls, 80, HTTPD_FLAG_NONE);

//    if (xTaskCreate(websocketBcast,             /* pointer to the task                      */
//                    "wsbcast",          /* task name for kernel awareness debugging */
//                    TASK_WEBSERVER_STACK_SIZE, /* task stack size                          */
//                    NULL,                       /* optional task startup argument           */
//                    TASK_WEBSERVER_PRIO,       /* initial priority                         */
//                    NULL                        /* optional task handle to create           */
//                    ) != pdPASS)
//    {
//        assert(0);
//    }


    return ret;
}

void task_webserver_event_handler(void *ctx, system_event_t *event)
{
	//pass events along to cgi handler
	cgiWiFiEventHandlerEsp32(ctx, event);
}

//=================================
// Private functions
//=================================

//Function that tells the authentication system what users/passwords live on the system.
//This is disabled in the default build; if you want to try it, enable the authBasic line in
//the builtInUrls below.
int myPassFn(HttpdConnData *connData, int no, char *user, int userLen, char *pass, int passLen) {
	if (no==0) {
		strcpy(user, "admin");
		strcpy(pass, "s3cr3t");
		return 1;
//Add more users this way. Check against incrementing no for each user added.
//	} else if (no==1) {
//		strcpy(user, "user1");
//		strcpy(pass, "something");
//		return 1;
	}
	return 0;
}


//Broadcast the status every second over connected websockets
static void websocketBcast()
{
	static int ctr=0;
	char buff[128];
	while(1) {
		ctr++;
//		sprintf(buff, "Up for %d minutes %d seconds!\n", ctr/60, ctr%60);
		sprintf(buff, "{\n \"min\": \"%d\",\n \"sec\": \"%d\" }\n", ctr/60, ctr%60);
		cgiWebsockBroadcast("/websocket/ws.cgi", buff, strlen(buff), WEBSOCK_FLAG_NONE);
		vTaskDelay(1000/portTICK_RATE_MS);
	}
}

//On reception of a message, send "You sent: " plus whatever the other side sent
static void myWebsocketRecv(Websock *ws, char *data, int len, int flags) {
	int i;
	char buff[128];
	sprintf(buff, "You sent: ");
	for (i=0; i<len; i++) buff[i+10]=data[i];
	buff[i+10]=0;
	cgiWebsocketSend(ws, buff, strlen(buff), WEBSOCK_FLAG_NONE);
}

//Websocket connected. Install reception handler and send welcome message.
static void myWebsocketConnect(Websock *ws) {
	ws->recvCb=myWebsocketRecv;
//	cgiWebsocketSend(ws, "Hi, Websocket!", 14, WEBSOCK_FLAG_NONE);
}

//On reception of a message, echo it back verbatim
static void myEchoWebsocketRecv(Websock *ws, char *data, int len, int flags) {
	printf("EchoWs: echo, len=%d\n", len);
	cgiWebsocketSend(ws, data, len, flags);
}

//Echo websocket connected. Install reception handler.
static void myEchoWebsocketConnect(Websock *ws) {
	printf("EchoWs: connect\n");
	ws->recvCb=myEchoWebsocketRecv;
}


static Websock wsScreen;
static void myWebsocketScreenConnect(Websock *ws) {
	printf("myWebsocketScreenConnect: connect\n");
	wsScreen = *ws;
	ws->recvCb=myWebsocketScreenRecv;
}
//On reception of a message, echo it back verbatim
static void myWebsocketScreenRecv(Websock *ws, char *data, int len, int flags) {
	extern uint16_t* draw_buf;
	char* buf_ptr = (char*)draw_buf;
//	cgiWebsocketSend(ws, draw_buf, 320*240*2/16, WEBSOCK_FLAG_BIN);
#define SEND_SIZE 960
	for(int i=0; i<320*240*2/SEND_SIZE; i++)
	{
		if(i==0)
		{
			cgiWebsocketSend(ws, &buf_ptr[i*SEND_SIZE], SEND_SIZE, WEBSOCK_FLAG_BIN | WEBSOCK_FLAG_MORE);
		}
		else if(i < (320*240*2-SEND_SIZE)/SEND_SIZE)
		{
			cgiWebsocketSend(ws, &buf_ptr[i*SEND_SIZE], SEND_SIZE, WEBSOCK_FLAG_BIN | WEBSOCK_FLAG_CONT | WEBSOCK_FLAG_MORE);
		}
		else
		{
			cgiWebsocketSend(ws, &buf_ptr[i*SEND_SIZE], SEND_SIZE, WEBSOCK_FLAG_BIN | WEBSOCK_FLAG_CONT);
		}
	}




}


