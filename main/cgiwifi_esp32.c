
#include <libesphttpd/esp.h>
#include "libesphttpd/cgiwifi.h"

#include "esp_event_loop.h"
#include "esp_log.h"
#include "esp_wifi.h"
#include "wifi_scan.h"

#define TAG "CGI-WIFI"

//WiFi access point data
typedef struct {
	char ssid[32];
	char bssid[8];
	int channel;
	char rssi;
	char enc;
} ApData;

//Scan result
typedef struct {
	char scanInProgress; //if 1, don't access the underlying stuff from the webpage.
	ApData **apData;
	int noAps;
} ScanResultData;

//Static scan status storage.
static ScanResultData cgiWifiAps;

#define WIFI_IDLE		0
#define WIFI_BUSY 		1
#define WIFI_CONNECTED 	2
#define WIFI_GOT_IP 	3
#define WIFI_FAIL 		4
//Connection result var
static int connTryStatus=WIFI_IDLE;
//static os_timer_t resetTimer;

////Callback the code calls when a wlan ap scan is done. Basically stores the result in
////the cgiWifiAps struct.
void wifiScanDoneEsp32(void *ctx, system_event_t *event)
{

	httpd_printf("wifiScanDoneEsp32\n");
	uint8_t rssi_percentage;

	//Clear prev ap data if needed.
	if (cgiWifiAps.apData!=NULL)
	{
		int n;
		for (n=0; n<cgiWifiAps.noAps; n++)
		{
			free(cgiWifiAps.apData[n]);
		}
		free(cgiWifiAps.apData);
	}

	int8_t apCount = WS_isScanComplete();
	printf("Number of access points found: %d\n",apCount);

	//Allocate memory for access point data
	cgiWifiAps.apData=(ApData **)malloc(sizeof(ApData *)*apCount);
	if (cgiWifiAps.apData==NULL) {
		printf("Out of memory allocating apData\n");
		cgiWifiAps.scanInProgress=0;
		return;
	}
	cgiWifiAps.noAps=apCount;
	httpd_printf("Scan done: found %d APs\n", apCount);

	if (apCount == 0)
	{
		cgiWifiAps.scanInProgress=0;
		return ESP_OK;
	}
	int i;
	printf("======================================================================\n");
	printf("             SSID             |    RSSI    |           AUTH           \n");
	printf("======================================================================\n");
	for (i=0; i<apCount; i++)
	{
		wifi_ap_record_t* apData = WS_getNetworkInfo(i);
        if(NULL != apData )
        {
			char *authmode;
			switch(apData->authmode)
			{
				case WIFI_AUTH_OPEN:
					authmode = "WIFI_AUTH_OPEN";
					break;
				case WIFI_AUTH_WEP:
					authmode = "WIFI_AUTH_WEP";
					break;
				case WIFI_AUTH_WPA_PSK:
					authmode = "WIFI_AUTH_WPA_PSK";
					break;
				case WIFI_AUTH_WPA2_PSK:
					authmode = "WIFI_AUTH_WPA2_PSK";
					break;
				case WIFI_AUTH_WPA_WPA2_PSK:
					authmode = "WIFI_AUTH_WPA_WPA2_PSK";
					break;
				default:
					authmode = "Unknown";
					break;
			}
			printf("%26.26s    |    % 4d    |    %22.22s\n",apData->ssid, apData->rssi, authmode);

			//Save the ap data.
			cgiWifiAps.apData[i]=(ApData *)malloc(sizeof(ApData));
			if (cgiWifiAps.apData[i]==NULL) {
				httpd_printf("Can't allocate mem for ap buff.\n");
				cgiWifiAps.scanInProgress=0;
				return;
			}

			//calc rssi in percent for quality
	#define RSSI_MAX -50
	#define RSSI_MIN -100
			if(apData->rssi <= RSSI_MIN)
			{
				rssi_percentage = 0;
			}
			else if(apData->rssi >= RSSI_MAX)
			{
				rssi_percentage = 250;
			}
			else
			{
				rssi_percentage = 5 * ( apData->rssi + 100 );
			}

			cgiWifiAps.apData[i]->rssi		= rssi_percentage;
			cgiWifiAps.apData[i]->channel	= 0; //bss_link->channel;
			cgiWifiAps.apData[i]->enc		= apData->authmode != WIFI_AUTH_OPEN ? 1 : 0;
			strncpy(cgiWifiAps.apData[i]->ssid, (char*)apData->ssid, 32);
			strncpy(cgiWifiAps.apData[i]->bssid, (char*)apData->bssid, 6);
        }
	}

	//We're done.
	cgiWifiAps.scanInProgress=0;
}


//Routine to start a WiFi access point scan.
static void ICACHE_FLASH_ATTR wifiStartScan() {
//	int x;
	if (cgiWifiAps.scanInProgress) return;
	cgiWifiAps.scanInProgress=1;

	wifi_scan_config_t scanConf = {
	      .ssid = NULL,
	      .bssid = NULL,
	      .channel = 0,
	      .show_hidden = true,
		  .scan_type = WIFI_SCAN_TYPE_PASSIVE
	   };

	ESP_ERROR_CHECK(esp_wifi_scan_start(&scanConf, false));
//	wifi_station_scan(NULL, wifiScanDoneCb);
}

void cgiWiFiEventHandlerEsp32(void *ctx, system_event_t *event)
{
    switch(event->event_id) {
    case SYSTEM_EVENT_STA_START:
        break;
    case SYSTEM_EVENT_STA_GOT_IP:
    	connTryStatus=WIFI_GOT_IP;
        break;
    case SYSTEM_EVENT_STA_CONNECTED:
    	connTryStatus = WIFI_CONNECTED;
        break;
    case SYSTEM_EVENT_STA_DISCONNECTED:
    	connTryStatus=WIFI_FAIL;
        break;
    case SYSTEM_EVENT_AP_START:
        break;
    case SYSTEM_EVENT_AP_STACONNECTED:
        break;
    case SYSTEM_EVENT_AP_STADISCONNECTED:
        break;
    case SYSTEM_EVENT_SCAN_DONE:
    	ESP_LOGI(TAG, "~~~~~~SCAN_DONE~~~~~");
    	wifiScanDoneEsp32(ctx,event);
        break;
    default:
        break;
    }
}


//This CGI is called from the bit of AJAX-code in wifi.tpl. It will initiate a
//scan for access points and if available will return the result of an earlier scan.
//The result is embedded in a bit of JSON parsed by the javascript in wifi.tpl.
CgiStatus ICACHE_FLASH_ATTR cgiWiFiScanEsp32(HttpdConnData *connData) {
	int pos=(int)connData->cgiData;
	int len;
	char buff[1024];

	printf("====================pos = %d\n",pos);
	if (!cgiWifiAps.scanInProgress && pos!=0) {
		//Fill in json code for an access point
		if (pos-1<cgiWifiAps.noAps) {
			len=sprintf(buff, "{\"essid\": \"%s\", \"bssid\": \"" MACSTR "\", \"rssi\": \"%d\", \"enc\": \"%d\", \"channel\": \"%d\"}%s\n",
					cgiWifiAps.apData[pos-1]->ssid, MAC2STR(cgiWifiAps.apData[pos-1]->bssid), cgiWifiAps.apData[pos-1]->rssi,
					cgiWifiAps.apData[pos-1]->enc, cgiWifiAps.apData[pos-1]->channel, (pos-1==cgiWifiAps.noAps-1)?"":",");
			httpdSend(connData, buff, len);
		}
		pos++;
		if ((pos-1)>=cgiWifiAps.noAps) {
			len=sprintf(buff, "]\n}\n}\n");
			httpdSend(connData, buff, len);
			//Also start a new scan.
			wifiStartScan();
			return HTTPD_CGI_DONE;
		} else {
			connData->cgiData=(void*)pos;
			return HTTPD_CGI_MORE;
		}
	}

	httpdStartResponse(connData, 200);
	httpdHeader(connData, "Content-Type", "text/json");
	httpdEndHeaders(connData);

	if (cgiWifiAps.scanInProgress==1) {
		//We're still scanning. Tell Javascript code that.
		len=sprintf(buff, "{\n \"result\": { \n\"inProgress\": \"1\"\n }\n}\n");
		httpdSend(connData, buff, len);
		return HTTPD_CGI_DONE;
	} else {
		//We have a scan result. Pass it on.
		len=sprintf(buff, "{\n \"result\": { \n\"inProgress\": \"0\", \n\"APs\": [\n");
		httpdSend(connData, buff, len);
		if (cgiWifiAps.apData==NULL) cgiWifiAps.noAps=0;
		connData->cgiData=(void *)1;
		return HTTPD_CGI_MORE;
	}
}

#ifndef ESP32
//Temp store for new ap info.
static struct station_config stconf;

//This routine is ran some time after a connection attempt to an access point. If
//the connect succeeds, this gets the module in STA-only mode.
static void ICACHE_FLASH_ATTR resetTimerCb(void *arg) {
	int x=wifi_station_get_connect_status();
	if (x==STATION_GOT_IP) {
		//Go to STA mode. This needs a reset, so do that.
		httpd_printf("Got IP. Going into STA mode..\n");
		wifi_set_opmode(1);
		system_restart();
	} else {
		connTryStatus=CONNTRY_FAIL;
		httpd_printf("Connect fail. Not going into STA-only mode.\n");
		//Maybe also pass this through on the webpage?
	}
}



//Actually connect to a station. This routine is timed because I had problems
//with immediate connections earlier. It probably was something else that caused it,
//but I can't be arsed to put the code back :P
static void ICACHE_FLASH_ATTR reassTimerCb(void *arg) {
	int x;
	httpd_printf("Try to connect to AP....\n");
	wifi_station_disconnect();
	wifi_station_set_config(&stconf);
	wifi_station_connect();
	x=wifi_get_opmode();
	connTryStatus=CONNTRY_WORKING;
	if (x!=1) {
		//Schedule disconnect/connect
		os_timer_disarm(&resetTimer);
		os_timer_setfn(&resetTimer, resetTimerCb, NULL);
		os_timer_arm(&resetTimer, 15000, 0); //time out after 15 secs of trying to connect
	}
}
#endif

//This cgi uses the routines above to connect to a specific access point with the
//given ESSID using the given password.
CgiStatus ICACHE_FLASH_ATTR cgiWiFiConnectEsp32(HttpdConnData *connData)
{
	char essid[128];
	char passwd[128];

	if (connData->conn==NULL) {
		//Connection aborted. Clean up.
		return HTTPD_CGI_DONE;
	}

	httpdFindArg(connData->post->buff, "essid", essid, sizeof(essid));
	httpdFindArg(connData->post->buff, "passwd", passwd, sizeof(passwd));

	//Connect to the defined access point.
	httpd_printf("Try to connect to AP %s pw %s\n", essid, passwd);

	wifi_config_t config;
	memset(&config, 0, sizeof(config));
	strncpy((char*)config.sta.ssid, essid,32);
	strncpy((char*)config.sta.password, passwd,64);
	esp_wifi_set_config(WIFI_IF_STA, &config);
	esp_wifi_connect();

	httpdRedirect(connData, "connecting.html");

	return HTTPD_CGI_DONE;
}

//This cgi uses the routines above to connect to a specific access point with the
//given ESSID using the given password.
CgiStatus ICACHE_FLASH_ATTR cgiWiFiSetModeEsp32(HttpdConnData *connData) {
#ifndef ESP32
	int len;
	char buff[1024];

	if (connData->conn==NULL) {
		//Connection aborted. Clean up.
		return HTTPD_CGI_DONE;
	}

	len=httpdFindArg(connData->getArgs, "mode", buff, sizeof(buff));
	if (len!=0) {
		httpd_printf("cgiWifiSetMode: %s\n", buff);
#ifndef DEMO_MODE
		wifi_set_opmode(atoi(buff));
		system_restart();
#endif
	}
	httpdRedirect(connData, "/wifi");
#endif
	return HTTPD_CGI_DONE;
}

//Set wifi channel for AP mode
CgiStatus ICACHE_FLASH_ATTR cgiWiFiSetChannelEsp32(HttpdConnData *connData) {
#ifndef ESP32
	int len;
	char buff[64];

	if (connData->conn==NULL) {
		//Connection aborted. Clean up.
		return HTTPD_CGI_DONE;
	}

	len=httpdFindArg(connData->getArgs, "ch", buff, sizeof(buff));
	if (len!=0) {
		httpd_printf("cgiWifiSetChannel: %s\n", buff);
		int channel = atoi(buff);
		if (channel > 0 && channel < 15) {
			httpd_printf("Setting ch=%d\n", channel);

			struct softap_config wificfg;
			wifi_softap_get_config(&wificfg);
			wificfg.channel = (uint8)channel;
			wifi_softap_set_config(&wificfg);
		}
	}
	httpdRedirect(connData, "/wifi");
#endif

	return HTTPD_CGI_DONE;
}

CgiStatus ICACHE_FLASH_ATTR cgiWiFiConnStatusEsp32(HttpdConnData *connData)
{
	char buff[1024];
	int len=0;

	httpdStartResponse(connData, 200);
	httpdHeader(connData, "Content-Type", "text/json");
	httpdEndHeaders(connData);
	if (connTryStatus==WIFI_IDLE)
	{
		len=sprintf(buff, "{\n \"status\": \"idle\"\n }\n");
	}
	else if (connTryStatus==WIFI_BUSY )
	{
		len=sprintf(buff, "{\n \"status\": \"working...\"\n }\n");
	}
	else if( connTryStatus==WIFI_CONNECTED )
	{
		len=sprintf(buff, "{\n \"status\": \"Connected\"\n }\n");
	}
	else if( connTryStatus == WIFI_GOT_IP )
	{
        tcpip_adapter_ip_info_t ip_info;
        if (tcpip_adapter_get_ip_info(TCPIP_ADAPTER_IF_STA, &ip_info) == 0)
        {
    		len=sprintf(buff, "{\n \"status\": \"success\",\n \"ip\": \"%d.%d.%d.%d\" }\n",
    			(ip_info.ip.addr>>0)&0xff,
				(ip_info.ip.addr>>8)&0xff,
    			(ip_info.ip.addr>>16)&0xff,
				(ip_info.ip.addr>>24)&0xff
				);
        }

	} else {
		len=sprintf(buff, "{\n \"status\": \"fail\"\n }\n");
	}

	httpdSend(connData, buff, len);

	return HTTPD_CGI_DONE;
}

//Template code for the WLAN page.
int ICACHE_FLASH_ATTR tplWlanEsp32(HttpdConnData *connData, char *token, void **arg) {
#ifndef ESP32
	char buff[1024];
	int x;
	static struct station_config stconf;
	if (token==NULL) return HTTPD_CGI_DONE;
	wifi_station_get_config(&stconf);

	strcpy(buff, "Unknown");
	if (strcmp(token, "WiFiMode")==0) {
		x=wifi_get_opmode();
		if (x==1) strcpy(buff, "Client");
		if (x==2) strcpy(buff, "SoftAP");
		if (x==3) strcpy(buff, "STA+AP");
	} else if (strcmp(token, "currSsid")==0) {
		strcpy(buff, (char*)stconf.ssid);
	} else if (strcmp(token, "WiFiPasswd")==0) {
		strcpy(buff, (char*)stconf.password);
	} else if (strcmp(token, "WiFiapwarn")==0) {
		x=wifi_get_opmode();
		if (x==2) {
			strcpy(buff, "<b>Can't scan in this mode.</b> Click <a href=\"setmode.cgi?mode=3\">here</a> to go to STA+AP mode.");
		} else {
			strcpy(buff, "Click <a href=\"setmode.cgi?mode=2\">here</a> to go to standalone AP mode.");
		}
	}
	httpdSend(connData, buff, -1);
#endif
	return HTTPD_CGI_DONE;
}
