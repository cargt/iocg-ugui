#include "ugui.h"
#include "ugui_config.h"

#ifndef __UGUI_SETTINGS_ABOUT_H
#define __UGUI_SETTINGS_ABOUT_H

UG_RESULT UG_SettingsAboutCreate();
UG_RESULT UG_SettingsAboutShow();
UG_RESULT UG_SettingsAboutHide();

#endif //#ifndef __UGUI_SETTINGS_ABOUT_H
