#include "ugui.h"
#include "images.h"

#ifndef __UGUI_SETTINGS_GENERAL_H
#define __UGUI_SETTINGS_GENERAL_H

UG_RESULT UG_SettingsGeneralCreate();
UG_RESULT UG_SettingsGeneralShow();
UG_RESULT UG_SettingsGeneralHide();

#endif //#ifndef __UGUI_SETTINGS_GENERAL_H
