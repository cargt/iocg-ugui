#include "ugui.h"

#ifndef __UGUI_MSG_H
#define __UGUI_MSG_H

typedef enum
{
	UGUI_MSG_STYLE_NO_BUTTONS,
	UGUI_MSG_STYLE_1_BUTTON,
	UGUI_MSG_STYLE_2_BUTTONS
}ugui_msg_style_t;

typedef enum
{
	UGUI_MSG_ICON_NONE,
	UGUI_MSG_ICON_WORKING,

}ugui_msg_icon_t;

typedef void (*UG_Msg_Callback)( int btn );

UG_RESULT UG_MsgCreate();
UG_RESULT UG_MsgShow(const char* msg,
		const char* 		msg2,
		ugui_msg_icon_t		icon,
		ugui_msg_style_t 	style,
		const char* 		btn1_str,
		const char* 		btn2_str,
		UG_Msg_Callback 	cb,
		UG_S32 				timeout_ms,
		ugui_system_event_t evt_mask );
UG_RESULT UG_MsgHide();

#endif
