#include <string.h>
#include "freertos/FreeRTOS.h"
#include "main.h"
#include "ugui_helper.h"

#include "ugui_settings_network_wifi_info.h"
#include "ugui_settings_network_wifi.h"
#include "task_network.h"


//buttons
#define TXB_ID_HEADER				TXB_ID_0
#define IMG_ID_LOGO					IMG_ID_1
#define IMG_ID_BATTERY				IMG_ID_2
#define IMG_ID_WIFI					IMG_ID_3
#define BUTTON_ID_BACK 				BTN_ID_4
#define TXB_ID_ABOUT				TXB_ID_5
#define TXB_ID_IP_ADDR				TXB_ID_6
#define TXB_ID_MAC_ADDR				TXB_ID_7
#define TXB_ID_SSID					TXB_ID_8
#define TXB_ID_STATUS				TXB_ID_9
#define MAX_OBJECTS        			10

#define LINE_XS		10
#define LINE_XE		(UG_WindowGetInnerWidth(&window_1)-1-LINE_XS)
#define LINE_H		30

#define LINE1_XS	LINE_XS
#define LINE1_XE	LINE_XE
#define LINE1_YS	SETTINGS_TXB_YE
#define LINE1_YE	LINE1_YS+LINE_H
#define LINE2_XS	LINE_XS
#define LINE2_XE	LINE_XE
#define LINE2_YS	LINE1_YE
#define LINE2_YE	LINE2_YS+LINE_H
#define LINE3_XS	LINE_XS
#define LINE3_XE	LINE_XE
#define LINE3_YS	LINE2_YE
#define LINE3_YE	LINE3_YS+LINE_H
#define LINE4_XS	LINE_XS
#define LINE4_XE	LINE_XE
#define LINE4_YS	LINE3_YE
#define LINE4_YE	LINE4_YS+LINE_H


/* Window 1 */
static UG_WINDOW window_1;
static UG_OBJECT obj_buff_wnd_1[MAX_OBJECTS];
static UG_TEXTBOX tb_header;

static UG_TEXTBOX tb_header;
static UG_IMAGE img_battery;
static UG_IMAGE img_wifi;
static UG_BUTTON button_back;
static UG_TEXTBOX tb_about;
static UG_TEXTBOX tb_ip_addr;
static UG_TEXTBOX tb_mac_addr;
static UG_TEXTBOX tb_ssid;
static UG_TEXTBOX tb_status;

#define IP_ADDR_SIZE (16+1)
#define MAC_ADDR_SIZE (32+1)
static char ip_str[100] = "";
static char mac_str[100] = "";
static char ssid_str[100] = "";

static void touch_callback( UG_MESSAGE* msg );
static void window_update( ugui_system_event_t evt );

UG_RESULT UG_SettingsNetworkWifiInfoCreate()
{
	/*******************************************************
	 * Window 1
	 *******************************************************/
	UG_WindowCreate( &window_1, obj_buff_wnd_1, MAX_OBJECTS, touch_callback );
	UG_WindowSetForeColor( &window_1,SETTINGS_WINDOW_FORECOLOR );
	UG_WindowSetBackColor( &window_1,SETTINGS_WINDOW_BACKCOLOR );
	UG_WindowSetStyle( &window_1, WINDOW_STYLE);

	UG_S16 width = UG_WindowGetInnerWidth(&window_1);
	UG_S16 height = UG_WindowGetInnerHeight(&window_1);

	/*******************************************************
	 * Window Header
	 ******************************************************/

	// text box - window header
	UG_TextboxCreate( &window_1, &tb_header,TXB_ID_HEADER,0,0,width,WINDOW_HEADER_H);
	UG_TextboxSetFont( &window_1, TXB_ID_HEADER, &TXB_HEADER_FONT );
	UG_TextboxSetForeColor( &window_1, TXB_ID_HEADER, TXB_HEADER_FORECOLOR );
	UG_TextboxSetBackColor( &window_1, TXB_ID_HEADER, TXB_HEADER_BACKCOLOR );
	UG_TextboxSetAlignment( &window_1, TXB_ID_HEADER, ALIGN_CENTER );
	UG_TextboxSetText( &window_1, TXB_ID_HEADER, "Cargt uGUI" );

	// image - battery
	UG_ImageCreate( &window_1, &img_battery, IMG_ID_BATTERY,
			HEADER_BATTERY_XS,
			HEADER_BATTERY_YS,
			HEADER_BATTERY_XE,
			HEADER_BATTERY_YE );
	UG_ImageSetBMP( &window_1, IMG_ID_BATTERY, &HEADER_BATTERY_IMG );
	UG_ImageSetEraseFirstFlag( &window_1, IMG_ID_BATTERY, 1, TXB_HEADER_BACKCOLOR );

	// image - wifi
	UG_ImageCreate( &window_1, &img_wifi, IMG_ID_WIFI,
			HEADER_WIFI_XS,
			HEADER_WIFI_YS,
			HEADER_WIFI_XE,
			HEADER_WIFI_YE );
	UG_ImageSetBMP( &window_1, IMG_ID_WIFI, &HEADER_WIFI_IMG );
	UG_ImageSetEraseFirstFlag( &window_1, IMG_ID_WIFI, 1, TXB_HEADER_BACKCOLOR );

	//back
	UG_ButtonCreate( &window_1, &button_back, BUTTON_ID_BACK, SETTINGS_BUTTON_BACK_XS,SETTINGS_BUTTON_BACK_YS,SETTINGS_BUTTON_BACK_XE,SETTINGS_BUTTON_BACK_YE);
	UG_ButtonSetFont( &window_1, 		BUTTON_ID_BACK, &SETTINGS_BUTTON_FONT );
	UG_ButtonSetForeColor( &window_1, 	BUTTON_ID_BACK, TXB_HEADER_FORECOLOR );
	UG_ButtonSetBackColor( &window_1, 	BUTTON_ID_BACK, TXB_HEADER_BACKCOLOR );
	UG_ButtonSetStyle( &window_1, 		BUTTON_ID_BACK, SETTINGS_BUTTON_BACK_STYLE);
	UG_ButtonSetImage( &window_1, BUTTON_ID_BACK, BTN_IMG_CENTER, &SETTINGS_BUTTON_BACK_ICON, SETTINGS_BUTTON_BACK_ICON.width, SETTINGS_BUTTON_BACK_ICON.height );

	/*******************************************************
	 * Window Specific items
	 ******************************************************/
	// text box
	UG_TextboxCreate( &window_1, &tb_about,TXB_ID_ABOUT,SETTINGS_TXB_XS,SETTINGS_TXB_YS,SETTINGS_TXB_XE,SETTINGS_TXB_YE-1);
	UG_TextboxSetFont( &window_1, TXB_ID_ABOUT, &SETTINGS_TXB_FONT );
	UG_TextboxSetForeColor( &window_1, TXB_ID_ABOUT, SETTINGS_TXB_FORECOLOR );
	UG_TextboxSetBackColor( &window_1, TXB_ID_ABOUT, SETTINGS_TXB_BACKCOLOR );
	UG_TextboxSetAlignment( &window_1, TXB_ID_ABOUT, SETTINGS_TXB_ALIGNMENT );
	UG_TextboxSetText( &window_1, TXB_ID_ABOUT, "WiFi Info" );

	// status
	UG_TextboxCreate( &window_1, &tb_status, TXB_ID_STATUS,
			LINE1_XS,
			LINE1_YS,
			LINE1_XE,
			LINE1_YE );
	UG_TextboxSetFont( &window_1, TXB_ID_STATUS, &FONT_16 );
	UG_TextboxSetForeColor( &window_1, TXB_ID_STATUS, SETTINGS_WINDOW_FORECOLOR );
	UG_TextboxSetBackColor( &window_1, TXB_ID_STATUS, SETTINGS_WINDOW_BACKCOLOR );
	UG_TextboxSetAlignment( &window_1, TXB_ID_STATUS, ALIGN_CENTER_LEFT );
	UG_TextboxSetText( &window_1, TXB_ID_STATUS, "Disconnected" );

	// ssid
	UG_TextboxCreate( &window_1, &tb_ssid, TXB_ID_SSID,
			LINE2_XS,
			LINE2_YS,
			LINE2_XE,
			LINE2_YE );
	UG_TextboxSetFont( &window_1, TXB_ID_SSID, &FONT_16 );
	UG_TextboxSetForeColor( &window_1, TXB_ID_SSID, SETTINGS_WINDOW_FORECOLOR );
	UG_TextboxSetBackColor( &window_1, TXB_ID_SSID, SETTINGS_WINDOW_BACKCOLOR );
	UG_TextboxSetAlignment( &window_1, TXB_ID_SSID, ALIGN_CENTER_LEFT );
	UG_TextboxSetText( &window_1, TXB_ID_SSID, ssid_str );

	// ip addr
	UG_TextboxCreate( &window_1, &tb_ip_addr, TXB_ID_IP_ADDR,
			LINE3_XS,
			LINE3_YS,
			LINE3_XE,
			LINE3_YE );
	UG_TextboxSetFont( &window_1, TXB_ID_IP_ADDR, &FONT_16 );
	UG_TextboxSetForeColor( &window_1, TXB_ID_IP_ADDR, SETTINGS_WINDOW_FORECOLOR );
	UG_TextboxSetBackColor( &window_1, TXB_ID_IP_ADDR, SETTINGS_WINDOW_BACKCOLOR );
	UG_TextboxSetAlignment( &window_1, TXB_ID_IP_ADDR, ALIGN_CENTER_LEFT );
	UG_TextboxSetText( &window_1, TXB_ID_IP_ADDR, ip_str );

	// mac addr
	UG_TextboxCreate( &window_1, &tb_mac_addr, TXB_ID_MAC_ADDR,
			LINE4_XS,
			LINE4_YS,
			LINE4_XE,
			LINE4_YE );
	UG_TextboxSetFont( &window_1, TXB_ID_MAC_ADDR, &FONT_16 );
	UG_TextboxSetForeColor( &window_1, TXB_ID_MAC_ADDR, SETTINGS_WINDOW_FORECOLOR );
	UG_TextboxSetBackColor( &window_1, TXB_ID_MAC_ADDR, SETTINGS_WINDOW_BACKCOLOR );
	UG_TextboxSetAlignment( &window_1, TXB_ID_MAC_ADDR, ALIGN_CENTER_LEFT );
	UG_TextboxSetText( &window_1, TXB_ID_MAC_ADDR, mac_str );

	return UG_RESULT_OK;
}

UG_RESULT UG_SettingsNetworkWifiInfoShow()
{
	char ip[IP_ADDR_SIZE];
	char mac[MAC_ADDR_SIZE];
	char my_ssid[65];

	//wifi status
	enum wifi_status_t status = task_network_getWifiStatus();
	if( status == WIFI_STATUS_CONNECTED )
	{
		UG_TextboxSetText( &window_1, TXB_ID_STATUS, "Connected");
	}
	else
	{
		UG_TextboxSetText( &window_1, TXB_ID_STATUS, "Disconnected");
	}

	//ip addr
	task_network_getIpAddressWifi(ip,IP_ADDR_SIZE);
	sprintf( ip_str, "IP:   %s",ip);
	UG_TextboxSetText( &window_1, TXB_ID_IP_ADDR, ip_str );

	//mac addr
	ESP_ERROR_CHECK(esp_read_mac((uint8_t*)mac, ESP_MAC_WIFI_STA));
	sprintf( mac_str, "MAC:  %02x:%02x:%02x:%02x:%02x:%02x",
                mac[0], mac[1], mac[2]
                ,mac[3], mac[4], mac[5]
                );
		UG_TextboxSetText( &window_1, TXB_ID_MAC_ADDR, mac_str );

	return UG_WindowShow( &window_1, window_update );
}

static void window_update( ugui_system_event_t evt )
{
	switch(evt)
	{
	case UGUI_SYSTEM_EVENT_TICK:
		ugui_icon_wifi_update(&window_1,IMG_ID_WIFI);
		ugui_icon_battery_update(&window_1,IMG_ID_BATTERY);
		break;
	case UGUI_SYSTEM_EVENT_WIFI_SCAN_DONE:
		break;
	default:
		break;
	}
}

UG_RESULT UG_SettingsNetworkWifiInfoHide()
{
	return UG_WindowHide( &window_1 );
}

static void touch_callback( UG_MESSAGE* msg )
{
	if ( msg->type == MSG_TYPE_OBJECT )
	{
		if ( msg->id == OBJ_TYPE_BUTTON && msg->event == OBJ_EVENT_RELEASED )
		{
			switch(msg->sub_id)
			{
			case BUTTON_ID_BACK:
				UG_SettingsNetworkWifiShow();
				break;
			}
		}
	}

}

