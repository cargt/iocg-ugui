#include "ugui.h"
#include "ugui_config.h"

#ifndef __UGUI_SETTINGS_NETWORK_ETHERNET_H
#define __UGUI_SETTINGS_NETWORK_ETHERNET_H

UG_RESULT UG_SettingsNetworkEthernetCreate();
UG_RESULT UG_SettingsNetworkEthernetShow();
UG_RESULT UG_SettingsNetworkEthernetHide();

#endif //#ifndef __UGUI_SETTINGS_NETWORK_ETHERNET_H
