#include "sdkconfig.h"

#include "string.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_log.h"

#include "ugui_settings_general.h"

#include "ugui_home.h"
#include "ugui_helper.h"
#include "task_network.h"

#define TAG "UGUI_home"
//Home Window
#define HOME_WINDOW_FORECOLOR 	WINDOW_FORECOLOR
#define HOME_WINDOW_BACKCOLOR 	WINDOW_BACKCOLOR
#define HOME_WINDOW_STYLE 		WINDOW_STYLE
#define HOME_BUTTON_FORECOLOR 	HOME_WINDOW_FORECOLOR
#define HOME_BUTTON_BACKCOLOR 	HOME_WINDOW_BACKCOLOR
#define HOME_BUTTON_ALT_FORECOLOR	C_WHITE
#define HOME_BUTTON_ALT_BACKCOLOR	CARGT_GREEN
#define HOME_BUTTON_STYLE 		BUTTON_STYLE

#define TXB_ID_HEADER				TXB_ID_0
#define IMG_ID_LOGO					IMG_ID_1
#define IMG_ID_BATTERY				IMG_ID_2
#define IMG_ID_WIFI					IMG_ID_3
#define BUTTON_ID_SETTINGS 			BTN_ID_4
#define BUTTON_ID_WIFI_MODE			BTN_ID_5
#define BUTTON_ID_BACK				BTN_ID_6
#define BUTTON_ID_1					BTN_ID_7

#define MAX_OBJECTS        			8

static UG_WINDOW window_1;
static UG_OBJECT obj_buff_wnd_1[MAX_OBJECTS];


#define BTN_FORECOLOR 		CARGT_GREEN
#define BTN_BACKCOLOR 		C_WHITE
#define BTN_FONT_HDR		FONT_12
#define BTN_FONTCOLOR_HDR	C_DARK_GRAY
#define BTN_ALIGMENT_HDR 	ALIGN_TOP_LEFT
#define BTN_FONT_VALUE		FONT_36
#define BTN_ALIGMENT_VALUE 	ALIGN_BOTTOM_LEFT
#define BTN_FONT_FTR		FONT_12
#define BTN_ALIGMENT_FTR 	ALIGN_BOTTOM_RIGHT
#define BTN_SPACING			10
#define BTN_NUM_W		( (UG_WindowGetInnerWidth(&window_1) - 2*BTN_SPACING) )
#define BTN_NUM_H		( (UG_WindowGetInnerHeight(&window_1) - 2*BTN_SPACING - WINDOW_HEADER_H) )
#define BTN_OFFSET_VALUE_X	BTN_SPACING
#define BTN_OFFSET_VALUE_Y	BTN_SPACING


#define BTN_1_XS					BTN_OFFSET_VALUE_X
#define BTN_1_XE					(BTN_NUM_W+BTN_1_XS)
#define BTN_1_YS					WINDOW_HEADER_H+BTN_OFFSET_VALUE_Y
#define BTN_1_YE					(BTN_NUM_H+BTN_1_YS)


static UG_TEXTBOX tb_header;
static UG_IMAGE img_battery;
static UG_IMAGE img_wifi;
static UG_BUTTON button_settings;
static UG_BUTTON button_1;


//prototypes
static void window_update();
static void touch_callback( UG_MESSAGE* msg );


UG_RESULT UG_HomeCreate()
{
	/*******************************************************
	 * Window 1
	 *******************************************************/
	UG_WindowCreate( &window_1, obj_buff_wnd_1, MAX_OBJECTS, touch_callback );
	UG_WindowSetForeColor( &window_1,HOME_WINDOW_FORECOLOR );
	UG_WindowSetBackColor( &window_1,HOME_WINDOW_BACKCOLOR );
	UG_WindowSetStyle( &window_1, HOME_WINDOW_STYLE);

	UG_S16 width = UG_WindowGetInnerWidth(&window_1);
	UG_S16 height = UG_WindowGetInnerHeight(&window_1);

	/*******************************************************
	 * Window Header
	 ******************************************************/

	// text box - window header
	UG_TextboxCreate( &window_1, &tb_header,TXB_ID_HEADER,0,0,width,WINDOW_HEADER_H);
	UG_TextboxSetFont( &window_1, TXB_ID_HEADER, &TXB_HEADER_FONT );
	UG_TextboxSetForeColor( &window_1, TXB_ID_HEADER, TXB_HEADER_FORECOLOR );
	UG_TextboxSetBackColor( &window_1, TXB_ID_HEADER, TXB_HEADER_BACKCOLOR );
	UG_TextboxSetAlignment( &window_1, TXB_ID_HEADER, ALIGN_CENTER );
	UG_TextboxSetText( &window_1, TXB_ID_HEADER, "Cargt uGUI" );

	// image - battery
	UG_ImageCreate( &window_1, &img_battery, IMG_ID_BATTERY,
			HEADER_BATTERY_XS,
			HEADER_BATTERY_YS,
			HEADER_BATTERY_XE,
			HEADER_BATTERY_YE );
	UG_ImageSetBMP( &window_1, IMG_ID_BATTERY, &HEADER_BATTERY_IMG );
	UG_ImageSetEraseFirstFlag( &window_1, IMG_ID_BATTERY, 1, TXB_HEADER_BACKCOLOR );

	// image - wifi
	UG_ImageCreate( &window_1, &img_wifi, IMG_ID_WIFI,
			HEADER_WIFI_XS,
			HEADER_WIFI_YS,
			HEADER_WIFI_XE,
			HEADER_WIFI_YE );
	UG_ImageSetBMP( &window_1, IMG_ID_WIFI, &HEADER_WIFI_IMG );
	UG_ImageSetEraseFirstFlag( &window_1, IMG_ID_WIFI, 1, TXB_HEADER_BACKCOLOR );

	/*******************************************************
	 * Window Specific items
	 ******************************************************/
	//BUTTON - BUTTON_ID_1
	UG_ButtonCreate( &window_1, &button_1, BUTTON_ID_1,
			BTN_1_XS,
			BTN_1_YS,
			BTN_1_XE,
			BTN_1_YE );
	UG_ButtonSetForeColor( &window_1, 	BUTTON_ID_1, HOME_BUTTON_FORECOLOR );
	UG_ButtonSetBackColor( &window_1, 	BUTTON_ID_1, C_DARK_RED );
	UG_ButtonSetAlternateForeColor( &window_1, BUTTON_ID_1, HOME_BUTTON_ALT_FORECOLOR );
	UG_ButtonSetAlternateBackColor( &window_1, BUTTON_ID_1, HOME_BUTTON_ALT_BACKCOLOR );
	UG_ButtonSetStyle( &window_1, 		BUTTON_ID_1, HOME_BUTTON_STYLE);
//	UG_ButtonSetImage( &window_1, BUTTON_ID_1, BTN_IMG_BACKGROUND, &img_ring_green, img_ring.width, img_ring.height );
	UG_ButtonSetAlignment( &window_1, BUTTON_ID_1, BTN_ALIGMENT_VALUE);
	UG_ButtonSetFont( &window_1, BUTTON_ID_1, &BTN_FONT_VALUE );
	UG_ButtonSetText( &window_1, BUTTON_ID_1,"String 1" );
	UG_ButtonSetAlignment2( &window_1, BUTTON_ID_1, BTN_ALIGMENT_HDR);
	UG_ButtonSetFont2( &window_1, BUTTON_ID_1, &BTN_FONT_HDR, BTN_FONTCOLOR_HDR );
	UG_ButtonSetText2( &window_1, BUTTON_ID_1,"String 2" );
	UG_ButtonSetAlignment3( &window_1, BUTTON_ID_1, BTN_ALIGMENT_FTR);
	UG_ButtonSetFont3( &window_1, BUTTON_ID_1, &BTN_FONT_FTR, BTN_FONTCOLOR_HDR );
	UG_ButtonSetText3( &window_1, BUTTON_ID_1,"String 3" );
	UG_ButtonSetVSpace( &window_1, BUTTON_ID_1,3 );
	UG_ButtonSetHSpace( &window_1, BUTTON_ID_1,10 );
	UG_ButtonSetImage( &window_1, BUTTON_ID_1, BTN_IMG_BACKGROUND, &img_logo, img_logo.width, img_logo.height );

	//BUTTON - settings
	UG_ButtonCreate( &window_1, &button_settings, BUTTON_ID_SETTINGS,  1,1 ,width/6,WINDOW_HEADER_H-1);
	UG_ButtonSetForeColor( &window_1, 	BUTTON_ID_SETTINGS, TXB_HEADER_FORECOLOR );
	UG_ButtonSetBackColor( &window_1, 	BUTTON_ID_SETTINGS, TXB_HEADER_BACKCOLOR );
	UG_ButtonSetStyle( &window_1, 		BUTTON_ID_SETTINGS, HOME_BUTTON_STYLE);
	UG_ButtonSetImage( &window_1, BUTTON_ID_SETTINGS, BTN_IMG_CENTER, &img_settings, img_settings.width, img_settings.height );

	return UG_RESULT_OK;
}

UG_RESULT UG_HomeShow()
{
	return UG_WindowShow( &window_1, window_update );
}

UG_RESULT UG_HomeHide()
{
	return UG_WindowHide( &window_1 );
}

static void window_update()
{
	static int update_cnt = 0;

	if(update_cnt-- <= 0)
	{
		ugui_icon_wifi_update(&window_1,IMG_ID_WIFI);

	}
}

static void touch_callback( UG_MESSAGE* msg )
{
	if ( msg->type == MSG_TYPE_OBJECT )
	{
		if ( msg->id == OBJ_TYPE_BUTTON && msg->event == OBJ_EVENT_RELEASED )
		{
			switch(msg->sub_id)
			{
			case BUTTON_ID_SETTINGS:
				UG_SettingsGeneralShow();
				break;

			case BUTTON_ID_1:
				break;

			}

		}
	}

}

