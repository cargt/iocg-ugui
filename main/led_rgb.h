/*
 * lec_rgb.h
 *
 *  Created on: Feb 9, 2018
 *      Author: MJV
 */

#ifndef MAIN_LED_RGB_H_
#define MAIN_LED_RGB_H_

#include <stdint.h>

void ledRgb_init(uint8_t rGpioNum, uint8_t gGpioNum, uint8_t bGpioNum);

void ledRgb_set_color(uint8_t r, uint8_t g, uint8_t b);


#endif /* MAIN_LED_RGB_H_ */
