/*
 * cgistatus.h
 *
 *  Created by Graham Taylor-Jones on Sep 25, 2018
 *  Copyright (c) YEAR Cargt, LLC. All rights reserved.
 *
 */

#ifndef MAIN_CGISTATUS_H_
#define MAIN_CGISTATUS_H_

#include "libesphttpd/httpd.h"

CgiStatus cgiGetCANMessages(HttpdConnData *connData);
CgiStatus cgiGetCANStatus(HttpdConnData *connData);



#endif /* MAIN_CGISTATUS_H_ */
