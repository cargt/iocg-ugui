/*
 * task_can.c
 *
 *  Modified by Graham Taylor-Jones on Sept 27, 2018
 *  Copyright (c) 2018 Cargt, LLC. All rights reserved.

 *  Created on: 14.06.2017
 *      Author: rudi ;-)
 *
 *      Olimex EVB REV B
 *      with CAN
 *      CAN Rx = GPIO 35
 *      CAN Tx = GPIO  5
 *      Node   = 197 ( you can change all in Kconfig )
 *
 *      done ;-)
 *      ....
 *      code comes later to olimex github
 *      after PR visit it here
 *      https://github.com/OLIMEX/ESP32-EVB/tree/master/SOFTWARE
 *      have phun
 *      best wishes
 *      rudi ;-)
 */
#include "sdkconfig.h"

#include "freertos/FreeRTOS.h"
#include "esp_wifi.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "nvs_flash.h"

#include <string.h>

#include "UTL_circ_buf_msg.h"
#include "msg_frame.h"

#include "task_can.h"
#include "CAN.h"
#include "CAN_config.h"

/* brief  : rudi
 * content: Set the CAN Speed over Kconfig
 * you can use menuconfig for this
 * and you can expand like you need
 * you can also use your own
 * cfg - look in the main/cfg folder
 * you have then change by self
 */
 #ifndef CONFIG_ESPCAN
 #error for this demo you must enable and configure ESPCan in menuconfig
 #endif

#ifdef CONFIG_CAN_SPEED_100KBPS
#define CONFIG_SELECTED_CAN_SPEED CAN_SPEED_100KBPS
#endif

#ifdef CONFIG_CAN_SPEED_125KBPS
#define CONFIG_SELECTED_CAN_SPEED CAN_SPEED_125KBPS
#endif

#ifdef CONFIG_CAN_SPEED_250KBPS
#define CONFIG_SELECTED_CAN_SPEED CAN_SPEED_250KBPS
#endif

#ifdef CONFIG_CAN_SPEED_500KBPS
#define CONFIG_SELECTED_CAN_SPEED CAN_SPEED_500KBPS
#endif

#ifdef CONFIG_CAN_SPEED_800KBPS
#define CONFIG_SELECTED_CAN_SPEED CAN_SPEED_800KBPS
#endif

#ifdef CONFIG_CAN_SPEED_1000KBPS
#define CONFIG_SELECTED_CAN_SPEED CAN_SPEED_1000KBPS
#endif

#ifdef CONFIG_CAN_SPEED_USER_KBPS
#define CONFIG_SELECTED_CAN_SPEED CONFIG_CAN_SPEED_USER_KBPS_VAL /* per menuconfig */
#endif

#define TASK_CAN_PRIO                       5U
#define TASK_CAN_STACK_SIZE                 2048U

#define CAN_RX_QUEUE_LENGTH                 10U

#define STATUS_DISCONNECTION_TIMEOUT_SECS   60      // Number of seconds to invalidate a device as connected

#define     minval(x,y)     ( (x) < (y) ? (x) : (y) )

//=================================
// Private variables
//=================================

/* brief  : rudi
 * content: config CAN Speed, Tx, Rx Pins taken from Kconfig
 * over menuconfig you can set the cfg
 * defines are used in the head of code then
 * if you change to user cfg
 * you can change here too by your self
 * how you need this.
*/
CAN_device_t CAN_cfg = {
	.speed		= CONFIG_SELECTED_CAN_SPEED,	// CAN Node baudrade
	.tx_pin_id 	= CONFIG_ESP_CAN_TXD_PIN_NUM,	// CAN TX pin example menuconfig GPIO_NUM_5
	.rx_pin_id 	= CONFIG_ESP_CAN_RXD_PIN_NUM,	// CAN RX pin example menuconfig GPIO_NUM_35 ( Olimex )
	.rx_queue	= NULL,							// FreeRTOS queue for RX frames
};

static const char *TAG = "can_task";

static msg_frame_t prv_msg_buf[ NUM_CAN_FRAME_LOG_ENTRIES ];
static UTL_circ_buf_msg_t msg_circ_buf;
// Copy buffer for CGI
static msg_frame_t get_copy_prv_msg_buf[ NUM_CAN_FRAME_LOG_ENTRIES ];
static UTL_circ_buf_msg_t get_copy_msg_circ_buf;



device_status_t device_status_list[NUM_DEVICE_STATUS_ENTRIES];
device_status_t get_copy_device_status_list[NUM_DEVICE_STATUS_ENTRIES];


//=================================
// Private function prototypes
//=================================
static void disconnectDeviceStatusBasedOnTimeout( time_t now );
static int processMessageToDeviceStatusList( msg_frame_t *ptrMsg );

static void task_CAN(void *pvParameters);
static void task_CAN_TX(void* pvParameters);


//=================================
// Public functions
//=================================
/******************************************************************************
* can_event_handler
* This will be called by the task monitoring the system events.
* Note: this is called from a different thread
*****************************************************************************/
esp_err_t can_event_handler(void *ctx, system_event_t *event)
{
	return ESP_OK;
}


int task_can_get_device_status( device_status_t *ptr, uint8_t len )
{
    if( NULL == ptr ) return -1;
    if( 0 == len ) return 0;

    // TODO: get semaphore
    // Copy buffer as a snapshot so that CGI could use multiple requests of smaller messages
    memcpy( &get_copy_device_status_list, &device_status_list, sizeof get_copy_device_status_list );
    // TODO: release semaphore

    uint8_t cnt = 0;
    for( int i = 0; i < (sizeof get_copy_device_status_list / sizeof get_copy_device_status_list[0] ); i++ )
    {
        if( cnt >= len )
        {
            break;
        }
        if( false == get_copy_device_status_list[i].valid )
        {
            continue;
        }

        if( false == get_copy_device_status_list[i].connected )
        {
            continue;
        }

        memcpy( ptr++, &get_copy_device_status_list[i], sizeof( device_status_t ) );
        ++cnt;
    }
    return cnt;
} /* task_can_get_device_status */


int task_can_get_message_list( msg_frame_t *ptr, uint8_t len )
{
    if( NULL == ptr ) return -1;
    if( 0 == len ) return 0;

    // TODO: get semaphore
    // Copy circular buffer as a snapshot so that CGI could use multiple requests of smaller messages
    memcpy( &get_copy_msg_circ_buf, &msg_circ_buf, sizeof get_copy_msg_circ_buf );
    memcpy( &get_copy_prv_msg_buf, &prv_msg_buf, sizeof prv_msg_buf );
    // TODO: release semaphore


    uint32_t num_records_available = UTL_circ_buf_msg_used_space( &get_copy_msg_circ_buf );
    uint32_t copy_records = minval( num_records_available, len );

    // GET NEWEST MESSAGES ONLY - skip over old ones
    if( num_records_available > len )
    {
        // Pop unrequired values out of buffer
        for( int i = 0; i < ( num_records_available - len ); i++ )
        {
            UTL_circ_buf_msg_read( &get_copy_msg_circ_buf, ptr, 1 ); // NOTE DON'T advance pointer
        }
    }
    int cnt = 0;
    for( int i = 0; i < copy_records; i++ )
    {
        UTL_circ_buf_msg_read( &get_copy_msg_circ_buf, ptr++, 1 ); // NOTE advance pointer
        ++cnt;
    }
    return cnt;
} /* task_can_get_message_list */


int  task_can_init(void)
{
	/*brief: rudi
	* if you have "activate ESPCan"
	* then the code here runs..
	*
	*/
#ifdef CONFIG_ESPCAN
	printf("**********   CAN TESTINGS **********\n");
	printf("Olimex EVB Rev B Board \n");
	printf("ESPCan configured by this Data:\n");
	printf("Node           : 0x%03x\n", CONFIG_ESP_CAN_NODE_ITSELF);
	printf("CAN RXD PIN NUM: %d\n", CONFIG_ESP_CAN_RXD_PIN_NUM);
	printf("CAN TXD PIN NUM: %d\n", CONFIG_ESP_CAN_TXD_PIN_NUM);
	printf("CAN SPEED      : %d KBit/s\n", CONFIG_SELECTED_CAN_SPEED);

#ifdef CONFIG_CAN_SPEED_USER_KBPS
	printf("kBit/s setting was done by User\n");
#endif

	//Create CAN receive task
	int ret = xTaskCreate(task_CAN,                /* pointer to the task                      */
		TAG,                        /* task name for kernel awareness debugging */
		TASK_CAN_STACK_SIZE,        /* task stack size                          */
		NULL,                       /* optional task startup argument           */
		TASK_CAN_PRIO,              /* initial priority                         */
		NULL                        /* optional task handle to create           */
	);
	if (pdPASS != ret)
	{
		assert(0);
	}

	UTL_circ_buf_msg_init( &msg_circ_buf, &prv_msg_buf[0], sizeof prv_msg_buf / sizeof prv_msg_buf[0] );

	memset( &device_status_list[0], 0, sizeof device_status_list );

	/* check if we have set a test frame to send in Kconfig */
#ifdef CONFIG_CAN_TEST_SENDING_ENABLED
	printf("Test Frame sending is enabled\n");
	vTaskDelay(1000 / portTICK_PERIOD_MS);
	xTaskCreate(&task_CAN_TX, "task_CAN_TX", 2048, NULL, 5, NULL);
#endif

	ESP_LOGI(TAG, "can_tsk:started,ret=%d", ret);
	return ret;

#endif
}

//=================================
// Private functions
//=================================
static void disconnectDeviceStatusBasedOnTimeout( time_t now )
{
    // scan all status
    int loopLimit = ( sizeof device_status_list / sizeof device_status_list[0] );
    for( int i = 0; i < loopLimit; i++)
    {
        if( device_status_list[i].connected )
        {
            // Check last timestamp
            if( difftime( now, device_status_list[i].timestamp ) > STATUS_DISCONNECTION_TIMEOUT_SECS )
            {
                device_status_list[i].connected = false;
            }
        }
    }

}
static int processMessageToDeviceStatusList( msg_frame_t *ptrMsg )
{
    if( NULL == ptrMsg ) return -1;

    disconnectDeviceStatusBasedOnTimeout( ptrMsg->timestamp );

    // find serial number or first empty slot (!valid)
    int firstNotValid_idx = -1;
    int firstDisconnected_idx = -1;
    int put_idx = -1;
    uint32_t cmdID = ( ptrMsg->can_frame.MsgID & CMD_ID_MASK );


    // Check for messages it will process - ignore the rest
    if( ( CANMSGID_IOA200_STATUS_HEARTBEAT != cmdID )
        && ( CANMSGID_IOA200_STATUS_1_HEAT_UI != cmdID )
        && ( CANMSGID_IOA200_STATUS_1_COLD_UI != cmdID )
        && ( CANMSGID_IOA200_STATUS_2_HEAT != cmdID )
        && ( CANMSGID_IOA200_STATUS_HOT_FROST != cmdID )
        && ( CANMSGID_IOA200_STATUS_INVALID_CFG != cmdID )
        && ( CANMSGID_IOA200_STATUS_TEST_MODE != cmdID )
        )
    {
        return -1;
    }

    uint32_t msgSerialNumber = ptrMsg->can_frame.MsgID & CAN_SERIAL_NUMBER_MASK;


    // scan all messages
    int loopLimit = ( sizeof device_status_list / sizeof device_status_list[0] );
    for( int i = 0; i < loopLimit; i++)
    {
        if( ( -1 == firstNotValid_idx ) && ( false == device_status_list[i].valid ) )          { firstNotValid_idx = i; }
        if( ( -1 == firstDisconnected_idx ) && ( false == device_status_list[i].connected ) )  { firstDisconnected_idx = i; }
        if( ( -1 == put_idx ) && ( device_status_list[i].serial == msgSerialNumber ) )
        {
            put_idx = i;
            break;
        }
    }

    if( -1 == put_idx )
    {
        // Serial didn't match so check other positions
        if( -1 != firstNotValid_idx )
        {
            put_idx = firstNotValid_idx;
        }
        else if( -1 != firstDisconnected_idx )
        {
            put_idx = firstDisconnected_idx;
        }
        // if new slot, zero out memory
        if( -1 != put_idx )
        {
            memset( &device_status_list[put_idx], 0, sizeof device_status_list[0] );
        }
    }

    static const char *text_configMode_1 = "1-HOT";
    static const char *text_configMode_2 = "1-COLD";
    static const char *text_configMode_4 = "2-HOT";
    static const char *text_configMode_8 = "HT/CLD";
    static const char *text_configMode_15 = "TEST";
    static const char *text_configMode_Err = "ERR-CFG";
    static const char *text_ON = "on";
    static const char *text_OFF = "OFF";

    if( -1 != put_idx )
    {
        // Copy data
        device_status_list[put_idx].valid = true;
        device_status_list[put_idx].timestamp = ptrMsg->timestamp;
        device_status_list[put_idx].connected = true;
        device_status_list[put_idx].serial = msgSerialNumber;
        // Now the message dependent data
        // Use if statements to declare pointer variables for type safety
        if( CANMSGID_IOA200_STATUS_HEARTBEAT == cmdID )
        {
            // Grab software version
            union UNION_CANLIB_MSGDEF_HEARTBEAT_STATUS *ptrData = (union UNION_CANLIB_MSGDEF_HEARTBEAT_STATUS *)&ptrMsg->can_frame.data;
            device_status_list[put_idx].sw_major = ptrData->Heartbeat_status_type.major;
            device_status_list[put_idx].sw_minor = ptrData->Heartbeat_status_type.minor;
            device_status_list[put_idx].sw_patch = ptrData->Heartbeat_status_type.build;
            device_status_list[put_idx].brd_type = ptrData->Heartbeat_status_type.brd_cfg;
            if( 0 == strlen( device_status_list[put_idx].comments ) )
            {
                switch( ptrData->Heartbeat_status_type.brd_cfg )
                {
                // 4 bit jumper configuration
                case 1: // POSITION 1: Single jumper fitted
                    sprintf( device_status_list[put_idx].comments, "SW:%d.%d.%d, %s"
                            ,device_status_list[put_idx].sw_major
                            ,device_status_list[put_idx].sw_minor
                            ,device_status_list[put_idx].sw_patch
                            ,text_configMode_1 );
                    break;
                case 2: // POSITION 2: Single jumper fitted
                    sprintf( device_status_list[put_idx].comments, "SW:%d.%d.%d, %s"
                            ,device_status_list[put_idx].sw_major
                            ,device_status_list[put_idx].sw_minor
                            ,device_status_list[put_idx].sw_patch
                            ,text_configMode_2 );
                    break;
                case 4: // POSITION 3: Single jumper fitted
                    sprintf( device_status_list[put_idx].comments, "SW:%d.%d.%d, %s"
                            ,device_status_list[put_idx].sw_major
                            ,device_status_list[put_idx].sw_minor
                            ,device_status_list[put_idx].sw_patch
                            ,text_configMode_4 );
                    break;
                case 8: // POSITION 4: Single jumper fitted
                    sprintf( device_status_list[put_idx].comments, "SW:%d.%d.%d, %s"
                            ,device_status_list[put_idx].sw_major
                            ,device_status_list[put_idx].sw_minor
                            ,device_status_list[put_idx].sw_patch
                            ,text_configMode_8 );
                    break;
                case 15: // All jumpers fitted (0b1111)
                    sprintf( device_status_list[put_idx].comments, "SW:%d.%d.%d, %s"
                            ,device_status_list[put_idx].sw_major
                            ,device_status_list[put_idx].sw_minor
                            ,device_status_list[put_idx].sw_patch
                            ,text_configMode_15 );
                    break;
                case 0: // No jumpers fitted
                default:
                    sprintf( device_status_list[put_idx].comments, "SW:%d.%d.%d, %s"
                            ,device_status_list[put_idx].sw_major
                            ,device_status_list[put_idx].sw_minor
                            ,device_status_list[put_idx].sw_patch
                            ,text_configMode_Err );
                    break;
                }
            }
        }
        else if( CANMSGID_IOA200_STATUS_1_HEAT_UI == cmdID )
        {
            union UNION_CANMSGID_IOA200_STATUS_1_HEAT_UI *ptrData = (union UNION_CANMSGID_IOA200_STATUS_1_HEAT_UI *)&ptrMsg->can_frame.data;
            sprintf( device_status_list[put_idx].comments, "SW:%d.%d.%d, %s,SP=%d%c,RD=%d%c,O/P:Triac=%s,Relay=%s,I/P:Pwr=%s"
                    ,device_status_list[put_idx].sw_major
                    ,device_status_list[put_idx].sw_minor
                    ,device_status_list[put_idx].sw_patch
                    ,text_configMode_1
                    ,ptrData->one_heat_status_type.temp_setting
                    ,'F'
                    ,ptrData->one_heat_status_type.temp_reading
                    ,'F'
                    ,( true == ptrData->one_heat_status_type.heater_output ) ? text_ON : text_OFF
                    ,( true == ptrData->one_heat_status_type.relay_output ) ? text_ON : text_OFF
                    ,( true == ptrData->one_heat_status_type.pwr_sens_1 ) ? text_ON : text_OFF
                    );
        }
        else if( CANMSGID_IOA200_STATUS_1_COLD_UI == cmdID )
        {
            union UNION_CANMSGID_IOA200_STATUS_1_COLD_UI *ptrData = (union UNION_CANMSGID_IOA200_STATUS_1_COLD_UI *)&ptrMsg->can_frame.data;
            sprintf( device_status_list[put_idx].comments, "SW:%d.%d.%d, %s,SP=%d%c,RD=%d%c,"
                    ,device_status_list[put_idx].sw_major
                    ,device_status_list[put_idx].sw_minor
                    ,device_status_list[put_idx].sw_patch
                    ,text_configMode_2
                    ,ptrData->one_cold_status_type.temp_setting
                    ,'F'
                    ,ptrData->one_cold_status_type.temp_reading
                    ,'F'
                    );
        }
        else if( CANMSGID_IOA200_STATUS_2_HEAT == cmdID )
        {
            union UNION_CANMSGID_IOA200_STATUS_2_HEAT *ptrData = (union UNION_CANMSGID_IOA200_STATUS_2_HEAT *)&ptrMsg->can_frame.data;
            sprintf( device_status_list[put_idx].comments, "SW:%d.%d.%d, %s,SP1=%d%c,SP2=%d%c"
                    ,device_status_list[put_idx].sw_major
                    ,device_status_list[put_idx].sw_minor
                    ,device_status_list[put_idx].sw_patch
                    ,text_configMode_4
                    ,ptrData->two_heat_status_type.pct_setting_1
                    ,'%'
                    ,ptrData->two_heat_status_type.pct_setting_2
                    ,'%'
                    );
        }
        else if( CANMSGID_IOA200_STATUS_HOT_FROST == cmdID )
        {
            union UNION_CANMSGID_IOA200_STATUS_HOT_FROST *ptrData = (union UNION_CANMSGID_IOA200_STATUS_HOT_FROST *)&ptrMsg->can_frame.data;
            sprintf( device_status_list[put_idx].comments, "SW:%d.%d.%d, %s,SP=%d%c,RD=%d%c,mode=%s,"
                    ,device_status_list[put_idx].sw_major
                    ,device_status_list[put_idx].sw_minor
                    ,device_status_list[put_idx].sw_patch
                    ,text_configMode_8
                    ,ptrData->hot_frost_status_type.pct_setting
                    ,'%'
                    ,ptrData->hot_frost_status_type.temp_reading
                    ,'F'
                    ,( 1 == ptrData->hot_frost_status_type.heat_mode ) ? "H" : "c"
                    );
        }
        else if( CANMSGID_IOA200_STATUS_INVALID_CFG == cmdID )
        {
            union UNION_CANMSGID_IOA200_STATUS_INVALID_CFG *ptrData = (union UNION_CANMSGID_IOA200_STATUS_INVALID_CFG *)&ptrMsg->can_frame.data;
            sprintf( device_status_list[put_idx].comments, "SW:%d.%d.%d, %s,Jumpers=%c%c%c%c,"
                    ,device_status_list[put_idx].sw_major
                    ,device_status_list[put_idx].sw_minor
                    ,device_status_list[put_idx].sw_patch
                    ,text_configMode_Err
                    ,( ptrData->invalid_cfg_status_type.raw_jumpers & 0x08 ) ? '1' : '0'
                    ,( ptrData->invalid_cfg_status_type.raw_jumpers & 0x04 ) ? '1' : '0'
                    ,( ptrData->invalid_cfg_status_type.raw_jumpers & 0x02 ) ? '1' : '0'
                    ,( ptrData->invalid_cfg_status_type.raw_jumpers & 0x01 ) ? '1' : '0'
                    );
        }
        else if( CANMSGID_IOA200_STATUS_TEST_MODE == cmdID )
        {
            union UNION_CANMSGID_IOA200_STATUS_TEST_MODE *ptrData = (union UNION_CANMSGID_IOA200_STATUS_TEST_MODE *)&ptrMsg->can_frame.data;
            sprintf( device_status_list[put_idx].comments, "SW:%d.%d.%d, %s,(RAW HEX) IN=%02X,OUT=%02X,RTD1=%03X,RTD2=%03X,ADJ1=%03X,ADJ2=%03X,"
                    ,device_status_list[put_idx].sw_major
                    ,device_status_list[put_idx].sw_minor
                    ,device_status_list[put_idx].sw_patch
                    ,text_configMode_15
                    ,ptrData->test_mode_status_type.inputs
                    ,ptrData->test_mode_status_type.outputs
                    ,ptrData->test_mode_status_type.rtd_val_raw_1
                    ,ptrData->test_mode_status_type.rtd_val_raw_2
                    ,ptrData->test_mode_status_type.adj_sns_raw_1
                    ,ptrData->test_mode_status_type.adj_sns_raw_1
                    );
        }
    }

    return put_idx;
}


static void task_CAN( void *pvParameters ){
    (void)pvParameters;

    //frame buffer
    CAN_frame_t __RX_frame;

    //create CAN RX Queue
    CAN_cfg.rx_queue = xQueueCreate(CAN_RX_QUEUE_LENGTH, sizeof(CAN_frame_t));

    //start CAN Module
    CAN_init();
    printf("Can Init done - wait now..\n");
    while (1){
        //receive next CAN frame from queue
        if(xQueueReceive(CAN_cfg.rx_queue,&__RX_frame, 3*portTICK_PERIOD_MS)==pdTRUE){

        	//do stuff!
        	// printf("New Frame from 0x%08x, DLC %d, dataL: 0x%08x, dataH: 0x%08x \r\n",__RX_frame.MsgID,  __RX_frame.FIR.B.DLC, __RX_frame.data.u32[0],__RX_frame.data.u32[1]);

#if 0
			printf("Frame from : 0x%08x, DLC %d \n", __RX_frame.MsgID, __RX_frame.FIR.B.DLC);
			printf("D0: 0x%02x, ", __RX_frame.data.u8[0]);
			printf("D1: 0x%02x, ", __RX_frame.data.u8[1]);
			printf("D2: 0x%02x, ", __RX_frame.data.u8[2]);
			printf("D3: 0x%02x, ", __RX_frame.data.u8[3]);
			printf("D4: 0x%02x, ", __RX_frame.data.u8[4]);
			printf("D5: 0x%02x, ", __RX_frame.data.u8[5]);
			printf("D6: 0x%02x, ", __RX_frame.data.u8[6]);
			printf("D7: 0x%02x\n", __RX_frame.data.u8[7]);
			printf("==============================================================================\n");
#endif

			// Copy to local struct
            static uint32_t pktCount = 0;
            msg_frame_t msg;
            msg.pktCount = ++pktCount;
            time_t now = 0;
            msg.timestamp = time(&now);

			memcpy( &msg.can_frame, &__RX_frame, sizeof msg.can_frame );

            ESP_LOGI(TAG, "rxd: cnt:%d", pktCount );

            // Rolling buffer - if full then read old record out to create buffer space
			if( 0 == UTL_circ_buf_msg_free_space( &msg_circ_buf ) )
			{
			    msg_frame_t dummy;
			    UTL_circ_buf_msg_read( &msg_circ_buf, &dummy, 1 );
			}
            UTL_circ_buf_msg_write( &msg_circ_buf, &msg, 1 );

	        processMessageToDeviceStatusList( &msg );

            //loop back frame
        	// CAN_write_frame(&__RX_frame);
        }
    }
}


static void task_CAN_TX(void* pvParameters) {

   CAN_frame_t __TX_frame;
   uint32_t counter = 0;

      __TX_frame.MsgID = CONFIG_ESP_CAN_NODE_ITSELF;
      __TX_frame.FIR.B.DLC   =  8;
      __TX_frame.data.u8[0]  = 'E';
      __TX_frame.data.u8[1]  = 'S';
      __TX_frame.data.u8[2]  = 'P';
      __TX_frame.data.u8[3]  = '-';
      __TX_frame.data.u8[4]  = 'C';
      __TX_frame.data.u8[5]  = 'A';
      __TX_frame.data.u8[6]  = 'N';
      __TX_frame.data.u8[7]  = counter;

while(1) {
      __TX_frame.data.u8[7] = counter;
      CAN_write_frame(&__TX_frame);
      printf("frame send [%3d]\r", counter);
      fflush(stdout);
      vTaskDelay( 1000 / portTICK_PERIOD_MS);  // to see ( printf on receiver side ) what happend..
      counter++;
      if (counter >= 256) counter = 0;
  }
}


