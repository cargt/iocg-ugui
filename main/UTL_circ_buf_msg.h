/*
 * UTL_circ_buf_msg.h
 *
 *  Created by Peter Carlson on March 22, 2017
 *  Copyright (c) 2017 Cargt, Inc. All rights reserved.
 *
 */

#ifndef UTL_CIRC_BUF_MSG_H_
#define UTL_CIRC_BUF_MSG_H_

#include <stdint.h>
#include <stdbool.h>

#include "msg_frame.h"


typedef struct
    {
//	uint32_t            rcrd_size;
	uint32_t            data_size;	//bytes
    uint32_t            read_idx;	//bytes
    uint32_t            write_idx;	//bytes
    uint8_t *           mem;
    bool                full;
    } UTL_circ_buf_msg_t;

uint32_t UTL_circ_buf_msg_free_space
    (
    UTL_circ_buf_msg_t *	cb
    );

void UTL_circ_buf_msg_init
    (
    UTL_circ_buf_msg_t *    cb,
	msg_frame_t *           buf,
	uint32_t                num_rcrds
    );

uint32_t UTL_circ_buf_msg_read
    (
    UTL_circ_buf_msg_t *    cb,
	msg_frame_t *           buf,
	uint32_t                num_rcrds
    );

void UTL_circ_buf_msg_reset
    (
    UTL_circ_buf_msg_t *     cb
    );

uint32_t UTL_circ_buf_msg_overwrite
    (
    UTL_circ_buf_msg_t *    cb,
    msg_frame_t *           buf,
    uint32_t                num_rcrds
    );

uint32_t UTL_circ_buf_msg_write
    (
    UTL_circ_buf_msg_t *    cb,
    msg_frame_t *           buf,
	uint32_t                num_rcrds
    );

uint32_t UTL_circ_buf_msg_used_space
    (
    UTL_circ_buf_msg_t *    cb
    );

#endif /* UTL_CIRC_BUF_MSG_H_ */
