#include "ugui_settings_general.h"
#include "ugui_helper.h"
#include "ugui_home.h"
#include "ugui_settings_network_wifi.h"
#include "ugui_settings_about.h"
#include "ugui_msg.h"
#include "ugui_keyboard.h"

#define MAX_SCROLL_OBJECTS      2
#define SCROLL_DOTS_CNT			DOT_GET_NUM_OF_DOTS(MAX_SCROLL_OBJECTS)

#define TXB_ID_HEADER				TXB_ID_0
#define IMG_ID_LOGO					IMG_ID_1
#define IMG_ID_BATTERY				IMG_ID_2
#define IMG_ID_WIFI					IMG_ID_3
#define TXB_ID_SETTINGS				TXB_ID_4
#define BUTTON_ID_BACK 				BTN_ID_5
#define BUTTON_ID_SCROLL_UP			BTN_ID_6
#define BUTTON_ID_SCROLL_DOWN		BTN_ID_7
#define IMG_ID_SCROLL_DOT_FIRST		IMG_ID_8
#define IMG_ID_SCROLL_DOT_LAST		IMG_ID_SCROLL_DOT_FIRST + SCROLL_DOTS_CNT
#define BUTTON_ID_SCROLL_FIRST		IMG_ID_SCROLL_DOT_LAST
#define BUTTON_ID_SCROLL_LAST		BUTTON_ID_SCROLL_FIRST + MAX_SCROLL_OBJECTS
#define MAX_OBJECTS        			BUTTON_ID_SCROLL_LAST

#define BUTTON_ID_WIFI		(BUTTON_ID_SCROLL_FIRST+0)
#define BUTTON_ID_ABOUT		(BUTTON_ID_SCROLL_FIRST+1)

/* Window 1 */
static UG_WINDOW window_1;
static UG_OBJECT obj_buff_wnd_1[MAX_OBJECTS];
static UG_TEXTBOX tb_header;

//buttons
static UG_TEXTBOX tb_settings;
static UG_IMAGE img_battery;
static UG_IMAGE img_wifi;
static UG_BUTTON button_back;
static UG_BUTTON button_scroll_up;
static UG_BUTTON button_scroll_down;
static UG_IMAGE img_scroll_dot[SCROLL_DOTS_CNT];
static UG_BUTTON button_scroll_items[MAX_SCROLL_OBJECTS];
static UG_U16 scroll_offset;
static UG_U16 scroll_offset_current;
static UG_S16 scroll_offset_max;
static UG_AREA scroll_area;
static UG_AREA button_scroll_items_area[MAX_SCROLL_OBJECTS];

static char scroll_item_txt[MAX_SCROLL_OBJECTS][20] =
{
		"Wi-Fi",
		"About"
};


static void touch_callback( UG_MESSAGE* msg );
static void scroll_update();
static void window_update( ugui_system_event_t evt );
static void ugui_beta_msg_callback( int btn );
static void ugui_buzzer_msg_callback( int btn );
static void ugui_units_msg_callback( int btn );
static void ugui_devname_keyboard_callback();

UG_RESULT UG_SettingsGeneralCreate()
{
	/*******************************************************
	 * Window 1
	 *******************************************************/
	UG_WindowCreate( &window_1, obj_buff_wnd_1, MAX_OBJECTS, touch_callback );
	UG_WindowSetForeColor( &window_1,SETTINGS_WINDOW_FORECOLOR );
	UG_WindowSetBackColor( &window_1,SETTINGS_WINDOW_BACKCOLOR );
	UG_WindowSetStyle( &window_1, WINDOW_STYLE);

	UG_S16 width = UG_WindowGetInnerWidth(&window_1);
	UG_S16 height = UG_WindowGetInnerHeight(&window_1);

	/*******************************************************
	 * Window Header
	 ******************************************************/

	// text box - window header
	UG_TextboxCreate( &window_1, &tb_header,TXB_ID_HEADER,0,0,width,WINDOW_HEADER_H);
	UG_TextboxSetFont( &window_1, TXB_ID_HEADER, &TXB_HEADER_FONT );
	UG_TextboxSetForeColor( &window_1, TXB_ID_HEADER, TXB_HEADER_FORECOLOR );
	UG_TextboxSetBackColor( &window_1, TXB_ID_HEADER, TXB_HEADER_BACKCOLOR );
	UG_TextboxSetAlignment( &window_1, TXB_ID_HEADER, ALIGN_CENTER );
	UG_TextboxSetText( &window_1, TXB_ID_HEADER, "Cargt uGUI" );

	// image - battery
	UG_ImageCreate( &window_1, &img_battery, IMG_ID_BATTERY,
			HEADER_BATTERY_XS,
			HEADER_BATTERY_YS,
			HEADER_BATTERY_XE,
			HEADER_BATTERY_YE );
	UG_ImageSetBMP( &window_1, IMG_ID_BATTERY, &HEADER_BATTERY_IMG );
	UG_ImageSetEraseFirstFlag( &window_1, IMG_ID_BATTERY, 1, TXB_HEADER_BACKCOLOR );

	// image - wifi
	UG_ImageCreate( &window_1, &img_wifi, IMG_ID_WIFI,
			HEADER_WIFI_XS,
			HEADER_WIFI_YS,
			HEADER_WIFI_XE,
			HEADER_WIFI_YE );
	UG_ImageSetBMP( &window_1, IMG_ID_WIFI, &HEADER_WIFI_IMG );
	UG_ImageSetEraseFirstFlag( &window_1, IMG_ID_WIFI, 1, TXB_HEADER_BACKCOLOR );

	/*******************************************************
	 * Window Specific items
	 ******************************************************/
	// text box
	UG_TextboxCreate( &window_1, &tb_settings,TXB_ID_SETTINGS,SETTINGS_TXB_XS,SETTINGS_TXB_YS,SETTINGS_TXB_XE,SETTINGS_TXB_YE-1);
	UG_TextboxSetFont( &window_1, TXB_ID_SETTINGS, &SETTINGS_TXB_FONT );
	UG_TextboxSetForeColor( &window_1, TXB_ID_SETTINGS, SETTINGS_TXB_FORECOLOR );
	UG_TextboxSetBackColor( &window_1, TXB_ID_SETTINGS, SETTINGS_TXB_BACKCOLOR );
	UG_TextboxSetAlignment( &window_1, TXB_ID_SETTINGS, SETTINGS_TXB_ALIGNMENT );
	UG_TextboxSetText( &window_1, TXB_ID_SETTINGS, "SETTINGS" );

	//back
	UG_ButtonCreate( &window_1, &button_back, BUTTON_ID_BACK, SETTINGS_BUTTON_BACK_XS,SETTINGS_BUTTON_BACK_YS,SETTINGS_BUTTON_BACK_XE,SETTINGS_BUTTON_BACK_YE);
	UG_ButtonSetFont( &window_1, 		BUTTON_ID_BACK, &SETTINGS_BUTTON_FONT );
	UG_ButtonSetForeColor( &window_1, 	BUTTON_ID_BACK, TXB_HEADER_FORECOLOR );
	UG_ButtonSetBackColor( &window_1, 	BUTTON_ID_BACK, TXB_HEADER_BACKCOLOR );
	UG_ButtonSetStyle( &window_1, 		BUTTON_ID_BACK, SETTINGS_BUTTON_BACK_STYLE);
	UG_ButtonSetImage( &window_1, BUTTON_ID_BACK, BTN_IMG_CENTER, &SETTINGS_BUTTON_BACK_ICON, SETTINGS_BUTTON_BACK_ICON.width, SETTINGS_BUTTON_BACK_ICON.height );

	//Scroll items
	scroll_area.xs = SETTINGS_SCROLL_AREA_XS;
	scroll_area.xe = SETTINGS_SCROLL_AREA_XE;
	scroll_area.ys = SETTINGS_SCROLL_AREA_YS;
	scroll_area.ye = SETTINGS_SCROLL_AREA_YE;

	for(int i=0; i<MAX_SCROLL_OBJECTS; i++)
	{
		button_scroll_items_area[i].xs = scroll_area.xs;
		button_scroll_items_area[i].xe = scroll_area.xe;
		button_scroll_items_area[i].ys = scroll_area.ys + SETTINGS_BUTTON_H * i;
		button_scroll_items_area[i].ye = button_scroll_items_area[i].ys + SETTINGS_BUTTON_H;

		UG_ButtonCreate( &window_1, &button_scroll_items[i], BUTTON_ID_SCROLL_FIRST+i, button_scroll_items_area[i].xs,button_scroll_items_area[i].ys,button_scroll_items_area[i].xe,button_scroll_items_area[i].ye);
		UG_ButtonSetFont( &window_1, 		BUTTON_ID_SCROLL_FIRST+i, &SETTINGS_BUTTON_FONT );
		UG_ButtonSetForeColor( &window_1, 	BUTTON_ID_SCROLL_FIRST+i, SETTINGS_BUTTON_FORECOLOR );
		UG_ButtonSetBackColor( &window_1, 	BUTTON_ID_SCROLL_FIRST+i, SETTINGS_BUTTON_BACKCOLOR );
		UG_ButtonSetStyle( &window_1, 		BUTTON_ID_SCROLL_FIRST+i, SETTINGS_BUTTON_STYLE);
		UG_ButtonSetText( &window_1, 		BUTTON_ID_SCROLL_FIRST+i, scroll_item_txt[i] );
		UG_ButtonSetAlignment( &window_1, 	BUTTON_ID_SCROLL_FIRST+i, ALIGN_CENTER_LEFT );
		UG_ButtonSetImage( &window_1, 		BUTTON_ID_SCROLL_FIRST+i, BTN_IMG_LEFT, NULL, 50, 0 );

		UG_ButtonSetAreaFrame( &window_1,   BUTTON_ID_SCROLL_FIRST+i, scroll_area.xs, scroll_area.ys, scroll_area.xe, scroll_area.ye);
		UG_ButtonHide( &window_1,			BUTTON_ID_SCROLL_FIRST+i);
	}

	for(int i=0; i<SCROLL_ITEMS_PER_PAGE(); i++)
	{
		if(i >= MAX_SCROLL_OBJECTS)
		{
			break;
		}
		UG_ButtonShow( &window_1, BUTTON_ID_SCROLL_FIRST+i);
	}

	scroll_offset = 0;
	scroll_offset_current = 0;
	scroll_offset_max = MAX_SCROLL_OBJECTS * SETTINGS_BUTTON_H - (SETTINGS_SCROLL_AREA_YE - SETTINGS_SCROLL_AREA_YS);

	if(scroll_offset_max > 0)
	{
		//BUTTON_ID_SCROLL_UP
		UG_ButtonCreate( &window_1, &button_scroll_up, BUTTON_ID_SCROLL_UP, SETTINGS_BUTTON_UP_XS,SETTINGS_BUTTON_UP_YS,SETTINGS_BUTTON_UP_XE,SETTINGS_BUTTON_UP_YE);
		UG_ButtonSetFont( &window_1, 		BUTTON_ID_SCROLL_UP, &SETTINGS_BUTTON_FONT );
		UG_ButtonSetForeColor( &window_1, 	BUTTON_ID_SCROLL_UP, SETTINGS_BUTTON_FORECOLOR );
		UG_ButtonSetBackColor( &window_1, 	BUTTON_ID_SCROLL_UP, SETTINGS_BUTTON_BACKCOLOR );
		UG_ButtonSetStyle( &window_1, 		BUTTON_ID_SCROLL_UP, ( BTN_STYLE_TOGGLE_COLORS | BTN_STYLE_NO_BORDERS ));
		UG_ButtonSetImage( &window_1, BUTTON_ID_SCROLL_UP, BTN_IMG_CENTER, &SETTINGS_BUTTON_UP_ICON, SETTINGS_BUTTON_UP_ICON.width, SETTINGS_BUTTON_UP_ICON.height );

		//BUTTON_ID_SCROLL_DOWN
		UG_ButtonCreate( &window_1, &button_scroll_down, BUTTON_ID_SCROLL_DOWN, SETTINGS_BUTTON_DOWN_XS,SETTINGS_BUTTON_DOWN_YS,SETTINGS_BUTTON_DOWN_XE,SETTINGS_BUTTON_DOWN_YE);
		UG_ButtonSetFont( &window_1, 		BUTTON_ID_SCROLL_DOWN, &SETTINGS_BUTTON_FONT );
		UG_ButtonSetForeColor( &window_1, 	BUTTON_ID_SCROLL_DOWN, SETTINGS_BUTTON_FORECOLOR );
		UG_ButtonSetBackColor( &window_1, 	BUTTON_ID_SCROLL_DOWN, SETTINGS_BUTTON_BACKCOLOR );
		UG_ButtonSetStyle( &window_1, 		BUTTON_ID_SCROLL_DOWN, ( BTN_STYLE_TOGGLE_COLORS | BTN_STYLE_NO_BORDERS ));
		UG_ButtonSetImage( &window_1, BUTTON_ID_SCROLL_DOWN, BTN_IMG_CENTER, &SETTINGS_BUTTON_DOWN_ICON, SETTINGS_BUTTON_DOWN_ICON.width, SETTINGS_BUTTON_DOWN_ICON.height );

		for(int i=0; i<SCROLL_DOTS_CNT; i++)
		{
			// image - scroll dot 1
			UG_ImageCreate( &window_1, &img_scroll_dot[i], IMG_ID_SCROLL_DOT_FIRST+i,
					SETTINGS_SCROLL_DOTS_AREA_XS,
					DOT_GET_YS(SCROLL_DOTS_CNT,i),
					SETTINGS_SCROLL_DOTS_AREA_XE,
					DOT_GET_YE(SCROLL_DOTS_CNT,i) );
			UG_ImageSetEraseFirstFlag( &window_1, IMG_ID_SCROLL_DOT_FIRST+i, 1, SETTINGS_WINDOW_BACKCOLOR );
			if(i==0)
			{
				UG_ImageSetBMP( &window_1, IMG_ID_SCROLL_DOT_FIRST+i, &SETTINGS_SCROLL_DOTS_FULL_ICON );
			}
			else
			{
				UG_ImageSetBMP( &window_1, IMG_ID_SCROLL_DOT_FIRST+i, &SETTINGS_SCROLL_DOTS_EMPTY_ICON );
			}
		}
	}

	return UG_RESULT_OK;

}

UG_RESULT UG_SettingsGeneralShow()
{
	UG_WindowShow( &window_1, window_update );
	return UG_RESULT_OK;
}

UG_RESULT UG_SettingsGeneralHide()
{
	UG_WindowHide( &window_1 );
	return UG_RESULT_OK;
}

static void window_update( ugui_system_event_t evt )
{
	switch(evt)
	{
	case UGUI_SYSTEM_EVENT_TICK:
		scroll_update();
		ugui_icon_wifi_update(&window_1,IMG_ID_WIFI);
		ugui_icon_battery_update(&window_1,IMG_ID_BATTERY);
		break;
	default:
		break;
	}
}

static void scroll_update()
{
	if(scroll_offset_current != scroll_offset)
	{
		if(scroll_offset_current - scroll_offset > SETTINGS_SCROLL_STEP_SIZE)
		{
			scroll_offset_current-=SETTINGS_SCROLL_STEP_SIZE;
		}
		else if(scroll_offset_current > scroll_offset )
		{
			scroll_offset_current = scroll_offset;
		}
		else if(scroll_offset - scroll_offset_current > SETTINGS_SCROLL_STEP_SIZE)
		{
			scroll_offset_current+=SETTINGS_SCROLL_STEP_SIZE;
		}
		else
		{
			scroll_offset_current = scroll_offset;
		}

		for(int i=0; i<MAX_SCROLL_OBJECTS; i++)
		{
			button_scroll_items_area[i].xs = scroll_area.xs;
			button_scroll_items_area[i].xe = scroll_area.xe;
			button_scroll_items_area[i].ys = scroll_area.ys + SETTINGS_BUTTON_H * i - scroll_offset_current;
			button_scroll_items_area[i].ye = button_scroll_items_area[i].ys + SETTINGS_BUTTON_H;
			UG_ButtonMove( &window_1, BUTTON_ID_SCROLL_FIRST+i, button_scroll_items_area[i].xs, button_scroll_items_area[i].ys, button_scroll_items_area[i].xe, button_scroll_items_area[i].ye);

			if(( button_scroll_items_area[i].ys < scroll_area.ye && button_scroll_items_area[i].ys > scroll_area.ys )
					|| ( button_scroll_items_area[i].ye > scroll_area.ys && button_scroll_items_area[i].ye < scroll_area.ye) )
			{
				UG_ButtonShow( &window_1, BUTTON_ID_SCROLL_FIRST+i);
			}
			else
			{
				UG_ButtonHide( &window_1, BUTTON_ID_SCROLL_FIRST+i);
			}
		}

		//update dots
		for(int i=0; i<SCROLL_DOTS_CNT; i++)
		{
			if(scroll_offset == scroll_offset_max )
			{
				if( i == (SCROLL_DOTS_CNT-1) )
				{
					UG_ImageSetBMP( &window_1, IMG_ID_SCROLL_DOT_FIRST+i, &SETTINGS_SCROLL_DOTS_FULL_ICON );
				}
				else
				{
					UG_ImageSetBMP( &window_1, IMG_ID_SCROLL_DOT_FIRST+i, &SETTINGS_SCROLL_DOTS_EMPTY_ICON );
				}
			}
			else if(scroll_offset >= SETTINGS_SCROLL_PAGE_SIZE*i && scroll_offset < SETTINGS_SCROLL_PAGE_SIZE*(i+1) )
			{
				UG_ImageSetBMP( &window_1, IMG_ID_SCROLL_DOT_FIRST+i, &SETTINGS_SCROLL_DOTS_FULL_ICON );
			}
			else
			{
				UG_ImageSetBMP( &window_1, IMG_ID_SCROLL_DOT_FIRST+i, &SETTINGS_SCROLL_DOTS_EMPTY_ICON );
			}
		}
	}
}

static void touch_callback( UG_MESSAGE* msg )
{
	if ( msg->type == MSG_TYPE_OBJECT )
	{
		if ( msg->id == OBJ_TYPE_BUTTON && msg->event == OBJ_EVENT_RELEASED )
		{
			switch(msg->sub_id)
			{
			case BUTTON_ID_BACK:
				UG_HomeShow();
				break;
			case BUTTON_ID_SCROLL_UP:
				if( scroll_offset < SETTINGS_SCROLL_PAGE_SIZE )
				{
					scroll_offset = 0;
				}
				else
				{
					scroll_offset -= SETTINGS_SCROLL_PAGE_SIZE;
				}
				break;
			case BUTTON_ID_SCROLL_DOWN:
				scroll_offset += SETTINGS_SCROLL_PAGE_SIZE;
				if( scroll_offset > scroll_offset_max )
				{
					scroll_offset = scroll_offset_max;
				}
				break;
			case BUTTON_ID_WIFI:
				UG_SettingsNetworkWifiShow();
				break;

			case BUTTON_ID_ABOUT:
				UG_SettingsAboutShow();
				break;

			}

		}
	}

}


