/*
 * wifi_scan.h
 *
 *  Created on: Oct 31, 2017
 *      Author: cargt
 */

#ifndef EXAMPLES_WIFI_SCAN_MAIN_WIFI_SCAN_H_
#define EXAMPLES_WIFI_SCAN_MAIN_WIFI_SCAN_H_

#include <stdint.h>
#include "esp_event.h"

typedef enum {
    WIFI_SCAN_FINISHED    =  0,
    WIFI_SCAN_RUNNING     = -1,
    WIFI_SCAN_FAILED      = -2,
    WIFI_SCAN_NOT_STARTED = -3,
} wifi_scan_status_t;

void WS_rxScanCompletionEventCallback(system_event_sta_scan_done_t *scanDoneEvent);

// Scan Result
wifi_ap_record_t* WS_getNetworkInfo(uint8_t  networkItem); // networkItem = idx 0 -> n-1

int8_t WS_isScanComplete(void);

wifi_scan_status_t WS_scanNetworks(
                bool async,
                bool show_hidden,
                bool passive,
                uint32_t max_ms_per_chan
                );


#endif /* EXAMPLES_WIFI_SCAN_MAIN_WIFI_SCAN_H_ */
