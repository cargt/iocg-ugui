#include "ugui.h"
#include "ugui_config.h"

#ifndef __UGUI_HOME_H
#define __UGUI_HOME_H


UG_RESULT UG_HomeCreate();
UG_RESULT UG_HomeShow();
UG_RESULT UG_HomeHide();

#endif //#ifndef __UGUI_HOME_H
