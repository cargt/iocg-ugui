/*
 * task_network.c
 *
 *  Created on: Nov 15, 2017
 *      Author: cargt
 */
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_log.h"
#include "esp_event.h"
#include "esp_timer.h"
#include "main.h"

#include "tft.h"
#include "ledc_backlight.h"


#include "ugui.h"
#include "ugui_home.h"
#include "ugui_msg.h"
#include "ugui_keyboard.h"
#include "ugui_settings_general.h"
#include "ugui_settings_about.h"
#include "ugui_settings_network.h"
#include "ugui_settings_network_ethernet.h"
#include "ugui_settings_network_wifi.h"
#include "ugui_settings_network_wifi_info.h"
#include "ugui_settings_network_wifi_ssid.h"

#include "task_gui.h"

#define TASK_GUI_PRIO                      6U // low/med priority
#define TASK_GUI_STACK_SIZE                0x3000U

static const char *TAG = "gui_task";

EventGroupHandle_t gui_event_group;

/* GUI structure */
UG_GUI gui;

uint16_t* draw_buf;
static uint8_t draw_buf_dirty = 1;

//=================================
// Private function prototypes
//=================================
static void guiTick_Handler( TimerHandle_t xTimer );
static void touch_tick();
static void tft_write_frame();
static void tft_write_bmp(const UG_BMP* bmp, UG_S16 x_off, UG_S16 y_off);
static void tft_write_clear_screen(UG_COLOR color);
static void gui_tsk(void * param);
static void pset(UG_S16 x, UG_S16 y, UG_COLOR color);
static void psetalpha(UG_S16 x, UG_S16 y, UG_COLOR color);
static color_t UG_color_to_colorRGB(UG_COLOR c);
static UG_RESULT _HW_DrawLine( UG_S16 x1, UG_S16 y1, UG_S16 x2, UG_S16 y2, UG_COLOR c );
static UG_RESULT _HW_FillFrame( UG_S16 x1, UG_S16 y1, UG_S16 x2, UG_S16 y2, UG_COLOR c );
static int read_touch(int *x, int* y, uint8_t raw);
static void initDisplay_TFTDriver(void);

//=================================
// Public functions
//=================================
/******************************************************************************
 * gui_event_handler
 * This event handler is to be called by the cen0xf800tral event handler
 * There can only be ONE system event handler
 */
esp_err_t gui_event_handler(void *ctx, system_event_t *event)
{
    switch(event->event_id)
    {
    case SYSTEM_EVENT_SCAN_DONE:
        // set gui event bit
    	ESP_LOGI(TAG,"SYSTEM_EVENT_SCAN_DONE");
        xEventGroupSetBits(gui_event_group, TG_EVNT_BIT_SCAN_COMPLETE);
        break;
    case SYSTEM_EVENT_STA_GOT_IP:
    	xEventGroupSetBits(gui_event_group, TG_EVNT_BIT_WIFI_CONNECT_SUCCESS);
    	break;
    case SYSTEM_EVENT_STA_DISCONNECTED:
    	ESP_LOGE(TAG,"SYSTEM_EVENT_STA_DISCONNECTED reson: %d\n",event->event_info.disconnected.reason);
    	if (	event->event_info.disconnected.reason != SYSTEM_EVENT_STA_LOST_IP
    		&&	event->event_info.disconnected.reason != SYSTEM_EVENT_STA_START)
    	{
    		xEventGroupSetBits(gui_event_group, TG_EVNT_BIT_WIFI_CONNECT_FAIL);
    	}
    	break;
    default:
        break;
    }
    return ESP_OK;
}

int task_gui_init(void)
{
    int ret = 0;

    if (xTaskCreate(gui_tsk,                    /* pointer to the task                      */
                    TAG,                        /* task name for kernel awareness debugging */
                    TASK_GUI_STACK_SIZE,        /* task stack size                          */
                    NULL,                       /* optional task startup argument           */
                    TASK_GUI_PRIO,              /* initial priority                         */
                    NULL                        /* optional task handle to create           */
                    ) != pdPASS)
    {
        assert(0);
    }
    return ret;
}

//goal is to put up splash screen ASAP
void task_gui_init_early(void)
{
	ESP_LOGI(TAG, "gui_tsk:init early");
    draw_buf = heap_caps_malloc(320*240*2,MALLOC_CAP_SPIRAM);//MALLOC_CAP_DMA);
    assert(draw_buf);

    initDisplay_TFTDriver();

    uint8_t disp_rot = DISPLAY_ROTATION_TFT_DRIVER;
    TFT_setRotation(disp_rot);

    tft_write_clear_screen(C_BLACK);
    tft_write_frame();
    ledc_setDutyCycle(0);
}


//=================================
// Private functions
//=================================
static void gui_tsk(void * param)
{
    // Must have initialized a global event handler that calls the one here.
    ESP_LOGI(TAG, "gui_tsk:init");
    gui_event_group = xEventGroupCreate();
    //gui_status_group = xEventGroupCreate();

    /* Init Gui structure and register pixel set driver function */
    UG_Init(&gui, (void(*)(UG_S16,UG_S16,UG_COLOR))pset, (void(*)(UG_S16,UG_S16,UG_COLOR))psetalpha, WIDTH, HEIGHT);


    UG_HomeCreate();
    UG_MsgCreate();
    UG_KeyboardCreate();
    UG_SettingsAboutCreate();
    UG_SettingsGeneralCreate();
    UG_SettingsNetworkCreate();
    UG_SettingsNetworkEthernetCreate();
    UG_SettingsNetworkWifiCreate();
    UG_SettingsNetworkWifiInfoCreate();
    UG_SettingsNetworkWifiSsidCreate();
    UG_HomeShow();
    ledc_setDutyCycle(100);

    ESP_LOGE(TAG,"UG_HomeShow\n");

    static TimerHandle_t    guiTick_timer;

    // Start Systick timer
    ESP_LOGI(TAG, "About to create systick timer");
    guiTick_timer = xTimerCreate( "SYS_TICK", pdMS_TO_TICKS(100), pdTRUE, (void *)0, guiTick_Handler );
    if (pdPASS != xTimerStart( guiTick_timer, 0))
    {
        ESP_LOGI(TAG, "guiTick_timer:start error");
    }

    if(NULL == guiTick_timer)
    {
    ESP_LOGI(TAG, "guiTick_timer=NULL");
    }
    else
    {
        ESP_LOGI(TAG, "guiTick_timer:create ok");
    }

    // task run loop
    while (1)
    {
        EventBits_t xBitTest = ( TG_EVNT_BIT_GUI_TICK
                                | TG_EVNT_BIT_SCAN_COMPLETE
                               );
        EventBits_t uxBits;
        uxBits = xEventGroupWaitBits(
                gui_event_group,        /* The event group being tested. */
                xBitTest,               /* The bits within the event group to wait for. */
                false,                  /* should bits be cleared before returning? */
                false,                  /* wait for all bits? */
                portMAX_DELAY           /* max wait time */
                );

        if (TG_EVNT_BIT_GUI_TICK & uxBits)
        {
            xEventGroupClearBits(gui_event_group, TG_EVNT_BIT_GUI_TICK);
            touch_tick();
            UG_Update(UGUI_SYSTEM_EVENT_TICK);
            if(draw_buf_dirty)
            {
            	tft_write_frame();
            	draw_buf_dirty = 0;
            }
        }

        if (TG_EVNT_BIT_SCAN_COMPLETE & uxBits)
        {
            xEventGroupClearBits(gui_event_group, TG_EVNT_BIT_SCAN_COMPLETE);
            // get ssid string
            ESP_LOGI(TAG, "event:rxd scan complete");
            UG_Update(UGUI_SYSTEM_EVENT_WIFI_SCAN_DONE);
        }
        if (TG_EVNT_BIT_WIFI_CONNECT_SUCCESS & uxBits)
        {
            xEventGroupClearBits(gui_event_group, TG_EVNT_BIT_WIFI_CONNECT_SUCCESS);
            ESP_LOGI(TAG, "event: UGUI_SYSTEM_EVENT_WIFI_CONNECT_SUCCESS");
            UG_Update(UGUI_SYSTEM_EVENT_WIFI_CONNECT_SUCCESS);
        }
        if (TG_EVNT_BIT_WIFI_CONNECT_FAIL & uxBits)
        {
            xEventGroupClearBits(gui_event_group, TG_EVNT_BIT_WIFI_CONNECT_FAIL);
            ESP_LOGI(TAG, "event: UGUI_SYSTEM_EVENT_WIFI_CONNECT_FAIL");
            UG_Update(UGUI_SYSTEM_EVENT_WIFI_CONNECT_FAIL);
        }

        vTaskDelay(100 / portTICK_PERIOD_MS);
    }
}

static void guiTick_Handler( TimerHandle_t xTimer )
{
	static int pwr_key_cnt = 0;

    // Set event bit
    BaseType_t xResult;
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    xResult = xEventGroupSetBitsFromISR(
                    gui_event_group,
                   TG_EVNT_BIT_GUI_TICK,
                    &xHigherPriorityTaskWoken);
    if ((pdFAIL != xResult) && (pdTRUE == xHigherPriorityTaskWoken))
    {
        portYIELD_FROM_ISR();
    }

}

static void tft_write_frame()
{

	disp_select();

	setHomePixel(0);

	for(int y=0;y<240;y++)
		for(int x=0;x<320;x+=32)
		{
			drawNextPixel32(x, y, &draw_buf[y*320+x], 0);

		}

	disp_deselect();
}

static void tft_write_bmp(const UG_BMP* bmp, UG_S16 x_off, UG_S16 y_off)
{
   UG_S16 x,y,xs;
   UG_U8 r,g,b;
   UG_U16* p;
   UG_U32* p32;
   UG_U16 tmp;
   UG_COLOR c;

    p32 = (UG_U32*)bmp->p;

    xs = x_off;
	for(y=0;y<bmp->height;y++)
	{
		x_off = xs;
		for(x=0;x<bmp->width;x++)
		{
			UG_U8* c8 = p32++;
			c = (c8[3]<<24) | (c8[2]<<0) | (c8[1]<<8) | (c8[0]<<16);
			psetalpha( x_off , y_off , c );
			x_off++;
		}
		y_off++;
	}

}

static void tft_write_clear_screen(UG_COLOR color)
{
	for(int y=0;y<DEFAULT_TFT_DISPLAY_HEIGHT;y++)
	{
		for(int x=0;x<DEFAULT_TFT_DISPLAY_WIDTH;x++)
		{
			psetalpha( x , y , color );
		}
	}
}

// ============================================================================
/* Pixel set function */

static void pset(UG_S16 x, UG_S16 y, UG_COLOR color)
{
	color |= 0xff000000;
	psetalpha(x,y,color);
}

static void psetalpha(UG_S16 x, UG_S16 y, UG_COLOR color)
{
	uint32_t addr;
	uint32_t a,r,g,b;
	uint16_t col_back;
	uint32_t r_back,g_back,b_back;
	uint8_t* color_byte_read;
	uint16_t color_read;
	uint8_t* color_byte;
	uint16_t color_write;
	int x_buf;
	int y_buf;

#if TASK_GUI_PORTRAIT == TASK_GUI_ORIENTATION
	x_buf = y;
	y_buf = TASK_GUI_DISPLAY_HEIGHT - x - 1;
#elif TASK_GUI_PORTRAIT_FLIPPED == TASK_GUI_ORIENTATION
	x_buf = TASK_GUI_DISPLAY_WIDTH - y - 1;
	y_buf = x;
#elif TASK_GUI_LANDSCAPE == TASK_GUI_ORIENTATION
	x_buf = x;
	y_buf = y;
#else// TASK_GUI_LANDSCAPE_FLIPPED == TASK_GUI_ORIENTATION
	x_buf = TASK_GUI_DISPLAY_WIDTH - x - 1;
	y_buf = TASK_GUI_DISPLAY_HEIGHT - y - 1;
#endif

	if( x_buf<0 ) return;
	if( y_buf<0 ) return;
	if( x_buf> (TASK_GUI_DISPLAY_WIDTH-1) ) return;
	if( y_buf> (TASK_GUI_DISPLAY_HEIGHT-1) ) return;

	color_byte_read = (uint8_t*)&draw_buf[y_buf*TASK_GUI_DISPLAY_WIDTH+x_buf]; 	//read current value
	color_read = color_byte_read[1] | (color_byte_read[0]<<8); 	//swap bytes
	r_back = (color_read & 0xf800)>>8;					//split background into colors
	g_back = (color_read & 0x07e0)>>3;
	b_back = (color_read & 0x001f)<<3;

	a = color>>24 & 0xFF;								//split new value into colors
	r = color>>16 & 0xFF;
	g = color>>8 & 0xFF;
	b = color & 0xFF;

	if(a<0xff)
	{
		r = ((r*a)>>8) + (r_back*(0x100-a)>>8);
		g = ((g*a)>>8) + (g_back*(0x100-a)>>8);
		b = ((b*a)>>8) + (b_back*(0x100-a)>>8);
	}
	r >>= 3;
	g >>= 2;
	b >>= 3;

	color_write = b | (g<<5) | (r<<11);

	color_byte = (UG_COLOR*)&color_write;

    draw_buf[y_buf*TASK_GUI_DISPLAY_WIDTH+x_buf]=color_byte[1] | (color_byte[0]<<8);
    draw_buf_dirty = 1;

}

static color_t UG_color_to_colorRGB(UG_COLOR c)
{
    color_t cRGB;
    cRGB.r = (uint8_t)((c & 0x00FF0000) >> 16);
    cRGB.g = (uint8_t)((c & 0x0000FF00) >> 8);
    cRGB.b = (uint8_t)((c & 0x000000FF));
    return cRGB;
}

/* Hardware accelerator for UG_DrawLine */
static UG_RESULT _HW_DrawLine( UG_S16 x1, UG_S16 y1, UG_S16 x2, UG_S16 y2, UG_COLOR c )
{
//   TFT_drawLine(x1, y1, x2, y2, UG_color_to_colorRGB(c));
   return UG_RESULT_OK;
}

/* Hardware accelerator for UG_FillFrame */
static UG_RESULT _HW_FillFrame( UG_S16 x1, UG_S16 y1, UG_S16 x2, UG_S16 y2, UG_COLOR c )
{
   int16_t x, y, w, h;
   // convert to delta y coordinates.
   if (x2 >= x1)
   {
      x = x1;
      w = (x2 - x1);
   }
   else
   {
      x = x2;
      w = (x1 - x2);
   }

   if (y2 >= y1)
   {
      y = y1;
      h = (y2 - y1);
   }
   else
   {
      y = y2;
      h = (y1 - y2);
   }

//   TFT_fillRect(x, y, w, h, UG_color_to_colorRGB(c));
   return UG_RESULT_OK;
}


void initDisplay_TFTDriver(void)
{
    // ========  PREPARE DISPLAY INITIALIZATION  =========

    esp_err_t ret;

    // === SET GLOBAL VARIABLES ==========================

    // ===================================================
    // ==== Set display type                         =====
    tft_disp_type = DEFAULT_DISP_TYPE;
    //tft_disp_type = DISP_TYPE_ILI9341;
    //tft_disp_type = DISP_TYPE_ILI9488;
    //tft_disp_type = DISP_TYPE_ST7735B;
    // ===================================================

    // ===================================================
    // === Set display resolution if NOT using default ===
    // === DEFAULT_TFT_DISPLAY_WIDTH &                 ===
    // === DEFAULT_TFT_DISPLAY_HEIGHT                  ===
    _width = DEFAULT_TFT_DISPLAY_WIDTH;  // smaller dimension
    _height = DEFAULT_TFT_DISPLAY_HEIGHT; // larger dimension
    //_width = 128;  // smaller dimension
    //_height = 160; // larger dimension
    // ===================================================

    // ===================================================
    // ==== Set maximum spi clock for display read    ====
    //      operations, function 'find_rd_speed()'    ====
    //      can be used after display initialization  ====
    max_rdclock = 8000000;
    // ===================================================

    // ====================================================================
    // === Pins MUST be initialized before SPI interface initialization ===
    // ====================================================================
    TFT_PinsInit();

    // ====  CONFIGURE SPI DEVICES(s)  ====================================================================================

    spi_lobo_device_handle_t spi;

    spi_lobo_bus_config_t buscfg={
        .miso_io_num=PIN_NUM_MISO,              // set SPI MISO pin
        .mosi_io_num=PIN_NUM_MOSI,              // set SPI MOSI pin
        .sclk_io_num=PIN_NUM_CLK,               // set SPI CLK pin
        .quadwp_io_num=-1,
        .quadhd_io_num=-1,
        .max_transfer_sz = 6*1024,
    };
    spi_lobo_device_interface_config_t devcfg={
        .clock_speed_hz=26000000,                // Initial clock out at 8 MHz
        .mode=0,                                // SPI mode 0
        .spics_io_num=-1,                       // we will use external CS pin
        .spics_ext_io_num=PIN_NUM_CS,           // external CS pin
        .flags=SPI_DEVICE_HALFDUPLEX,           // ALWAYS SET  to HALF DUPLEX MODE!! for display spi
    };

#if USE_TOUCH == TOUCH_TYPE_XPT2046
    spi_lobo_device_handle_t tsspi = NULL;

    spi_lobo_device_interface_config_t tsdevcfg={
        .clock_speed_hz=2500000,                //Clock out at 2.5 MHz
        .mode=0,                                //SPI mode 0
        .spics_io_num=PIN_NUM_TCS,              //Touch CS pin
        .spics_ext_io_num=-1,                   //Not using the external CS
        //.command_bits=8,                        //1 byte command
    };
#elif USE_TOUCH == TOUCH_TYPE_STMPE610
    spi_lobo_device_handle_t tsspi = NULL;

    spi_lobo_device_interface_config_t tsdevcfg={
        .clock_speed_hz=1000000,                //Clock out at 1 MHz
        .mode=STMPE610_SPI_MODE,                //SPI mode 0
        .spics_io_num=PIN_NUM_TCS,              //Touch CS pin
        .spics_ext_io_num=-1,                   //Not using the external CS
        .flags = 0,
    };
#elif USE_TOUCH == TOUCH_TYPE_SX8652
    spi_lobo_device_handle_t tsspi = NULL;

    spi_lobo_device_interface_config_t tsdevcfg={
        .clock_speed_hz=1000000,                //Clock out at 1 MHz
        .mode=SX8652_SPI_MODE,                	//SPI mode
		.cs_ena_pretrans = SX8652_SPI_CS_ENA_PRETRANS,
        .spics_io_num=-1,              //Touch CS pin
        .spics_ext_io_num=PIN_NUM_TCS,                   //Not using the external CS
        .flags = 0,
    };
#endif

    // ====================================================================================================================


    vTaskDelay(500 / portTICK_RATE_MS);
    ESP_LOGE(TAG,"\r\n==============================\r\n");
    ESP_LOGE(TAG,"GUI Display Setup\r\n");
    ESP_LOGE(TAG,"==============================\r\n");
    ESP_LOGE(TAG,"Pins used: miso=%d, mosi=%d, sck=%d, cs=%d\r\n", PIN_NUM_MISO, PIN_NUM_MOSI, PIN_NUM_CLK, PIN_NUM_CS);
#if USE_TOUCH > TOUCH_TYPE_NONE
    ESP_LOGE(TAG," Touch CS: %d\r\n", PIN_NUM_TCS);
#endif
    ESP_LOGE(TAG,"==============================\r\n\r\n");

    // ==================================================================
    // ==== Initialize the SPI bus and attach the LCD to the SPI bus ====

    ret=spi_lobo_bus_add_device(SPI_BUS, &buscfg, &devcfg, &spi);
    assert(ret==ESP_OK);
    ESP_LOGE(TAG,"SPI: display device added to spi bus (%d)\r\n", SPI_BUS);
    disp_spi = spi;

    // ==== Test select/deselect ====
    ret = spi_lobo_device_select(spi, 1);
    assert(ret==ESP_OK);
    ret = spi_lobo_device_deselect(spi);
    assert(ret==ESP_OK);

    ESP_LOGE(TAG,"SPI: attached display device, speed=%u\r\n", spi_lobo_get_speed(spi));
    ESP_LOGE(TAG,"SPI: bus uses native pins: %s\r\n", spi_lobo_uses_native_pins(spi) ? "true" : "false");

#if USE_TOUCH > TOUCH_TYPE_NONE
    // =====================================================
    // ==== Attach the touch screen to the same SPI bus ====

    ret=spi_lobo_bus_add_device(SPI_BUS, &buscfg, &tsdevcfg, &tsspi);
    assert(ret==ESP_OK);
    ESP_LOGE(TAG,"SPI: touch screen device added to spi bus (%d)\r\n", SPI_BUS);
    ts_spi = tsspi;

    // ==== Test select/deselect ====
    ret = spi_lobo_device_select(tsspi, 1);
    assert(ret==ESP_OK);
    ret = spi_lobo_device_deselect(tsspi);
    assert(ret==ESP_OK);

    ESP_LOGE(TAG,"SPI: attached TS device, speed=%u\r\n", spi_lobo_get_speed(tsspi));
#endif

    // ================================
    // ==== Initialize the Display ====
    TFT_display_init();
    ESP_LOGE(TAG,"SPI: display init...\r\n");
	sx8652_Init(WIDTH,HEIGHT);
	vTaskDelay(10 / portTICK_RATE_MS);
	uint32_t tver = sx8652_getID();
	ESP_LOGE(TAG,"SX8652 touch initialized, ver: %04x - %02x\r\n", tver >> 8, tver & 0xFF);

    // ==== Set SPI clock used for display operations ====
    spi_lobo_set_speed(spi, DEFAULT_SPI_CLOCK);
    ESP_LOGE(TAG,"SPI: Changed speed to %u\r\n", spi_lobo_get_speed(spi));

    gray_scale = 0;

    switch (tft_disp_type) {
        case DISP_TYPE_ILI9341:
            ESP_LOGE(TAG,"ILI9341");
            break;
        case DISP_TYPE_ILI9488:
            ESP_LOGE(TAG,"ILI9488");
            break;
        case DISP_TYPE_ST7789V:
            ESP_LOGE(TAG,"ST7789V");
            break;
        case DISP_TYPE_ST7735:
            ESP_LOGE(TAG,"ST7735");
            break;
        case DISP_TYPE_ST7735R:
            ESP_LOGE(TAG,"ST7735R");
            break;
        case DISP_TYPE_ST7735B:
            ESP_LOGE(TAG,"ST7735B");
            break;
        default:
            ESP_LOGE(TAG,"Unknown");
    }

}

static void touch_tick()
{
#if USE_TOUCH
	int tx, ty;

	if (TFT_read_touch(&tx, &ty, 0))
	{
		UG_TouchUpdate( (UG_S16)tx, (UG_S16)ty, TOUCH_STATE_PRESSED );
	}
	else
	{
		UG_TouchUpdate( -1, -1, TOUCH_STATE_RELEASED );
	}

#endif
}


