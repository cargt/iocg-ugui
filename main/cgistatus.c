/*
 * cgistatus.c
 *
 *  Created by Graham Taylor-Jones on Sep 25, 2018
 *  Copyright (c) 2018 Cargt, LLC. All rights reserved.
 *
 */


#include <libesphttpd/esp.h>

#include "esp_log.h"
#include "time.h"
#include "cgistatus.h"

#include "task_can.h"

#define TAG "CGI-STAT"

#define minval( X , Y ) ( ( (X) < (Y) ) ? (X) : (Y) )

static msg_frame_t      cgi_can_msg_list[NUM_CAN_FRAME_LOG_ENTRIES];
static device_status_t  cgi_device_status_list[NUM_DEVICE_STATUS_ENTRIES];

//This CGI is called from the bit of AJAX-code.
//It will get the latest CAN packets received
//The result is embedded in a bit of JSON parsed by the javascript.
static int num_can_status = 0;
CgiStatus ICACHE_FLASH_ATTR cgiGetCANStatus(HttpdConnData *connData) {
    int pos=(int)connData->cgiData;
    int len;

    printf("====================pos = %d\n",pos);
    ESP_LOGI(TAG, "cgiGetCANStatus:");
    char buff[1024];

    if( pos <= 0 )
    {
        // First CGI pass - Set up data for CGI to pull from
        num_can_status = task_can_get_device_status( &cgi_device_status_list[0], sizeof cgi_device_status_list / sizeof cgi_device_status_list[0] );


        if( num_can_status <= 0 )
        {
            int returnCode;
            if( num_can_status < 0 )
            {
                returnCode = 404;
                len=sprintf(buff, "{\n}\n");
            }
            else
            {
                returnCode = 200;
                len=sprintf(buff,
                        "{\n\"status\" : {" \
                        "\"data\":[{\"timestamp\":\"0\",\"serial\":\"0\",\"devType\":\"\",\"comments\":\"NO MESSAGES YET\"}]" \
                        "\n}\n}\n"
                        );
            }
            httpdStartResponse(connData, returnCode);
            httpdHeader(connData, "Content_type", "text/json");
            httpdEndHeaders(connData);
            ESP_LOGI(TAG, "status1:num:%d,len:%d,%s", num_can_status, len, buff );
            httpdSend(connData, buff, len);
            return HTTPD_CGI_DONE;
        }
        else
        {
            // Send JSON start
            httpdStartResponse(connData, 200);
            httpdHeader(connData, "Content_type", "text/json");
            httpdEndHeaders(connData);

            len=sprintf(buff, "{\n" \
                    "\"status\" : {" \
                    "\"data\":[" \
                    );
            //ESP_LOGI(TAG, "status start:num:%d,len:%d,%s", num_can_status, len, buff);
            ESP_LOGI(TAG, "status start:num:%d,len:%d", num_can_status, len);
            httpdSend(connData, buff, len);
            // Set first line for next CGI call
            connData->cgiData=(void *)1;
            return HTTPD_CGI_MORE;
        }
    }
    else
    {
        // Follow on CGI calls

        // start of new buffer - reset message string
        buff[0] = '\0';

#define MAX_NUM_OF_CGI_STATUS_LINES    2 // Send in chunks

        // Calculate positions on page and positions in array
        // Page lines "pos" are from 1 UP to number of msgs; Array Idx is 0 UP to (number of msgs -1)

        // Calculate number of lines to send: MAX or less
        int endPosValue = minval( ( (pos-1) + MAX_NUM_OF_CGI_STATUS_LINES ), num_can_status );
        int startLower_idx = pos - 1;
        int endUpper_idx = endPosValue - 1;

        for( int i = startLower_idx; i <= endUpper_idx; i++ )
        {
            struct tm timeinfo = { 0 };
            localtime_r((time_t *)&cgi_device_status_list[i].timestamp, &timeinfo);
            char strftime_buf[sizeof "2011-10-08T07:07:09Z"];
            strftime(strftime_buf, sizeof strftime_buf, "%FT%TZ", &timeinfo);

            char tempBuf[200];
            sprintf(tempBuf,
                    "%s" \
                    "\"timestamp\":\"%s\"" \
                    ",\"serial\":\"%07d\"" \
                    ",\"devType\":\"%02d\"" \
                    ",\"comments\":\"%s\"}"
                    ,( 0 == i ) ? "\n{": ",\n{"
                    ,strftime_buf
                    ,cgi_device_status_list[i].serial
                    ,cgi_device_status_list[i].brd_type
                    ,cgi_device_status_list[i].comments
                    );
            strcat(buff, tempBuf);
        }

        if( endPosValue >= num_can_status )
        {
            strcat(buff,"\n]\n}\n}\n");
            len=strlen(buff);
            //ESP_LOGI(TAG, "status done:pos:%d,(idx:%d,%d),len:%d,%s", endPosValue, startLower_idx, endUpper_idx, len, buff );
            ESP_LOGI(TAG, "status done:pos:%d,len:%d", endPosValue, len );
            httpdSend(connData, buff, len);
            num_can_status = 0;
            return HTTPD_CGI_DONE;
        }
        else
        {
            len=strlen(buff);
            //ESP_LOGI(TAG, "status cont:pos:%d,(idx:%d,%d),len:%d,%s", endPosValue, startLower_idx, endUpper_idx, len, buff );
            ESP_LOGI(TAG, "status cont:pos:%d,len:%d", endPosValue, len );
            httpdSend(connData, buff, len);
            connData->cgiData=(void *)( endPosValue + 1 );
            return HTTPD_CGI_MORE;
        }
    }
} /* cgiGetCANStatus */


char jsonMsgBuff[1024];
//This CGI is called from the bit of AJAX-code.
//It will get the latest CAN packets received
//The result is embedded in a bit of JSON parsed by the javascript.
static int num_can_msgs = 0;
CgiStatus ICACHE_FLASH_ATTR cgiGetCANMessages(HttpdConnData *connData) {
    int pos=(int)connData->cgiData;
    int len;

    printf("====================pos = %d\n",pos);
    ESP_LOGI(TAG, "cgiGetCANMessages:");

    if( pos <= 0 )
    {
        // First CGI pass - Set up data for CGI to pull from
        num_can_msgs = task_can_get_message_list( &cgi_can_msg_list[0], sizeof cgi_can_msg_list / sizeof cgi_can_msg_list[0] );
        if( num_can_msgs <= 0 )
        {
            int returnCode;
            if( num_can_msgs < 0 )
            {
                returnCode = 404;
                len=sprintf(jsonMsgBuff, "{\n}\n");
            }
            else
            {
                returnCode = 200;
                len=sprintf(jsonMsgBuff,
                        "{\n\"messages\" : {" \
                        "\"data\":[{\"timestamp\":\"0\",\"pktCount\":\"0\",\"serial\":\"0\",\"msgType\":\"0\",\"bytesText\":\"0\",\"comments\":\"NO MESSAGES YET\"}]" \
                        "\n}\n}\n"
                        );
            }
            httpdStartResponse(connData, returnCode);
            httpdHeader(connData, "Content_type", "text/json");
            httpdEndHeaders(connData);
            ESP_LOGI(TAG, "msgs1:num:%d,len:%d,%s", num_can_msgs, len, jsonMsgBuff);
            httpdSend(connData, jsonMsgBuff, len);
            return HTTPD_CGI_DONE;
        }
        else
        {
            // Send JSON start
            httpdStartResponse(connData, 200);
            httpdHeader(connData, "Content_type", "text/json");
            httpdEndHeaders(connData);

            len=sprintf(jsonMsgBuff, "{\n" \
                    "\"messages\" : {" \
                    "\"data\":[" \
                    );
            //ESP_LOGI(TAG, "msgs start:num:%d,len:%d,%s", num_can_msgs, len, jsonMsgBuff);
            ESP_LOGI(TAG, "msgs start:num:%d,len:%d", num_can_msgs, len);
            httpdSend(connData, jsonMsgBuff, len);
            // Set first line for next CGI call
            connData->cgiData=(void *)1;
            return HTTPD_CGI_MORE;
        }
    }
    else
    {
        // Follow on CGI calls

        // start of new buffer - reset message string
        jsonMsgBuff[0] = '\0';

#define MAX_NUM_OF_CGI_MESSAGE_LINES    5 // Send in chunks

        // Calculate positions on page and positions in array
        // Buffer is oldest to newest so get newest and print newest to oldest
        // Page lines "pos" are from 1 UP to number of msgs; Array Idx is (number of msgs -1) DOWN to 0

        // Calculate number of lines to send: MAX or less
        int endPosValue = minval( ( (pos-1) + MAX_NUM_OF_CGI_MESSAGE_LINES ), num_can_msgs );
        int startUpper_idx = num_can_msgs - pos;
        int endLower_idx = num_can_msgs - endPosValue;

        for( int i = startUpper_idx; i >= endLower_idx; i-- )
        {
            struct tm timeinfo = { 0 };
            localtime_r((time_t *)&cgi_can_msg_list[i].timestamp, &timeinfo);
            char strftime_buf[sizeof "2011-10-08T07:07:09Z"];
            strftime(strftime_buf, sizeof strftime_buf, "%FT%TZ", &timeinfo);

            char tempBuf[200];
            sprintf(tempBuf,
                    "%s" \
                    "\"timestamp\":\"%s\"" \
                    ",\"pktCount\":\"%d\"" \
                    ",\"serial\":\"%07d\"" \
                    ",\"msgType\":\"0x%02X\""
                    ",\"bytesText\":\"Hex:%02X,%02X,%02X,%02X,%02X,%02X,%02X,%02X\""
                    ",\"comments\":\"\"}"
                    ,( ( 1 == pos ) && ( startUpper_idx == i ) ) ? "\n{": ",\n{" /* only on very first should , be ommitted */
                    ,strftime_buf
                    ,cgi_can_msg_list[i].pktCount
                    ,( cgi_can_msg_list[i].can_frame.MsgID & CAN_SERIAL_NUMBER_MASK )
                    ,( (cgi_can_msg_list[i].can_frame.MsgID & CMD_ID_MASK ) >> CMD_ID_SHIFT_BITS )
                    ,cgi_can_msg_list[i].can_frame.data.u8[0]
                    ,cgi_can_msg_list[i].can_frame.data.u8[1]
                    ,cgi_can_msg_list[i].can_frame.data.u8[2]
                    ,cgi_can_msg_list[i].can_frame.data.u8[3]
                    ,cgi_can_msg_list[i].can_frame.data.u8[4]
                    ,cgi_can_msg_list[i].can_frame.data.u8[5]
                    ,cgi_can_msg_list[i].can_frame.data.u8[6]
                    ,cgi_can_msg_list[i].can_frame.data.u8[7]
                    );
            strcat(jsonMsgBuff, tempBuf);
        }

        if( endPosValue >= num_can_msgs )
        {
            strcat(jsonMsgBuff,"\n]\n}\n}\n");
            len=strlen(jsonMsgBuff);
            //ESP_LOGI(TAG, "msgs done:pos:%d,len:%d,%s", endPosValue, len, jsonMsgBuff);
            ESP_LOGI(TAG, "msgs done:pos:%d,len:%d", endPosValue, len);
            httpdSend(connData, jsonMsgBuff, len);
            num_can_msgs = 0;
            return HTTPD_CGI_DONE;
        }
        else
        {
            len=strlen(jsonMsgBuff);
            //ESP_LOGI(TAG, "msgs cont:pos:%d,len:%d,%s", endPosValue, len, jsonMsgBuff);
            ESP_LOGI(TAG, "msgs cont:pos:%d,len:%d", endPosValue, len);
            httpdSend(connData, jsonMsgBuff, len);
            connData->cgiData = (void *)( endPosValue + 1 );
            return HTTPD_CGI_MORE;
        }
    }
} /* cgiGetCANMessages */
