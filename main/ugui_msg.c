#include <string.h>
#include "ugui_msg.h"
#include "ugui_helper.h"
#include "ugui_home.h"
#include "ugui_settings_network_wifi_ssid.h"

//layout
#define HOME_WINDOW_FORECOLOR 	WINDOW_FORECOLOR
#define HOME_WINDOW_BACKCOLOR 	WINDOW_BACKCOLOR
#define HOME_WINDOW_STYLE 		WINDOW_STYLE
#define BUTTON_FRAME_RADIUS		10

#define TXB_ID_HEADER				TXB_ID_0
#define IMG_ID_LOGO					IMG_ID_1
#define IMG_ID_BATTERY				IMG_ID_2
#define IMG_ID_WIFI					IMG_ID_3
#define BUTTON_ID_MSG 				BTN_ID_4
#define BUTTON_ID_SINGLE			BTN_ID_5
#define BUTTON_ID_DUAL_1			BTN_ID_6
#define BUTTON_ID_DUAL_2			BTN_ID_7
#define MAX_OBJECTS        			8

static UG_WINDOW window_1;
static UG_OBJECT obj_buff_wnd_1[MAX_OBJECTS];

#define BTN_FORECOLOR 		C_BLACK
#define BTN_BACKCOLOR 		C_LIGHT_GRAY
#define BTN_ALT_FORECOLOR 	C_BLACK
#define BTN_ALT_BACKCOLOR 	CARGT_GREEN
#define BTN_FONT			FONT_24
#define BTN2_FONT			FONT_16
#define BTN3_FONT			FONT_12
#define BTN_FONT_ALIGMENT 	ALIGN_TOP_CENTER
#define BTN2_FONT_ALIGMENT 	ALIGN_CENTER
#define BTN_W	280
#define BTN_H	130
#define BTN_OFFSET_Y	10

#define BTN_1_XS					( (UG_WindowGetInnerWidth(&window_1)-BTN_W) / 2 )
#define BTN_1_XE					( BTN_W+BTN_1_XS )
#define BTN_1_YS					( WINDOW_HEADER_H + BTN_OFFSET_Y )
#define BTN_1_YE					( BTN_H + BTN_1_YS )

#define BTN_W_S	BTN_W
#define BTN_H_S	50
#define BTN_OFFSET_Y_S	10
#define BTN_S_XS					BTN_1_XS
#define BTN_S_XE					BTN_1_XE
#define BTN_S_YS					BTN_1_YE + BTN_OFFSET_Y_S
#define BTN_S_YE					( BTN_H_S + BTN_S_YS )

#define BTN_OFFSET_X	10
#define BTN_W_D	((BTN_W_S - BTN_OFFSET_X)/2)
#define BTN_H_D	BTN_H_S

#define BTN_D1_XS					BTN_1_XS
#define BTN_D1_XE					BTN_1_XS + BTN_W_D
#define BTN_D1_YS					BTN_S_YS
#define BTN_D1_YE					BTN_S_YE
#define BTN_D2_XS					BTN_D1_XE + BTN_OFFSET_X
#define BTN_D2_XE					BTN_D2_XS + BTN_W_D
#define BTN_D2_YS					BTN_S_YS
#define BTN_D2_YE					BTN_S_YE


static int animation_cnt = 0;
#define MSG_ICON_WORKING_CNT 6
static const UG_IMAGE* msg_icon_working[MSG_ICON_WORKING_CNT] =
{
		&img_wait_state_0,
		&img_wait_state_1,
		&img_wait_state_2,
		&img_wait_state_3,
		&img_wait_state_4,
		&img_wait_state_5
};



static UG_TEXTBOX tb_header;
static UG_IMAGE img_battery;
static UG_IMAGE img_wifi;
static UG_BUTTON button_msg;
static UG_BUTTON button_single;
static UG_BUTTON button_dual_1;
static UG_BUTTON button_dual_2;

static UG_S32 			msg_timeout = NULL;
static UG_S32			msg_timeout_cnt = NULL;
static UG_Msg_Callback 	msg_cb = NULL;
static ugui_msg_icon_t	msg_icon = UGUI_MSG_ICON_NONE;
static ugui_system_event_t msg_evt_mask = 0;

//prototypes
static void update_callback( ugui_system_event_t evt );
static void touch_callback( UG_MESSAGE* msg );


UG_RESULT UG_MsgCreate()
{
	/*******************************************************
	 * Window 1
	 *******************************************************/
	UG_WindowCreate( &window_1, obj_buff_wnd_1, MAX_OBJECTS, touch_callback );
	UG_WindowSetForeColor( &window_1,HOME_WINDOW_FORECOLOR );
	UG_WindowSetBackColor( &window_1,HOME_WINDOW_BACKCOLOR );
	UG_WindowSetStyle( &window_1, HOME_WINDOW_STYLE);

	UG_S16 width = UG_WindowGetInnerWidth(&window_1);
	UG_S16 height = UG_WindowGetInnerHeight(&window_1);

	/*******************************************************
	 * Window Header
	 ******************************************************/

	// text box - window header
	UG_TextboxCreate( &window_1, &tb_header,TXB_ID_HEADER,0,0,width,WINDOW_HEADER_H);
	UG_TextboxSetFont( &window_1, TXB_ID_HEADER, &TXB_HEADER_FONT );
	UG_TextboxSetForeColor( &window_1, TXB_ID_HEADER, TXB_HEADER_FORECOLOR );
	UG_TextboxSetBackColor( &window_1, TXB_ID_HEADER, TXB_HEADER_BACKCOLOR );
	UG_TextboxSetAlignment( &window_1, TXB_ID_HEADER, ALIGN_CENTER );
	UG_TextboxSetText( &window_1, TXB_ID_HEADER, "Cargt uGUI" );

	// image - battery
	UG_ImageCreate( &window_1, &img_battery, IMG_ID_BATTERY,
			HEADER_BATTERY_XS,
			HEADER_BATTERY_YS,
			HEADER_BATTERY_XE,
			HEADER_BATTERY_YE );
	UG_ImageSetBMP( &window_1, IMG_ID_BATTERY, &HEADER_BATTERY_IMG );
	UG_ImageSetEraseFirstFlag( &window_1, IMG_ID_BATTERY, 1, TXB_HEADER_BACKCOLOR );

	// image - wifi
	UG_ImageCreate( &window_1, &img_wifi, IMG_ID_WIFI,
			HEADER_WIFI_XS,
			HEADER_WIFI_YS,
			HEADER_WIFI_XE,
			HEADER_WIFI_YE );
	UG_ImageSetBMP( &window_1, IMG_ID_WIFI, &HEADER_WIFI_IMG );
	UG_ImageSetEraseFirstFlag( &window_1, IMG_ID_WIFI, 1, TXB_HEADER_BACKCOLOR );

	/*******************************************************
	 * Window Specific items
	 ******************************************************/
	//BUTTON - BUTTON_ID_MSG
	UG_ButtonCreate( &window_1, &button_msg, BUTTON_ID_MSG,
			BTN_1_XS,
			BTN_1_YS,
			BTN_1_XE,
			BTN_1_YE );
	UG_ButtonSetForeColor( &window_1, 	BUTTON_ID_MSG, BTN_FORECOLOR );
	UG_ButtonSetBackColor( &window_1, 	BUTTON_ID_MSG, BTN_BACKCOLOR );
	UG_ButtonSetAlternateForeColor( &window_1,
										BUTTON_ID_MSG, BTN_ALT_FORECOLOR );
	UG_ButtonSetAlternateBackColor( &window_1,
										BUTTON_ID_MSG, BTN_ALT_BACKCOLOR );
	UG_ButtonSetStyle( &window_1, 		BUTTON_ID_MSG, BUTTON_STYLE);
	UG_ButtonSetFrameRadius( &window_1, BUTTON_ID_MSG, BUTTON_FRAME_RADIUS );
	UG_ButtonSetAlignment( &window_1, 	BUTTON_ID_MSG, BTN_FONT_ALIGMENT);
	UG_ButtonSetFont( &window_1, 		BUTTON_ID_MSG, &BTN_FONT );
	UG_ButtonSetAlignment2( &window_1, 	BUTTON_ID_MSG, BTN2_FONT_ALIGMENT);
	UG_ButtonSetFont2( &window_1, 		BUTTON_ID_MSG, &BTN3_FONT, BTN_ALT_FORECOLOR );

	//BUTTON - BUTTON_ID_SINGLE
	UG_ButtonCreate( &window_1, &button_single, BUTTON_ID_SINGLE,
			BTN_S_XS,
			BTN_S_YS,
			BTN_S_XE,
			BTN_S_YE );
	UG_ButtonSetForeColor( &window_1, 	BUTTON_ID_SINGLE, BTN_FORECOLOR );
	UG_ButtonSetBackColor( &window_1, 	BUTTON_ID_SINGLE, BTN_BACKCOLOR );
	UG_ButtonSetAlternateForeColor( &window_1,BUTTON_ID_SINGLE, BTN_ALT_FORECOLOR );
	UG_ButtonSetAlternateBackColor( &window_1,BUTTON_ID_SINGLE, BTN_ALT_BACKCOLOR );
	UG_ButtonSetStyle( &window_1, 		BUTTON_ID_SINGLE, BUTTON_STYLE);
	UG_ButtonSetFrameRadius( &window_1, BUTTON_ID_SINGLE, BUTTON_FRAME_RADIUS );
	UG_ButtonSetAlignment( &window_1, 	BUTTON_ID_SINGLE, BTN2_FONT_ALIGMENT);
	UG_ButtonSetFont( &window_1, 		BUTTON_ID_SINGLE, &BTN2_FONT );

	//BUTTON - BUTTON_ID_DUAL_1
	UG_ButtonCreate( &window_1, &button_dual_1, BUTTON_ID_DUAL_1,
			BTN_D1_XS,
			BTN_D1_YS,
			BTN_D1_XE,
			BTN_D1_YE );
	UG_ButtonSetForeColor( &window_1, 	BUTTON_ID_DUAL_1, BTN_FORECOLOR );
	UG_ButtonSetBackColor( &window_1, 	BUTTON_ID_DUAL_1, BTN_BACKCOLOR );
	UG_ButtonSetAlternateForeColor( &window_1,
			BUTTON_ID_DUAL_1, BTN_ALT_FORECOLOR );
	UG_ButtonSetAlternateBackColor( &window_1,
			BUTTON_ID_DUAL_1, BTN_ALT_BACKCOLOR );
	UG_ButtonSetStyle( &window_1, 		BUTTON_ID_DUAL_1, BUTTON_STYLE);
	UG_ButtonSetFrameRadius( &window_1, BUTTON_ID_DUAL_1, BUTTON_FRAME_RADIUS );
	UG_ButtonSetAlignment( &window_1, 	BUTTON_ID_DUAL_1, BTN2_FONT_ALIGMENT);
	UG_ButtonSetFont( &window_1, 		BUTTON_ID_DUAL_1, &BTN2_FONT );

	//BUTTON - BUTTON_ID_DUAL_2
	UG_ButtonCreate( &window_1, &button_dual_2, BUTTON_ID_DUAL_2,
			BTN_D2_XS,
			BTN_D2_YS,
			BTN_D2_XE,
			BTN_D2_YE );
	UG_ButtonSetForeColor( &window_1, 	BUTTON_ID_DUAL_2, BTN_FORECOLOR );
	UG_ButtonSetBackColor( &window_1, 	BUTTON_ID_DUAL_2, BTN_BACKCOLOR );
	UG_ButtonSetAlternateForeColor( &window_1,
			BUTTON_ID_DUAL_2, BTN_ALT_FORECOLOR );
	UG_ButtonSetAlternateBackColor( &window_1,
			BUTTON_ID_DUAL_2, BTN_ALT_BACKCOLOR );
	UG_ButtonSetStyle( &window_1, 		BUTTON_ID_DUAL_2, BUTTON_STYLE);
	UG_ButtonSetFrameRadius( &window_1, BUTTON_ID_DUAL_2, BUTTON_FRAME_RADIUS );
	UG_ButtonSetAlignment( &window_1, 	BUTTON_ID_DUAL_2, BTN2_FONT_ALIGMENT);
	UG_ButtonSetFont( &window_1, 		BUTTON_ID_DUAL_2, &BTN2_FONT );

	return UG_RESULT_OK;
}

UG_RESULT UG_MsgShow(const char* msg,
		const char* 		msg2,
		ugui_msg_icon_t		icon,
		ugui_msg_style_t 	style,
		const char* 		btn1_str,
		const char* 		btn2_str,
		UG_Msg_Callback 	cb,
		UG_S32 				timeout_ms,
		ugui_system_event_t evt_mask )
{
	msg_cb = cb;
	msg_icon = icon;
	msg_timeout = timeout_ms;
	msg_timeout_cnt = -1;
	msg_evt_mask = evt_mask | UGUI_SYSTEM_EVENT_TICK;
	if(msg_timeout>0)
	{
		msg_timeout_cnt = msg_timeout / UGUI_MS_PER_TICK;
	}

	UG_ButtonSetText( &window_1,  BUTTON_ID_MSG, msg );
	UG_ButtonSetText2( &window_1,  BUTTON_ID_MSG, msg2 );
	if(msg2 == NULL)
	{
		UG_ButtonSetAlignment( &window_1, 	BUTTON_ID_MSG, BTN2_FONT_ALIGMENT);
	}
	else
	{
		UG_ButtonSetAlignment( &window_1, 	BUTTON_ID_MSG, BTN_FONT_ALIGMENT);
	}

	switch(style)
	{
	case UGUI_MSG_STYLE_NO_BUTTONS:
		UG_ButtonHide( &window_1,  BUTTON_ID_SINGLE );
		UG_ButtonHide( &window_1,  BUTTON_ID_DUAL_1 );
		UG_ButtonHide( &window_1,  BUTTON_ID_DUAL_2 );
		break;
	case UGUI_MSG_STYLE_1_BUTTON:
		UG_ButtonSetText( &window_1,  BUTTON_ID_SINGLE, btn1_str );
		UG_ButtonShow( &window_1,  BUTTON_ID_SINGLE );
		UG_ButtonHide( &window_1,  BUTTON_ID_DUAL_1 );
		UG_ButtonHide( &window_1,  BUTTON_ID_DUAL_2 );
		break;
	case UGUI_MSG_STYLE_2_BUTTONS:
		UG_ButtonSetText( &window_1,  BUTTON_ID_DUAL_1, btn1_str );
		UG_ButtonSetText( &window_1,  BUTTON_ID_DUAL_2, btn2_str );
		UG_ButtonHide( &window_1,  BUTTON_ID_SINGLE );
		UG_ButtonShow( &window_1,  BUTTON_ID_DUAL_1 );
		UG_ButtonShow( &window_1,  BUTTON_ID_DUAL_2 );
		break;

	}

	//icon
	switch(icon)
	{
	case UGUI_MSG_ICON_WORKING:
		UG_ButtonSetImage( &window_1, BUTTON_ID_MSG, BTN_IMG_BOTTOM, msg_icon_working[0], BTN_W/2, BTN_H/2 );
		break;
	case UGUI_MSG_ICON_NONE:
		UG_ButtonSetImage( &window_1, BUTTON_ID_MSG, BTN_IMG_BOTTOM,NULL, 0, 0 );
	default:
		break;

	}

	return UG_WindowShow( &window_1, update_callback );
}

UG_RESULT UG_MsgHide()
{
	return UG_WindowHide( &window_1 );
}

static void update_callback( ugui_system_event_t evt )
{
	static int update_cnt = 0;

	switch( evt & msg_evt_mask )
	{
	case UGUI_SYSTEM_EVENT_TICK:
		if(update_cnt-- <= 0)
		{
			update_cnt = 10;
			ugui_icon_wifi_update(&window_1,IMG_ID_WIFI);
			ugui_icon_battery_update(&window_1,IMG_ID_BATTERY);
		}

		if( msg_timeout_cnt > 0 )
		{
			msg_timeout_cnt--;
		}
		else if( msg_timeout_cnt == 0 )
		{
			msg_timeout_cnt = -1;
			if( msg_cb )
			{
				msg_cb(-1);
			}
		}

		//animation
		switch(msg_icon)
		{
		case UGUI_MSG_ICON_WORKING:
			animation_cnt++;
			if(animation_cnt >= MSG_ICON_WORKING_CNT*2)
			{
				animation_cnt = 0;
			}

			UG_ButtonSetImage( &window_1, BUTTON_ID_MSG, BTN_IMG_BOTTOM, msg_icon_working[animation_cnt/2], BTN_W/2, BTN_H/2 );
			break;
		case UGUI_MSG_ICON_NONE:
		default:
			break;

		}

		break;
	case UGUI_SYSTEM_EVENT_WIFI_CONNECT_SUCCESS:
	{
		UG_MsgShow("WiFi Connected",
 			0,
			0,
			UGUI_MSG_STYLE_1_BUTTON,
			"ok",
			0,
			UG_HomeShow,
			3000,
			0);
		
	}
		break;
	case UGUI_SYSTEM_EVENT_WIFI_CONNECT_FAIL:
    	UG_MsgShow("Failed",
    			0,
				0,
				UGUI_MSG_STYLE_1_BUTTON,
				"ok",
				0,
				UG_SettingsNetworkWifiSsidShow,
				5000,
				0);
		break;
	default:
		break;
	}
}

static void touch_callback( UG_MESSAGE* msg )
{
	if ( msg->type == MSG_TYPE_OBJECT )
	{
		if ( msg->id == OBJ_TYPE_BUTTON && msg->event == OBJ_EVENT_RELEASED )
		{
			switch(msg->sub_id)
			{
			case BUTTON_ID_MSG:

				break;
			case BUTTON_ID_SINGLE:
			case BUTTON_ID_DUAL_1:
				if( msg_cb )
				{
					msg_cb(0);
				}
				break;
			case BUTTON_ID_DUAL_2:
				if( msg_cb )
				{
					msg_cb(1);
				}
				break;
			}

		}
	}

}

