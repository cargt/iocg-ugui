#ifndef __images_H
#define __images_H

#include "ugui.h"

extern const UG_BMP img_arrow_left;
extern const UG_BMP img_batt_100;
extern const UG_BMP img_scroll_dot_empty;
extern const UG_BMP img_settings;
extern const UG_BMP img_wifi_none;
extern const UG_BMP img_wait_state_0;
extern const UG_BMP img_arrow_right;
extern const UG_BMP img_back;
extern const UG_BMP img_ring;
extern const UG_BMP img_wifi_100;
extern const UG_BMP img_batt_50;
extern const UG_BMP img_wait_state_1;
extern const UG_BMP img_ring_red;
extern const UG_BMP img_icon_settings;
extern const UG_BMP img_batt_75;
extern const UG_BMP img_scroll_dot_full;
extern const UG_BMP img_wifi_0;
extern const UG_BMP img_batt_25;
extern const UG_BMP img_wifi_66;
extern const UG_BMP img_logo;
extern const UG_BMP img_wait_state_4;
extern const UG_BMP img_arrow_forward;
extern const UG_BMP img_wifi_mode_offline;
extern const UG_BMP img_wait_state_3;
extern const UG_BMP img_batt_0;
extern const UG_BMP img_wait_state_2;
extern const UG_BMP img_ring_blue;
extern const UG_BMP img_wifi_mode_online;
extern const UG_BMP img_ring_green;
extern const UG_BMP img_wifi_33;
extern const UG_BMP img_batt_charging;
extern const UG_BMP img_wait_state_5;
extern const UG_BMP img_scroll_down;
extern const UG_BMP img_backspace;
extern const UG_BMP img_scroll_up;

#endif //__images_H"
