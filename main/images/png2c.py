import glob
import os
import re
import subprocess

def convertPngToC(input_file,output_file):
    filename_full = input_file
    filename_list = filename_full.split(b'.')
    filename = filename_list[0]
    filename_c = filename.replace(b'-',b'_') 
    batcmd=b"identify -format '%[fx:w]x%[fx:h]' " + filename_full
    result = subprocess.check_output(batcmd.decode("utf-8") , shell=True)
    img_size = re.findall(b'[1-9][0-9]*',result)

    print(result)

    batcmd=b"convert "+filename_full+ b" " +filename+b".rgba" 
    #batcmd=b"convert "+filename_full+ b" -separate -swap 1,3 -combine " +filename+b".rgba"
    print(batcmd.decode("utf-8"))
    result = subprocess.check_output(batcmd.decode("utf-8"), shell=True)

    const_string = "const "
    f = open(output_file + b".c", 'a')
    f.write(str(const_string).encode('utf-8'))
    f.close()
    batcmd=b"xxd -i " + filename + b".rgba >> " + output_file + b".c"
    result = subprocess.check_output(batcmd.decode("utf-8"), shell=True)
  
    struct_def =  b"const UG_BMP img_"+ filename_c +b" =\n"
    struct_def += b"{\n"
    struct_def += b"\t(void*)" + filename_c + b"_rgba,\n"
    struct_def += b"\t"+img_size[0]+b",\n"
    struct_def += b"\t"+img_size[1]+b",\n"
    struct_def += b"\tBMP_BPP_32,\n"
    struct_def += b"\tBMP_RGBA8888,\n"
    struct_def += b"\t0x0000\n"
    struct_def += b"};\n"

    f = open(output_file + b".c", 'a')
    f.write(str(struct_def).encode('utf-8'))
    f.close()

    f = open(output_file + b".h", 'a')
    f.write("extern const UG_BMP img_"+ str(filename_c).encode('utf-8') + ";\n")
    f.close()


file_list = "images.txt"
outfile = b"images"
try:
    os.remove(outfile + b".c")
except OSError:
    pass

try:
    os.remove(outfile + b".h")
except OSError:
    pass

f = open(outfile + b".c", 'a')
f.write("#include \""+ str(outfile).encode('utf-8') + ".h\"\n\n")
f.close()

#ifndef __LTDC_H
#define __LTDC_H

f = open(outfile + b".h", 'a')
f.write("#ifndef __"+ str(outfile).encode('utf-8') + "_H\n")
f.write("#define __"+ str(outfile).encode('utf-8') + "_H\n\n")
f.write("#include \"ugui.h\"\n\n")
f.close()

os.chdir(".")
for file in glob.glob("*.png"):
    print(file)            
    convertPngToC(file.encode(),outfile)

f = open(outfile + b".h", 'a')
f.write("\n#endif //__"+ str(outfile).encode('utf-8') + "_H\"\n")
f.close()

