#include "ugui.h"
#include "ugui_config.h"

#ifndef __UGUI_SETTINGS_NETWORK_WIFI_SSID_H
#define __UGUI_SETTINGS_NETWORK_WIFI_SSID_H

UG_RESULT UG_SettingsNetworkWifiSsidCreate();
UG_RESULT UG_SettingsNetworkWifiSsidShow();
UG_RESULT UG_SettingsNetworkWifiSdidHide();

#endif //#ifndef __UGUI_SETTINGS_NETWORK_WIFI_SSID_H
