#ifndef CGIWIFI_H
#define CGIWIFI_H

#include "libesphttpd/httpd.h"
#include "esp_event_loop.h"

CgiStatus cgiWiFiScanEsp32(HttpdConnData *connData);
int tplWlanEsp32(HttpdConnData *connData, char *token, void **arg);
CgiStatus cgiWiFiEsp32(HttpdConnData *connData);
CgiStatus cgiWiFiConnectEsp32(HttpdConnData *connData);
CgiStatus cgiWiFiSetModeEsp32(HttpdConnData *connData);
CgiStatus cgiWiFiSetChannelEsp32(HttpdConnData *connData);
CgiStatus cgiWiFiConnStatusEsp32(HttpdConnData *connData);

void cgiWiFiEventHandlerEsp32(void *ctx, system_event_t *event);

#endif
