/*
 * ledc_backlight.c
 *
 *  Created on: Jan 3, 2018
 *      Author: Graham
 */
#include "sdkconfig.h"

#include "driver/ledc.h"
#include "esp_log.h"

#include "led_rgb.h"

#define TAG "LED_RGB"

#define LED_RGB_R	0
#define LED_RGB_B	1
#define LED_RGB_G	2

#define LEDC_PWM_MAX                    ((1<<13) - 1)
#define LEDC_PWM_RESOLUTION             LEDC_TIMER_13_BIT
#define LEDC_PWM_COMMON_FREQUENCY       5000
#define LEDC_TIMER_MODE                 LEDC_LOW_SPEED_MODE
#define LEDC_TIMER_NUM_RGB        		LEDC_TIMER_1
#define LEDC_CHANNEL_NUM_R      		LEDC_CHANNEL_1
#define LEDC_CHANNEL_NUM_G      		LEDC_CHANNEL_2
#define LEDC_CHANNEL_NUM_B      		LEDC_CHANNEL_3
static ledc_channel_config_t ledc_channel[3];

//***********************************************
//* Private Prototypes
//***********************************************
static void ledRgb_update(ledc_channel_config_t* ledc_channel, uint32_t reqPWMValue);


//***********************************************
//* Public Functions
//***********************************************
void ledRgb_init(uint8_t rGpioNum, uint8_t gGpioNum, uint8_t bGpioNum)
{
    ledc_timer_config_t ledc_timer = {
        .bit_num = LEDC_PWM_RESOLUTION,         // resolution of PWM duty
        .freq_hz = LEDC_PWM_COMMON_FREQUENCY,   // frequency of PWM signal
        .speed_mode = LEDC_TIMER_MODE,          // timer mode
        .timer_num = LEDC_TIMER_NUM_RGB   // timer index
    };
    ledc_timer_config(&ledc_timer);


	ledc_channel[LED_RGB_R].channel    = LEDC_CHANNEL_NUM_R;
	ledc_channel[LED_RGB_R].duty       = LEDC_PWM_MAX;
	ledc_channel[LED_RGB_R].gpio_num   = rGpioNum;
	ledc_channel[LED_RGB_R].speed_mode = LEDC_TIMER_MODE;
	ledc_channel[LED_RGB_R].timer_sel  = LEDC_TIMER_NUM_RGB;
	if(rGpioNum != 255) {
		ledc_channel_config(&ledc_channel[LED_RGB_R]);
    }


	ledc_channel[LED_RGB_G].channel    = LEDC_CHANNEL_NUM_G;
	ledc_channel[LED_RGB_G].duty       = LEDC_PWM_MAX;
	ledc_channel[LED_RGB_G].gpio_num   = gGpioNum;
	ledc_channel[LED_RGB_G].speed_mode = LEDC_TIMER_MODE;
	ledc_channel[LED_RGB_G].timer_sel  = LEDC_TIMER_NUM_RGB;
	if(gGpioNum != 255) {
		ledc_channel_config(&ledc_channel[LED_RGB_G]);
    }


	ledc_channel[LED_RGB_B].channel    = LEDC_CHANNEL_NUM_B;
	ledc_channel[LED_RGB_B].duty       = LEDC_PWM_MAX;
	ledc_channel[LED_RGB_B].gpio_num   = bGpioNum;
	ledc_channel[LED_RGB_B].speed_mode = LEDC_TIMER_MODE;
	ledc_channel[LED_RGB_B].timer_sel  = LEDC_TIMER_NUM_RGB;
	if(bGpioNum != 255) {
		ledc_channel_config(&ledc_channel[LED_RGB_B]);
    }


    ESP_LOGI(TAG, "PWM LED RGB setup");
} // ledc_init

void ledRgb_set_color(uint8_t r, uint8_t g, uint8_t b)
{
    uint32_t pwmVal;

    if(ledc_channel[LED_RGB_R].gpio_num != 255) {
		pwmVal = ((((uint32_t) r) * LEDC_PWM_MAX))/ 256;
		ledRgb_update(&ledc_channel[LED_RGB_R], pwmVal);
    }

    if(ledc_channel[LED_RGB_G].gpio_num != 255) {
    	pwmVal = ((((uint32_t) g) * LEDC_PWM_MAX))/ 256;
    	ledRgb_update(&ledc_channel[LED_RGB_G], pwmVal);
    }

    if(ledc_channel[LED_RGB_B].gpio_num != 255) {
    	pwmVal = ((((uint32_t) b) * LEDC_PWM_MAX))/ 256;
    	ledRgb_update(&ledc_channel[LED_RGB_B], pwmVal);
    }
}


//***********************************************
//* Private Functions
//***********************************************
static void ledRgb_update(ledc_channel_config_t* ledc_channel, uint32_t reqPWMValue)
{
    ledc_set_duty(ledc_channel->speed_mode, ledc_channel->channel, reqPWMValue);
    ledc_update_duty(ledc_channel->speed_mode, ledc_channel->channel);
} // ledc_backlight_update
