/*
 * can.h
 *
 *  Created by Graham Taylor-Jones on Sept 11, 2018
 *  Copyright (c) 2018 Cargt, LLC. All rights reserved.
 *
 * Versions:
 *      1 - Initial Protocol
 *      2 - Add bootloader command IDs and shift originals to be different
 */

#ifndef CAN_IOA200_H_
#define CAN_IOA200_H_

#include <stdbool.h>
#include <stdint.h>

/**********************************************************
 * USED IN JOINT PROJECTS so Version Control is important
 *********************************************************/
#define CAN_IOA200_PROTOCOL_VERSION 3

#ifndef PRJ_CAN_PROTOCOL_VERSION_REQUIRED
    #error "PRJ_CAN_PROTOCOL_VERSION_REQUIRED not defined for project"
#else
    #if ( PRJ_CAN_PROTOCOL_VERSION_REQUIRED != CAN_IOA200_PROTOCOL_VERSION )
        #error "CAN Protocol Versions do NOT match: PRJ_CAN_PROTOCOL_VERSION_REQUIRED != CAN_IOA200_PROTOCOL_VERSION"
    #endif
#endif

/**********************************************************
 * COPIED FROM DUKE HEADER
 *********************************************************/
// Message Type. Typically only 'STANDARD' 'DATA' is used.
// NOTE: IOA200 Project uses 'EXT' 'DATA' with possible 'RTR' if needed
#define CAN_MSG_DATA			0x01 // message type
#define CAN_MSG_RTR				0x02 // data or RTR
#define CAN_FRAME_EXT			0x03 // Frame type
#define CAN_FRAME_STD			0x04 // extended or standard

struct STRUCT_CAN_MESSAGE_BY_ID
{
	unsigned char message_type;			// RTR message or data message
	unsigned char frame_type;			// extended(J1939) or standard(11bit)
	unsigned char buffer;				// 29 bit id max of 0x1FFF FFFF and 11 bit id max of 0x7FF
	unsigned long id;
	unsigned char data[8];
	unsigned char data_length;
};

/*********************************************************/


// CAN CMD PROTOCOL
#define CAN_DEFAULT_EMPTY_PKT_VALUE                     ( ( uint64_t )0xAAAAAAAAAAAAAAAA )

// CAN BIT ID
// 29 bit [28:21(8 bits cmd id)] [20:0(21 bits serial number "3bytes with mask")]
#define CMD_ID_SHIFT_BITS                               20U
#define CMD_ID_MASK                                     0x1FF00000 // 0x1F E0 00 00 = [28:20 9 bits]

#define CAN_SERIAL_NUMBER_MASK                          0x000FFFFF // 0x00F FF FF = 20 bits


/**********************************************************
 * BOOTLOADER COMMAND IDs
 * - MAKE SURE THEY DON'T CLASH WITH ANY OTHERS
 *********************************************************/
/* Sent from server to adjust upper page address of flash memory. */
#define CANMSGID_BOOT_UPPER_PAGE_ADDRESS    ( ( 0x150 << CMD_ID_SHIFT_BITS ) & CMD_ID_MASK )     /* cftp_lib.h original (long) 0x150 */

/* Sent from server to designate TFTP success and begin bootload. */
#define CANMSGID_BOOT_BEGIN_BOOTLOAD        ( ( 0x151 << CMD_ID_SHIFT_BITS ) & CMD_ID_MASK )     /* cftp_lib.h original (long) 0x151 */

/* Used to terminate either successful or failed bootload. */
#define CANMSGID_BOOT_END_BOOTLOAD          ( ( 0x152 << CMD_ID_SHIFT_BITS ) & CMD_ID_MASK )     /* cftp_lib.h original (long) 0x152 */

/* Sent From client's bootloader to server to begin CAN bootload. */
#define CANMSGID_BOOT_REQUEST_TFTP          ( ( 0x153 << CMD_ID_SHIFT_BITS ) & CMD_ID_MASK )     /* cftp_lib.h original (long) 0x153 */

/* Initiate Block transfer. */
#define CANMSGID_BOOT_BLOCK_START           ( ( 0x154 << CMD_ID_SHIFT_BITS ) & CMD_ID_MASK )     /* cftp_lib.h original (long) 0x154 */

/* Data section of Block transfer. */
#define CANMSGID_BOOT_BLOCK_DATA            ( ( 0x155 << CMD_ID_SHIFT_BITS ) & CMD_ID_MASK )     /* cftp_lib.h original (long) 0x155 */

/* Terminate Block transfer - No Longer Used. */
#define CANMSGID_BOOT_BLOCK_END             ( ( 0x156 << CMD_ID_SHIFT_BITS ) & CMD_ID_MASK )     /* cftp_lib.h original (long) 0x156 */

/* Client needs to ACK CAN Block and Upper Address Messages, ACK w/ ID. */
#define CANMSGID_BOOT_ACK                   ( ( 0x157 << CMD_ID_SHIFT_BITS ) & CMD_ID_MASK )     /* cftp_lib.h original (long) 0x157 */
/*********************************************************/

/**********************************************************
 * MESSAGE ID DEFINITIONS
 *********************************************************/
// *** DEBUG COMMANDS ***
#define CANMSGID_IOA200_STATUS_HEARTBEAT                    ( ( 0x020 << CMD_ID_SHIFT_BITS ) & CMD_ID_MASK )
#define CANMSGID_IOA200_STATUS_ZONE_1_RAW_ADC               ( ( 0x021 << CMD_ID_SHIFT_BITS ) & CMD_ID_MASK )
#define CANMSGID_IOA200_STATUS_ZONE_2_RAW_ADC               ( ( 0x022 << CMD_ID_SHIFT_BITS ) & CMD_ID_MASK )
#define CANMSGID_IOA200_STATUS_INVALID_REQUEST              ( ( 0x023 << CMD_ID_SHIFT_BITS ) & CMD_ID_MASK )

// *** READ AND WRITE VALUES COMMANDS ***
#define CANMSGID_IOA200_WRITE_ZONE_1_SETPOINT               ( ( 0x030 << CMD_ID_SHIFT_BITS ) & CMD_ID_MASK )
#define CANMSGID_IOA200_READ_ZONE_1_SETPOINT                ( ( 0x031 << CMD_ID_SHIFT_BITS ) & CMD_ID_MASK )

#define CANMSGID_IOA200_WRITE_ZONE_2_SETPOINT               ( ( 0x032 << CMD_ID_SHIFT_BITS ) & CMD_ID_MASK )
#define CANMSGID_IOA200_READ_ZONE_2_SETPOINT                ( ( 0x033 << CMD_ID_SHIFT_BITS ) & CMD_ID_MASK )

#define CANMSGID_IOA200_WRITE_CFG_SOFT_ON_OFF               ( ( 0x034 << CMD_ID_SHIFT_BITS ) & CMD_ID_MASK )
#define CANMSGID_IOA200_READ_CFG_SOFT_ON_OFF                ( ( 0x035 << CMD_ID_SHIFT_BITS ) & CMD_ID_MASK )

#define CANMSGID_IOA200_WRITE_CFG_FORMAT_DEG_FRNHT_CLCS     ( ( 0x036 << CMD_ID_SHIFT_BITS ) & CMD_ID_MASK )     // Faranheit or celcius
#define CANMSGID_IOA200_READ_CFG_FORMAT_DEG_FRNHT_CLCS      ( ( 0x037 << CMD_ID_SHIFT_BITS ) & CMD_ID_MASK )

#define CANMSGID_IOA200_WRITE_CFG_LABEL                     ( ( 0x038 << CMD_ID_SHIFT_BITS ) & CMD_ID_MASK )
#define CANMSGID_IOA200_READ_CFG_LABEL                      ( ( 0x039 << CMD_ID_SHIFT_BITS ) & CMD_ID_MASK )

#define CANMSGID_IOA200_WRITE_SERIAL_NUMBER                 ( ( 0x03A << CMD_ID_SHIFT_BITS ) & CMD_ID_MASK )     // serial number is read as part of messages

// *** STATUS COMMANDS ACCORDING TO RUN MODE ***
#define CANMSGID_IOA200_GET_STATUS_FROM_DEVICE              ( ( 0x040 << CMD_ID_SHIFT_BITS ) & CMD_ID_MASK )     // Reply is runtime dependent
#define CANMSGID_IOA200_STATUS_1_HEAT_UI                    ( ( 0x041 << CMD_ID_SHIFT_BITS ) & CMD_ID_MASK )
#define CANMSGID_IOA200_STATUS_1_COLD_UI                    ( ( 0x042 << CMD_ID_SHIFT_BITS ) & CMD_ID_MASK )
#define CANMSGID_IOA200_STATUS_2_HEAT                       ( ( 0x043 << CMD_ID_SHIFT_BITS ) & CMD_ID_MASK )
#define CANMSGID_IOA200_STATUS_HOT_FROST                    ( ( 0x044 << CMD_ID_SHIFT_BITS ) & CMD_ID_MASK )
#define CANMSGID_IOA200_STATUS_INVALID_CFG                  ( ( 0x045 << CMD_ID_SHIFT_BITS ) & CMD_ID_MASK )
#define CANMSGID_IOA200_STATUS_TEST_MODE                    ( ( 0x046 << CMD_ID_SHIFT_BITS ) & CMD_ID_MASK )


/**********************************************************
 * MESSAGE STRUCTURE DEFINITIONS
 *********************************************************/
#pragma pack(1)
union UNION_CANLIB_MSGDEF_HEARTBEAT_STATUS
{
    uint64_t                    all;
    uint16_t                    w[4];
    uint8_t                     b[8];
    struct
    {
        // SW Version
        uint8_t                 major;
        uint8_t                 minor;
        uint8_t                 build;
        uint8_t                 brd_cfg     : 4;
        unsigned                            : 4; // spare
        unsigned                            : 32;
        // TODO: Uptime counter
    } Heartbeat_status_type;
};
#pragma pack()

#pragma pack(1)
union UNION_CANMSGID_IOA200_STATUS_1_HEAT_UI
{
    uint64_t                    all;
    uint16_t                    w[4];
    uint8_t                     b[8];
    struct
	{
        // Readings specific to run mode
        // readings can be cut down to 12 bits if more space needed
        unsigned                temp_reading    : 16;
        unsigned                temp_setting    : 16;
        unsigned                pwr_sens_1      : 1;
        unsigned                heater_output   : 1;
        unsigned                relay_output    : 1;
        unsigned                                : 5;
        unsigned                                : 24; // spare
	} one_heat_status_type;
};
#pragma pack()

#pragma pack(1)
union UNION_CANMSGID_IOA200_STATUS_1_COLD_UI
{
    uint64_t                    all;
    uint16_t                    w[4];
    uint8_t                     b[8];
    struct
	{
        // Readings specific to run mode
        // readings can be cut down to 12 bits if more space needed
        unsigned                temp_reading    : 16;
        unsigned                temp_setting    : 16;
        unsigned                                : 32; // spare
	} one_cold_status_type;
};
#pragma pack()

#pragma pack(1)
union UNION_CANMSGID_IOA200_STATUS_2_HEAT
{
    uint64_t                    all;
    uint16_t                    w[4];
    uint8_t                     b[8];
    struct
    {
        // Readings specific to run mode
        unsigned                pct_setting_1   : 8;
        unsigned                pct_setting_2   : 8;
        unsigned                                : 16; // spare
        unsigned                                : 32; // spare
    } two_heat_status_type;
};
#pragma pack()

#pragma pack(1)
union UNION_CANMSGID_IOA200_STATUS_HOT_FROST
{
    uint64_t                    all;
    uint16_t                    w[4];
    uint8_t                     b[8];
    struct
	{
        // Readings specific to run mode
        // readings can be cut down to 12 bits if more space needed
        // mode, pct and reading could be combined into a union if even more space needed. Pct OR Reading depending on mode
        unsigned                pct_setting     : 8;
        unsigned                temp_reading    : 16;
        unsigned                heat_mode       : 1;
        unsigned                                : 7; // spare
        unsigned                                : 32; // spare
	} hot_frost_status_type;
};
#pragma pack()

#pragma pack(1)
union UNION_CANMSGID_IOA200_STATUS_INVALID_CFG
{
    uint64_t                    all;
    uint16_t                    w[4];
    uint8_t                     b[8];
    struct
	{
        // Readings specific to run mode
        unsigned                raw_jumpers : 4;
        unsigned                            : 4; // spare
        unsigned                            : 8; // spare
        unsigned                            : 16; // spare
        unsigned                            : 32; // spare
	} invalid_cfg_status_type;
};
#pragma pack()

#pragma pack(1)
union UNION_CANMSGID_IOA200_STATUS_TEST_MODE
{
    uint64_t                    all;
    uint16_t                    w[4];
    uint8_t                     b[6];
    struct
	{
        // message format
        unsigned                inputs          : 8;
        unsigned                outputs         : 8;
        unsigned                rtd_val_raw_1   : 12;
        unsigned                rtd_val_raw_2   : 12;
        unsigned                adj_sns_raw_1   : 12;
        unsigned                adj_sns_raw_2   : 12;
	} test_mode_status_type;
};
#pragma pack()

#pragma pack(1)
union UNION_CANLIB_MSGDEF_IOA200_FORMATTED_STATUS
{
    union UNION_CANMSGID_IOA200_STATUS_1_HEAT_UI   status_1_heat;
    union UNION_CANMSGID_IOA200_STATUS_1_COLD_UI   status_1_cold;
	union UNION_CANMSGID_IOA200_STATUS_2_HEAT      status_2_heat;
	union UNION_CANMSGID_IOA200_STATUS_HOT_FROST   status_hot_frost;
	union UNION_CANMSGID_IOA200_STATUS_INVALID_CFG status_invalid_cfg;
	union UNION_CANMSGID_IOA200_STATUS_TEST_MODE   status_test_mode;
};
#pragma pack()

#pragma pack(1)
struct STRUCT_CANLIB_MSGDEF_CAN_IOC
{
    union UNION_CANLIB_MSGDEF_IOA200_FORMATTED_STATUS        stat1;
};
#pragma pack()

#pragma pack(1)
typedef struct
	{
	int16_t					rtd_val;
	int16_t					adj_sns;
	union {
		uint8_t				io_byte;
		struct {
			bool pwr_sens       : 1;
			bool bit1_spare     : 1;
			bool bit2_spare     : 1;
			bool bit3_spare     : 1;
			bool triac_ctrl     : 1;
			bool ssr_sink       : 1;
			bool led_out        : 1;
			bool bit7_spare     : 1;
		} io_bits; // struct
	}; // union
	} Zone_status_type;
#pragma pack()


#endif /* CAN_IOA200_H_ */
