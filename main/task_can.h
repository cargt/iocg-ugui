/*
 * task_can.h
 *
 *  Created by Graham Taylor-Jones on Sept 27, 2018
 *  Copyright (c) 2018 Cargt, LLC. All rights reserved.
 *
 */

#ifndef MAIN_TASK_CAN_H_
#define MAIN_TASK_CAN_H_

#include "sdkconfig.h"

#include <stdint.h>
#include <stdbool.h>
#include "esp_event.h"

#include "CAN.h"
#include "msg_frame.h"


/**********************************************************
 * Define CAN protocol version
 *********************************************************/
#define PRJ_CAN_PROTOCOL_VERSION_REQUIRED 3
#include "can_ioa200.h"
//*********************************************************

#define DEVICE_STATUS_COMMENTS_TEXT_LEN     100

#define NUM_CAN_FRAME_LOG_ENTRIES           50
#define NUM_DEVICE_STATUS_ENTRIES           16



typedef struct {
    bool        valid;      // position is populated
    bool        connected;  // reset if no message required after timeout
    uint32_t    serial;
    uint32_t    timestamp;
    uint8_t     sw_major;
    uint8_t     sw_minor;
    uint8_t     sw_patch;
    uint8_t     brd_type;
    char        comments[DEVICE_STATUS_COMMENTS_TEXT_LEN];
} device_status_t;


esp_err_t can_event_handler(void *ctx, system_event_t *event);

int task_can_get_device_status( device_status_t *ptr, uint8_t len );
int task_can_get_message_list( msg_frame_t *ptr, uint8_t len );


int  task_can_init(void);

#endif /* MAIN_TASK_CAN_H_ */
