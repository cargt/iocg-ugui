/*
 * task_network.c
 *
 *  Created on: Nov 15, 2017
 *      Author: cargt
 */
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_wifi.h"
#include "esp_log.h"
#include "esp_event_loop.h"
#include "esp_system.h"
#include <string.h>
#include "wifi_scan.h"
#include "apps/sntp/sntp.h"
#include "task_network.h"

#ifdef CONFIG_ENABLE_ETHERNET_INTERFACE

#ifdef CONFIG_PHY_LAN8720
#include "eth_phy/phy_lan8720.h"
#define DEFAULT_ETHERNET_PHY_CONFIG phy_lan8720_default_ethernet_config
#endif

#ifdef CONFIG_PHY_USE_POWER_PIN
#define PIN_PHY_POWER CONFIG_PHY_POWER_PIN
#endif // CONFIG_PHY_USE_POWER_PIN

#define PIN_SMI_MDC   CONFIG_PHY_SMI_MDC_PIN
#define PIN_SMI_MDIO  CONFIG_PHY_SMI_MDIO_PIN
#endif // CONFIG_ENABLE_ETHERNET_INTERFACE


#define COMPILER_ASSERT(X) (void) sizeof(char[(X) ? 1 : -1])

#define TASK_NETWORK_PRIO                   5U
#define TASK_NETWORK_STACK_SIZE             0x4000U

#define MAC_ADDR_SIZE (32+1)

/*set the ssid and password via "make menuconfig"*/
#define DEFAULT_SSID "Cargt"
#define DEFAULT_PWD "31184bd9"

#if CONFIG_WIFI_ALL_CHANNEL_SCAN
#define DEFAULT_SCAN_METHOD WIFI_ALL_CHANNEL_SCAN
#elif CONFIG_WIFI_FAST_SCAN
#define DEFAULT_SCAN_METHOD WIFI_FAST_SCAN
#else
#define DEFAULT_SCAN_METHOD WIFI_FAST_SCAN
#endif /*CONFIG_SCAN_METHOD*/

#if CONFIG_WIFI_CONNECT_AP_BY_SIGNAL
#define DEFAULT_SORT_METHOD WIFI_CONNECT_AP_BY_SIGNAL
#elif CONFIG_WIFI_CONNECT_AP_BY_SECURITY
#define DEFAULT_SORT_METHOD WIFI_CONNECT_AP_BY_SECURITY
#else
#define DEFAULT_SORT_METHOD WIFI_CONNECT_AP_BY_SIGNAL
#endif /*CONFIG_SORT_METHOD*/

#if CONFIG_FAST_SCAN_THRESHOLD
#define DEFAULT_RSSI CONFIG_FAST_SCAN_MINIMUM_SIGNAL
#if CONFIG_EXAMPLE_OPEN
#define DEFAULT_AUTHMODE WIFI_AUTH_OPEN
#elif CONFIG_EXAMPLE_WEP
#define DEFAULT_AUTHMODE WIFI_AUTH_WEP
#elif CONFIG_EXAMPLE_WPA
#define DEFAULT_AUTHMODE WIFI_AUTH_WPA_PSK
#elif CONFIG_EXAMPLE_WPA2
#define DEFAULT_AUTHMODE WIFI_AUTH_WPA2_PSK
#else
#define DEFAULT_AUTHMODE WIFI_AUTH_OPEN
#endif
#else
#define DEFAULT_RSSI -127
#define DEFAULT_AUTHMODE WIFI_AUTH_OPEN
#endif /*CONFIG_FAST_SCAN_THRESHOLD*/


static const char *TAG = "network_task";

struct time_status_t
{
    bool isSntpInitialized;
    bool isSystemTimeSet;
};

// IP ADDRESS
struct ip_addr_t {
    bool isIpAddrValid;
    char str[IP4ADDR_STRLEN_MAX];
    // semaphore
};

static struct ip_addr_t ipAddrStructWifi = {
    .isIpAddrValid = false,
    .str = { 0 },
};

uint8_t wifi_macAddress[6];

/* FreeRTOS event group to signal task */
static EventGroupHandle_t network_event_group;

#ifdef CONFIG_ENABLE_ETHERNET_INTERFACE
static enum ethernet_status_t ethernet_status;

static struct ip_addr_t ipAddrStructEthernet = {
    .isIpAddrValid = false,
    .str = { 0 },
};

uint8_t ethernet_macAddress[6];
#endif

static wifi_config_t wifi_config;
static enum wifi_status_t wifi_status;

static bool autoScanEnabled = false;
struct time_status_t timeStatus =
    {
        .isSntpInitialized = false,
        .isSystemTimeSet = false
    };

static TimerHandle_t xTimerAutoConnectH;
static TimerHandle_t xTimerAutoRescanH;
static TimerHandle_t xTimerSystemTimeH;
void vTimerAutoConnectCallback( TimerHandle_t xTimerH );
void vTimerAutoRescanCallback( TimerHandle_t xTimerH );
void vTimerSystemTimeCallback( TimerHandle_t xTimerH );

static bool scan_running = false;



//=================================
// Private function prototypes
//=================================
#ifdef CONFIG_ENABLE_ETHERNET_INTERFACE
static void initializeEthernet(void);
#endif // CONFIG_ENABLE_ETHERNET_INTERFACE

static int createAutoRescanTimer(void);
static int createSystemTimeCheckTimer(void);
static void initializeSntp(void);
static void network_tsk(void * param);
static esp_err_t setDefaultWifiConfig(wifi_config_t* wifi_config);
static bool checkSystemTime(void);
static wifi_scan_status_t start_scan(void);


//=================================
// Public functions
//=================================
bool task_network_getIpAddressWifi(char *buf, uint8_t bufSize)
{
    if ((NULL == buf) || (0 == bufSize)) return false;
    if (false == ipAddrStructWifi.isIpAddrValid) return false;

    // strlen doesn't include null so need room, hence >=
    size_t len = strlen(ipAddrStructWifi.str);
    if (len >= bufSize) return false;

    strcpy(buf, ipAddrStructWifi.str);
    *(buf + len) = '\0';
    return true;
}

enum wifi_status_t task_network_getWifiStatus(void)
{
    return wifi_status;
}

#ifdef CONFIG_ENABLE_ETHERNET_INTERFACE
/**********************************************************
 * ETHERNET FUNCTIONS
 *********************************************************/
bool task_network_getIpAddressEthernet(char *buf, uint8_t bufSize)
{
    if ((NULL == buf) || (0 == bufSize)) return false;
    if (false == ipAddrStructEthernet.isIpAddrValid) return false;

    // strlen doesn't include null so need room, hence >=
    size_t len = strlen(ipAddrStructEthernet.str);
    if (len >= bufSize) return false;

    strcpy(buf, ipAddrStructEthernet.str);
    *(buf + len) = '\0';
    return true;
}

enum ethernet_status_t task_network_getEthernetStatus(void)
{
    return ethernet_status;
}
#endif // CONFIG_ENABLE_ETHERNET_INTERFACE

void task_network_sendStartScanEvent(void)
{
    // Set event bit
    BaseType_t xResult;
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    xResult = xEventGroupSetBitsFromISR(
                    network_event_group,
                    TN_EVNT_BIT_START_SCAN,
                    &xHigherPriorityTaskWoken);
    if ((pdFAIL != xResult) && (pdTRUE == xHigherPriorityTaskWoken))
    {
        portYIELD_FROM_ISR();
    }
}

int task_network_init(void)
{
    int ret = xTaskCreate(network_tsk,                /* pointer to the task                      */
                    TAG,                        /* task name for kernel awareness debugging */
                    TASK_NETWORK_STACK_SIZE,    /* task stack size                          */
                    NULL,                       /* optional task startup argument           */
                    TASK_NETWORK_PRIO,          /* initial priority                         */
                    NULL                        /* optional task handle to create           */
                    );
    if (pdPASS != ret)
    {
        assert(0);
    }
    ESP_LOGI(TAG, "network_tsk:started,ret=%d", ret);
    return ret;
}

void task_network_signalSystemShuttingDown(void)
{
    // Set status bit
    BaseType_t xResult;
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    xResult = xEventGroupSetBitsFromISR(
                    network_status_group,
                    TN_SYSTEM_SHUTTING_DOWN_STATUS_BIT,
                    &xHigherPriorityTaskWoken);
    if ((pdFAIL != xResult) && (pdTRUE == xHigherPriorityTaskWoken))
    {
        portYIELD_FROM_ISR();
    }
} // task_network_signalSystemShuttingDown

bool task_network_updateSSIDAndPassword(const char *ssid, const char *password)
{
    // check enough room for null char at end.
    // Defensive move as not sure about wifi library checking of array vs string
    uint8_t ssidLength = strlen(ssid);
    uint8_t passwordLength = strlen(password);
    if (ssidLength >= sizeof wifi_config.sta.ssid) return false;
    if (passwordLength >= sizeof wifi_config.sta.password) return false;

    ESP_ERROR_CHECK(esp_wifi_get_config(ESP_IF_WIFI_STA, &wifi_config));

    strcpy((char *)wifi_config.sta.ssid, ssid);
    strcpy((char *)wifi_config.sta.password, password);

    ESP_LOGI(TAG, "task_network_updateSSIDAndPassword:set event,ssid=%s,pwd=%s", wifi_config.sta.ssid, wifi_config.sta.password);
    xEventGroupSetBits(network_event_group, TN_EVNT_BIT_SSID_UPDATE);

    return true;
}

//=================================
// Private functions
//=================================
/******************************************************************************
 * network_event_handler
 * This will be called by the task monitoring the system events.
 * Note: this is called from a different thread
 *****************************************************************************/
esp_err_t network_event_handler(void *ctx, system_event_t *event)
{
    char *ipAddrPtr = NULL;

    switch(event->event_id) {

#ifdef CONFIG_ENABLE_ETHERNET_INTERFACE
    case SYSTEM_EVENT_ETH_START:
        ESP_LOGI(TAG, "SYSTEM_EVENT_ETH_START");
        break;

    case SYSTEM_EVENT_ETH_CONNECTED:
        ethernet_status = ETH_STATUS_CONNECTED;
        ESP_LOGI(TAG, "SYSTEM_EVENT_ETH_CONNECTED");
        break;

    case SYSTEM_EVENT_ETH_GOT_IP:
        ESP_LOGI(TAG, "SYSTEM_EVENT_ETH_GOT_IP");
        ipAddrPtr = ip4addr_ntoa(&event->event_info.got_ip.ip_info.ip);
        ESP_LOGI(TAG, "got eth ip:%s", ipAddrPtr);

        strncpy(ipAddrStructEthernet.str, ipAddrPtr, sizeof ipAddrStructEthernet.str);
        ipAddrStructEthernet.isIpAddrValid = true;
        xEventGroupSetBits(network_status_group, TN_ETH_IP_VALID_STATUS_BIT);
        ipAddrStructEthernet.isIpAddrValid = true;
        break;

    case SYSTEM_EVENT_ETH_DISCONNECTED:
        ESP_LOGI(TAG, "SYSTEM_EVENT_ETH_DISCONNECTED");
        xEventGroupClearBits(network_status_group, (TN_ETH_CONNECTED_STATUS_BIT | TN_ETH_IP_VALID_STATUS_BIT));
        break;

    case SYSTEM_EVENT_ETH_STOP:
        ESP_LOGI(TAG, "SYSTEM_EVENT_ETH_STOP");
        break;

#endif // CONFIG_ENABLE_ETHERNET_INTERFACE

    case SYSTEM_EVENT_STA_START:
        ESP_LOGI(TAG, "SYSTEM_EVENT_STA_START");
        // check shutdown flag before reconnecting
        if (!(TN_SYSTEM_SHUTTING_DOWN_STATUS_BIT & xEventGroupGetBits(network_status_group)))
        {
//            ESP_ERROR_CHECK(esp_wifi_connect());
            esp_err_t e = esp_wifi_connect();
            ESP_LOGE(TAG,"esp_wifi_connect %d",e);
        }
        break;

    case SYSTEM_EVENT_STA_CONNECTED:
        xEventGroupSetBits(network_status_group, TN_WIFI_CONNECTED_STATUS_BIT);
        wifi_status = WIFI_STATUS_CONNECTED;
        initializeSntp();
        break;

    case SYSTEM_EVENT_SCAN_DONE:
        ESP_LOGI(TAG, "SYSTEM_EVENT_SCAN_DONE");
        ESP_LOGI(TAG, "event:status=%u,number=%u,scan_id=%u",
                event->event_info.scan_done.status,
                event->event_info.scan_done.number,
                event->event_info.scan_done.scan_id
                );

        WS_rxScanCompletionEventCallback(&event->event_info.scan_done);
        int8_t numberOfAP = WS_isScanComplete();
        for (int i = 0; i < numberOfAP; ++i)
        {
            wifi_ap_record_t* apData = WS_getNetworkInfo(i);
            if(NULL != apData)
            {
                ESP_LOGI(TAG, "ssid=%s,rssi=%d,ch=%u,sec=%u", apData->ssid, apData->rssi, apData->primary, apData->authmode);
            }
        }
        xEventGroupSetBits(network_event_group, TN_EVNT_BIT_SCAN_COMPLETE);
        break;

    case SYSTEM_EVENT_STA_GOT_IP:
        ESP_LOGI(TAG, "SYSTEM_EVENT_STA_GOT_IP");
        ipAddrPtr = ip4addr_ntoa(&event->event_info.got_ip.ip_info.ip);
        ESP_LOGI(TAG, "got wifi ip:%s", ipAddrPtr);

        strncpy(ipAddrStructWifi.str, ipAddrPtr, sizeof ipAddrStructWifi.str);
        ipAddrStructWifi.isIpAddrValid = true;
        xEventGroupSetBits(network_status_group, TN_WIFI_IP_VALID_STATUS_BIT);

//        udp_logging_init( "192.168.255.255", 1337 );
        break;

    case SYSTEM_EVENT_STA_DISCONNECTED:
        ESP_LOGI(TAG, "SYSTEM_EVENT_STA_DISCONNECTED");
        wifi_status = WIFI_STATUS_DISCONNECTED;
        xEventGroupClearBits(network_status_group, (TN_WIFI_CONNECTED_STATUS_BIT | TN_WIFI_IP_VALID_STATUS_BIT));
        // check shutdown flag before reconnecting
//        if (!(TN_SYSTEM_SHUTTING_DOWN_STATUS_BIT & xEventGroupGetBits(network_status_group)))
//        {
//            esp_wifi_connect();
//        }

//        udp_logging_free();
        break;

    default:
        break;
    }
    return ESP_OK;
}


#ifdef CONFIG_ENABLE_ETHERNET_INTERFACE
__attribute__((unused)) static void printEthernetDebug(void)
{
    tcpip_adapter_ip_info_t ip;
    memset(&ip, 0, sizeof(tcpip_adapter_ip_info_t));
    if (tcpip_adapter_get_ip_info(ESP_IF_ETH, &ip) == 0)
    {
        ESP_LOGI(TAG, "~~~~~~~~~~~");
        ESP_LOGI(TAG, "ETHIP:"IPSTR, IP2STR(&ip.ip));
        ESP_LOGI(TAG, "ETHPMASK:"IPSTR, IP2STR(&ip.netmask));
        ESP_LOGI(TAG, "ETHPGW:"IPSTR, IP2STR(&ip.gw));
        ESP_LOGI(TAG, "~~~~~~~~~~~");
    }
} // printEthernetDebug

#ifdef CONFIG_PHY_USE_POWER_PIN
/* This replaces the default PHY power on/off function with one that
   also uses a GPIO for power on/off.

   If this GPIO is not connected on your device (and PHY is always powered), you can use the default PHY-specific power
   on/off function rather than overriding with this one.
*/
static void phy_device_power_enable_via_gpio(bool enable)
{
    assert(DEFAULT_ETHERNET_PHY_CONFIG.phy_power_enable);

    if (!enable) {
        /* Do the PHY-specific power_enable(false) function before powering down */
        DEFAULT_ETHERNET_PHY_CONFIG.phy_power_enable(false);
    }

    gpio_pad_select_gpio(PIN_PHY_POWER);
    gpio_set_direction(PIN_PHY_POWER,GPIO_MODE_OUTPUT);
    if(enable == true) {
        gpio_set_level(PIN_PHY_POWER, 1);
        ESP_LOGD(TAG, "phy_device_power_enable(TRUE)");
    } else {
        gpio_set_level(PIN_PHY_POWER, 0);
        ESP_LOGD(TAG, "power_enable(FALSE)");
    }

    // Allow the power up/down to take effect, min 300us
    vTaskDelay(1);

    if (enable) {
        /* Run the PHY-specific power on operations now the PHY has power */
        DEFAULT_ETHERNET_PHY_CONFIG.phy_power_enable(true);
    }
} // phy_device_power_enable_via_gpio
#endif

static void eth_gpio_config_rmii(void)
{
    // RMII data pins are fixed:
    // TXD0 = GPIO19
    // TXD1 = GPIO22
    // TX_EN = GPIO21
    // RXD0 = GPIO25
    // RXD1 = GPIO26
    // CLK == GPIO0
    phy_rmii_configure_data_interface_pins();
    // MDC is GPIO 23, MDIO is GPIO 18
    phy_rmii_smi_configure_pins(PIN_SMI_MDC, PIN_SMI_MDIO);
} // eth_gpio_config_rmii

static void initializeEthernet(void)
{
    esp_err_t ret = ESP_OK;
    eth_config_t config = DEFAULT_ETHERNET_PHY_CONFIG;
    /* Set the PHY address in the example configuration */
    config.phy_addr = CONFIG_PHY_ADDRESS;
    config.gpio_config = eth_gpio_config_rmii;
    config.tcpip_input = tcpip_adapter_eth_input;
    config.clock_mode = CONFIG_PHY_CLOCK_MODE;

#ifdef CONFIG_PHY_USE_POWER_PIN
    /* Replace the default 'power enable' function with an example-specific
       one that toggles a power GPIO. */
    config.phy_power_enable = phy_device_power_enable_via_gpio;
#endif

    ret = esp_eth_init(&config);

    if(ret == ESP_OK)
        esp_eth_enable();

} // initializeEthernet
#endif // CONFIG_ENABLE_ETHERNET_INTERFACE


uint8_t task_network_get_rssi()
{
	uint8_t ret = 0;

	wifi_ap_record_t wifidata;
	if (esp_wifi_sta_get_ap_info(&wifidata)==0)
	{
	   ret = wifidata.rssi;
	}

	return ret;
}

//=================================
// Private functions
//=================================

static int createAutoConnectTimer(void)
{
    xTimerAutoConnectH = xTimerCreate( "Auto Connect Timer", pdMS_TO_TICKS(1000), pdTRUE, (void *) 0, vTimerAutoConnectCallback);
    if (NULL == xTimerAutoConnectH)
        return -1;
    else
    {
        BaseType_t ret = xTimerStart(xTimerAutoConnectH, 0);
        if (pdPASS != ret)
        {
            ESP_LOGI(TAG, "Error Starting Timer");
            return -1;
        }
        else
        {
            ESP_LOGI(TAG, "Starting Timer");
            return 0;
        }
    }
}

static int createAutoRescanTimer(void)
{
    xTimerAutoRescanH = xTimerCreate( "Auto Rescan Timer", pdMS_TO_TICKS(5000), pdTRUE, (void *) 0, vTimerAutoRescanCallback);
    if (NULL == xTimerAutoRescanH)
        return -1;
    else
    {
        BaseType_t ret = xTimerStart(xTimerAutoRescanH, 0);
        if (pdPASS != ret)
        {
            ESP_LOGI(TAG, "Error Starting Timer");
            return -1;
        }
        else
        {
            ESP_LOGI(TAG, "Starting Timer");
            return 0;
        }
    }
}

static int createSystemTimeCheckTimer(void)
{
    // NOTE: NOT AUTORELOAD
    xTimerSystemTimeH = xTimerCreate( "System Time check Timer", pdMS_TO_TICKS(2000), pdFALSE, (void *) 0, vTimerSystemTimeCallback);
    if (NULL == xTimerSystemTimeH)
        return -1;
    else
    {
        BaseType_t ret = xTimerStart(xTimerSystemTimeH, 0);
        if (pdPASS != ret)
        {
            ESP_LOGI(TAG, "Error Starting Timer");
            return -1;
        }
        else
        {
            ESP_LOGI(TAG, "Starting Timer");
            return 0;
        }
    }
}

static void initializeSntp(void)
{
    if (true == timeStatus.isSntpInitialized) return;

    ESP_LOGI(TAG, "Initializing SNTP");
    sntp_setoperatingmode(SNTP_OPMODE_POLL);
    sntp_setservername(0, "pool.ntp.org");
    sntp_init();

    timeStatus.isSntpInitialized = true;
    createSystemTimeCheckTimer();
    ESP_LOGI(TAG, "SNTP initialization finished");
}

static void strreplace(char s[], char chr, char repl_chr)
{
     int i=0;
     while(s[i]!='\0')
     {
           if(s[i]==chr)
           {
               s[i]=repl_chr;
           }
           i++;
     }
          printf("%s",s);
}

static void network_tsk(void * param)
{
	wifi_config_t ap_config;
	char mac[MAC_ADDR_SIZE];
	char mac_str[32];

    // Must have initialized nvs - nvs_flash_init()
    // Must have initialized tcp - tcpip_adapter_init()
    // Must have initialized a global event handler that calls the one here.

    network_event_group = xEventGroupCreate();
    network_status_group = xEventGroupCreate();

    memset(&wifi_macAddress[0], 0, sizeof wifi_macAddress);

#ifdef CONFIG_ENABLE_ETHERNET_INTERFACE
    initializeEthernet();
    memset(&ethernet_macAddress[0], 0, sizeof ethernet_macAddress);
#endif // CONFIG_ENABLE_ETHERNET_INTERFACE

    //get ssid & password from storage
    setDefaultWifiConfig(&wifi_config);

    ESP_ERROR_CHECK(esp_read_mac((uint8_t*)mac, ESP_MAC_WIFI_STA));

    sprintf( mac_str, "cargt_%02x:%02x:%02x:%02x:%02x:%02x",
                mac[0], mac[1], mac[2]
                ,mac[3], mac[4], mac[5]
                );

    strreplace(mac_str, ' ', '$');

    strcpy((char*)&ap_config.ap.password, "password");

    //setup sta
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    ESP_ERROR_CHECK( esp_wifi_set_storage(WIFI_STORAGE_FLASH) );
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_APSTA));
//    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config) );

    //strcpy((char*)(&ap_config.ap.ssid), "ESPCargt");
    strcpy((char*)(&ap_config.ap.ssid), mac_str);
    ap_config.ap.ssid_len = strlen((char *)(&ap_config.ap.ssid));
    ap_config.ap.channel = 1;
//    ap_config.ap.authmode = WIFI_AUTH_OPEN;
    ap_config.ap.authmode = WIFI_AUTH_WPA2_PSK;
    ap_config.ap.ssid_hidden = 0;
    ap_config.ap.max_connection = 1;
    ap_config.ap.beacon_interval = 100;

    esp_wifi_set_config(WIFI_IF_AP, &ap_config);

    ESP_ERROR_CHECK(esp_wifi_start());

    createAutoConnectTimer();

    if(autoScanEnabled)
    {
        xEventGroupSetBits(network_event_group, TN_EVNT_BIT_START_SCAN);
        createAutoRescanTimer();
    }

    ESP_LOGI(TAG, "Obtain MAC addresses");
    ESP_ERROR_CHECK(esp_read_mac(&wifi_macAddress[0], ESP_MAC_WIFI_STA));
    ESP_LOGI(TAG, "Wifi MAC address:%02x:%02x:%02x:%02x:%02x:%02x",
                wifi_macAddress[0], wifi_macAddress[1], wifi_macAddress[2]
                ,wifi_macAddress[3], wifi_macAddress[4], wifi_macAddress[5]
                );

#ifdef CONFIG_ENABLE_ETHERNET_INTERFACE
    ESP_ERROR_CHECK(esp_read_mac(&ethernet_macAddress[0], ESP_MAC_ETH));
    ESP_LOGI(TAG, "Eth MAC address:%02x:%02x:%02x:%02x:%02x:%02x",
                 ethernet_macAddress[0], ethernet_macAddress[1], ethernet_macAddress[2]
                ,ethernet_macAddress[3], ethernet_macAddress[4], ethernet_macAddress[5]
                );
#endif // CONFIG_ENABLE_ETHERNET_INTERFACE

    ESP_LOGI(TAG, "start task loop");
    // task run loop
    while (1)
    {
        EventBits_t xBitTest = ( TN_EVNT_BIT_CONNECTED
                                | TN_EVNT_BIT_START_SCAN
                                | TN_EVNT_BIT_SCAN_COMPLETE
                                | TN_EVNT_BIT_CHECK_SYSTEM_TIME
								| TN_EVNT_BIT_CONNECT
                                );
        EventBits_t uxBits;
        uxBits = xEventGroupWaitBits(
                network_event_group,    /* The event group being tested. */
                xBitTest,               /* The bits within the event group to wait for. */
                false,                  /* should bits be cleared before returning? */
                false,                  /* wait for all bits? */
                (1000 / portTICK_PERIOD_MS)           /* max wait time */
//                portMAX_DELAY           /* max wait time */
                );

        if (TN_EVNT_BIT_START_SCAN & uxBits)
        {
            xEventGroupClearBits(network_event_group, TN_EVNT_BIT_START_SCAN);
            scan_running = true;
            start_scan();
        }
        if (TN_EVNT_BIT_SCAN_COMPLETE & uxBits)
        {
            xEventGroupClearBits(network_event_group, TN_EVNT_BIT_SCAN_COMPLETE);
            scan_running = false;
            ESP_LOGI(TAG, "Scan complete:");
        }

        if (TN_EVNT_BIT_SSID_UPDATE & uxBits)
        {
            xEventGroupClearBits(network_event_group, TN_EVNT_BIT_SSID_UPDATE);
            ESP_LOGI(TAG, "ssid_update:disconnect wifi");
            ESP_ERROR_CHECK(esp_wifi_disconnect());
            ESP_LOGI(TAG, "ssid_update:set config wifi");
            ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config));
            ESP_LOGI(TAG, "ssid_update:connect wifi");
            // check shutdown flag before reconnecting
            if (!(TN_SYSTEM_SHUTTING_DOWN_STATUS_BIT & xEventGroupGetBits(network_status_group)))
            {
                ESP_ERROR_CHECK(esp_wifi_connect());
            }
        }

        if (TN_EVNT_BIT_CONNECTED & uxBits)
        {

        }

        if (TN_EVNT_BIT_CHECK_SYSTEM_TIME & uxBits)
        {
            xEventGroupClearBits(network_event_group, TN_EVNT_BIT_CHECK_SYSTEM_TIME);
            if (checkSystemTime())
            {
                // Delete timer - stop checking for initial system time
                xTimerDelete(xTimerSystemTimeH, 0);
            }
            else
            {
                // Restart timer as not auto reload
                xTimerStart(xTimerSystemTimeH, 0);
            }
        }

        if(TN_EVNT_BIT_CONNECT & uxBits)
        {
        	xEventGroupClearBits(network_event_group, TN_EVNT_BIT_CONNECT);
        	esp_wifi_connect();
        }

    }

}

static esp_err_t setDefaultWifiConfig(wifi_config_t* wifi_config)
{
    memset(wifi_config, 0, sizeof (wifi_config_t));

    COMPILER_ASSERT(sizeof DEFAULT_SSID < sizeof wifi_config->sta.ssid);
    strcpy((char *)wifi_config->sta.ssid, DEFAULT_SSID);

    COMPILER_ASSERT(sizeof DEFAULT_PWD < sizeof wifi_config->sta.password);
    strcpy((char *)wifi_config->sta.password, DEFAULT_PWD);

    wifi_config->sta.scan_method = DEFAULT_SCAN_METHOD;
    wifi_config->sta.sort_method = DEFAULT_SORT_METHOD;
    wifi_config->sta.threshold.rssi = DEFAULT_RSSI;
    wifi_config->sta.threshold.authmode = DEFAULT_AUTHMODE;

    return ESP_OK;
}


static bool checkSystemTime(void)
{
    // Assumes nvs flash setup
    // Wifi is already connected
    // sntp has been initialized - operating mode, server name, sntp_init()

    if (false == timeStatus.isSntpInitialized)
    {
        ESP_LOGE(TAG, "sntp not initialized when try to check system time");
        return false;
    }

    time_t now = 0;
    struct tm timeinfo = { 0 };

    // check if system time is valid
    time(&now);
    localtime_r(&now, &timeinfo);
    if (timeinfo.tm_year < (2017 - 1900))
    {
        timeStatus.isSystemTimeSet = false;
        ESP_LOGI(TAG, "system time: NOT set");
        return false;
    }
    else
    {
        char strftime_buf[sizeof "2011-10-08T07:07:09Z"];
        strftime(strftime_buf, sizeof strftime_buf, "%FT%TZ", &timeinfo);
        ESP_LOGI(TAG, "system time is set. The current date/time is: %s", strftime_buf);
        timeStatus.isSystemTimeSet = true;
        return true;
    }
} // checkSystemTime

static wifi_scan_status_t start_scan(void)
{
    bool async = true;
    bool showHidden = false;
    bool passiveScan = false;
    uint32_t maxScanPerChan = 500;
    wifi_scan_status_t cmdRet;
    cmdRet = WS_scanNetworks(async, showHidden, passiveScan, maxScanPerChan);
    if (WIFI_SCAN_RUNNING == cmdRet)
    {
        ESP_LOGI(TAG, "start_scan:running:");
        return 0;
    }
    else if (WIFI_SCAN_FINISHED == cmdRet)
    {
        ESP_LOGI(TAG, "start_scan:finished");
        return 0;
    }
    else
    {
        ESP_LOGW(TAG, "start_scan:error");
        return -1;
    }
} // start_scan

void vTimerAutoConnectCallback( TimerHandle_t xTimerH )
{
	if(wifi_status == WIFI_STATUS_DISCONNECTED && scan_running == false)
	{
		xEventGroupSetBits(network_event_group, TN_EVNT_BIT_CONNECT);
	}
}

void vTimerAutoRescanCallback( TimerHandle_t xTimerH )
{
	task_network_sendStartScanEvent();
} // vTimerAutoRescanCallback

void vTimerSystemTimeCallback( TimerHandle_t xTimerH )
{
     xEventGroupSetBits(network_event_group, TN_EVNT_BIT_CHECK_SYSTEM_TIME);
} // vTimerSystemTimeCallback

