#ifndef __UGUI_CONFIG_TYPES_H
#define __UGUI_CONFIG_TYPES_H

#include <stdint.h>

/* Specify platform-dependent integer types here */

#define __UG_FONT_DATA const
typedef uint8_t      UG_U8;
typedef int8_t       UG_S8;
typedef uint16_t     UG_U16;
typedef int16_t      UG_S16;
typedef uint32_t     UG_U32;
typedef int32_t      UG_S32;

typedef enum
{
	UGUI_SYSTEM_EVENT_TICK,
	UGUI_SYSTEM_EVENT_WIFI_SCAN_DONE,
	UGUI_SYSTEM_EVENT_WIFI_CONNECT_SUCCESS,
	UGUI_SYSTEM_EVENT_WIFI_CONNECT_FAIL,

}ugui_system_event_t;


/* Enable color mode */
#define USE_COLOR_RGB888   // RGB = 0xFF,0xFF,0xFF

typedef void (*WindowUpdateFp)( ugui_system_event_t );




#endif
