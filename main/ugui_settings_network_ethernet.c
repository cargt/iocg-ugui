#include "ugui_home.h"
#include "ugui_settings_network.h"

#include "ugui_settings_network_ethernet.h"

#define MAX_OBJECTS        10

/* Window 1 */
static UG_WINDOW window_1;
static UG_OBJECT obj_buff_wnd_1[MAX_OBJECTS];
static UG_TEXTBOX tb_header;

//buttons
#define BUTTON_ID_HOME 				BTN_ID_0
#define BUTTON_ID_BACK 				BTN_ID_1
#define BUTTON_ID_NETWORK 			BTN_ID_2
#define BUTTON_ID_TCPIP				BTN_ID_3

static UG_BUTTON button_home;
static UG_BUTTON button_back;
static UG_BUTTON button_network;
static UG_BUTTON button_tcpip;


static void touch_callback( UG_MESSAGE* msg );
static void window_update( ugui_system_event_t evt );

UG_RESULT UG_SettingsNetworkEthernetCreate()
{
	/* Create Window 1 */
	UG_WindowCreate( &window_1, obj_buff_wnd_1, MAX_OBJECTS, touch_callback );
	UG_WindowSetForeColor( &window_1,SETTINGS_WINDOW_FORECOLOR );
	UG_WindowSetBackColor( &window_1,SETTINGS_WINDOW_BACKCOLOR );
	UG_WindowSetStyle( &window_1, SETTINGS_WINDOW_STYLE);

	// text box
	UG_TextboxCreate( &window_1, &tb_header,TXB_ID_0,SETTINGS_TXB_XS,SETTINGS_TXB_YS,SETTINGS_TXB_XE,SETTINGS_TXB_YE);
	UG_TextboxSetFont( &window_1, TXB_ID_0, &SETTINGS_TXB_FONT );
	UG_TextboxSetForeColor( &window_1, TXB_ID_0, SETTINGS_TXB_FORECOLOR );
	UG_TextboxSetBackColor( &window_1, TXB_ID_0, SETTINGS_TXB_BACKCOLOR );
	UG_TextboxSetAlignment( &window_1, TXB_ID_0, SETTINGS_TXB_ALIGNMENT );
	UG_TextboxSetText( &window_1, TXB_ID_0, "Ethernet\nSettings" );

	//home
	UG_ButtonCreate( &window_1, &button_home, BUTTON_ID_HOME,  SETTINGS_BUTTON_HOME_XS,SETTINGS_BUTTON_HOME_YS,SETTINGS_BUTTON_HOME_XE,SETTINGS_BUTTON_HOME_YE);
	UG_ButtonSetFont( &window_1, 		BUTTON_ID_HOME, &SETTINGS_BUTTON_FONT );
	UG_ButtonSetForeColor( &window_1, 	BUTTON_ID_HOME, SETTINGS_BUTTON_FORECOLOR );
	UG_ButtonSetBackColor( &window_1, 	BUTTON_ID_HOME, SETTINGS_BUTTON_BACKCOLOR );
	UG_ButtonSetText( &window_1, 		BUTTON_ID_HOME, "Home" );
	UG_ButtonSetStyle( &window_1, 		BUTTON_ID_HOME, SETTINGS_BUTTON_STYLE);

	//back
	UG_ButtonCreate( &window_1, &button_back, BUTTON_ID_BACK, SETTINGS_BUTTON_BACK_XS,SETTINGS_BUTTON_BACK_YS,SETTINGS_BUTTON_BACK_XE,SETTINGS_BUTTON_BACK_YE);
	UG_ButtonSetFont( &window_1, 		BUTTON_ID_BACK, &SETTINGS_BUTTON_FONT );
	UG_ButtonSetForeColor( &window_1, 	BUTTON_ID_BACK, SETTINGS_BUTTON_FORECOLOR );
	UG_ButtonSetBackColor( &window_1, 	BUTTON_ID_BACK, SETTINGS_BUTTON_BACKCOLOR );
	UG_ButtonSetText( &window_1, 		BUTTON_ID_BACK, "Back" );
	UG_ButtonSetStyle( &window_1, 		BUTTON_ID_BACK, SETTINGS_BUTTON_STYLE);

	//ethernet
	UG_ButtonCreate( &window_1, &button_network, BUTTON_ID_NETWORK, SETTINGS_BUTTON_1_XS,SETTINGS_BUTTON_1_YS,SETTINGS_BUTTON_1_XE,SETTINGS_BUTTON_1_YE);
	UG_ButtonSetFont( &window_1, 		BUTTON_ID_NETWORK, &SETTINGS_BUTTON_FONT );
	UG_ButtonSetForeColor( &window_1, 	BUTTON_ID_NETWORK, SETTINGS_BUTTON_FORECOLOR );
	UG_ButtonSetBackColor( &window_1, 	BUTTON_ID_NETWORK, SETTINGS_BUTTON_BACKCOLOR );
	UG_ButtonSetText( &window_1, 		BUTTON_ID_NETWORK, "Network" );
	UG_ButtonSetStyle( &window_1, 		BUTTON_ID_NETWORK, SETTINGS_BUTTON_STYLE);

	//Wi-Fi
	UG_ButtonCreate( &window_1, &button_tcpip, BUTTON_ID_TCPIP, SETTINGS_BUTTON_2_XS,SETTINGS_BUTTON_2_YS,SETTINGS_BUTTON_2_XE,SETTINGS_BUTTON_2_YE);
	UG_ButtonSetFont( &window_1, 		BUTTON_ID_TCPIP, &SETTINGS_BUTTON_FONT );
	UG_ButtonSetForeColor( &window_1, 	BUTTON_ID_TCPIP, SETTINGS_BUTTON_FORECOLOR );
	UG_ButtonSetBackColor( &window_1, 	BUTTON_ID_TCPIP, SETTINGS_BUTTON_BACKCOLOR );
	UG_ButtonSetText( &window_1, 		BUTTON_ID_TCPIP, "Advanced" );
	UG_ButtonSetStyle( &window_1, 		BUTTON_ID_TCPIP, SETTINGS_BUTTON_STYLE);

	//button 3
//	UG_ButtonCreate( &window_1, &button_about, BUTTON_ID_ABOUT, SETTINGS_BUTTON_3_XS,SETTINGS_BUTTON_3_YS,SETTINGS_BUTTON_3_XE,SETTINGS_BUTTON_3_YE);
//	UG_ButtonSetFont( &window_1, 		BUTTON_ID_ABOUT, &SETTINGS_BUTTON_FONT );
//	UG_ButtonSetForeColor( &window_1, 	BUTTON_ID_ABOUT, SETTINGS_BUTTON_FORECOLOR );
//	UG_ButtonSetBackColor( &window_1, 	BUTTON_ID_ABOUT, SETTINGS_BUTTON_BACKCOLOR );
//	UG_ButtonSetText( &window_1, 		BUTTON_ID_ABOUT, "Button 3" );
//	UG_ButtonSetStyle( &window_1, 		BUTTON_ID_ABOUT, SETTINGS_BUTTON_STYLE);

	return UG_RESULT_OK;
}

UG_RESULT UG_SettingsNetworkEthernetShow()
{
	return UG_WindowShow( &window_1, window_update );
}

static void window_update( ugui_system_event_t evt )
{
	switch(evt)
	{
	case UGUI_SYSTEM_EVENT_TICK:
//		ugui_icon_wifi_update(&window_1,IMG_ID_WIFI);
//		ugui_icon_battery_update(&window_1,IMG_ID_BATTERY);
		break;
	case UGUI_SYSTEM_EVENT_WIFI_SCAN_DONE:
//		UG_SettingsNetworkWifiSsidUpdate();
		break;
	default:
		break;
	}
}


UG_RESULT UG_SettingsNetworkEthernetHide()
{
	return UG_WindowHide( &window_1 );
}

static void touch_callback( UG_MESSAGE* msg )
{
	if ( msg->type == MSG_TYPE_OBJECT )
	{
		if ( msg->id == OBJ_TYPE_BUTTON && msg->event == OBJ_EVENT_RELEASED )
		{
			switch(msg->sub_id)
			{
			case BUTTON_ID_HOME:
				UG_HomeShow();
				break;
			case BUTTON_ID_BACK:
				UG_SettingsNetworkShow();
				break;
			case BUTTON_ID_NETWORK:
				break;
			case BUTTON_ID_TCPIP:
				break;
			}

		}
	}

}

