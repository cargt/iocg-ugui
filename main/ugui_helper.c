/*
 * ugui_helper.c
 *
 *  Created on: Feb 21, 2018
 *      Author: mark
 */

#include "esp_log.h"
#include "ugui.h"
#include "ugui_helper.h"
#include "ugui_home.h"
#include "ugui_keyboard.h"
#include "ugui_msg.h"

#include "string.h"
#include "esp_wifi.h"

static void ugui_emailKeyboard2(int code);
static void email_keyboard_callback();
static void email_password_keyboard_callback();

void ugui_icon_wifi_update(UG_WINDOW* window_1, int id)
{
	//wifi rssi
	wifi_ap_record_t wifidata;
	if( esp_wifi_sta_get_ap_info(&wifidata)==0)
	{
		//100% = -50rssi
		//0% = -100rssi
		if(wifidata.rssi < -100)
			UG_ImageSetBMP( window_1, id, &img_wifi_0 );
		else if(wifidata.rssi < -80)
			UG_ImageSetBMP( window_1, id, &img_wifi_33 );
		else if(wifidata.rssi < -60)
			UG_ImageSetBMP( window_1, id, &img_wifi_66 );
		else
			UG_ImageSetBMP( window_1, id, &img_wifi_100 );
	}
	else
	{
		UG_ImageSetBMP( window_1, id, &img_wifi_none );
	}
}

void ugui_icon_battery_update(UG_WINDOW* window_1, int id)
{
#define BATT_NUM_OF_THRESHOLDS 5
#define BATT_HYSTERESIS 0.050
	typedef struct
	{
		float threshold;
		UG_BMP* icon;
	}battery_ranges_t;
	static const battery_ranges_t bat_thresholds[BATT_NUM_OF_THRESHOLDS+2] =
	{
			{ .threshold = 5.000, .icon = &img_batt_100 },	// > 100%
			{ .threshold = 3.956, .icon = &img_batt_100 },	// 100
			{ .threshold = 3.835, .icon = &img_batt_75 },	// 75%
			{ .threshold = 3.750, .icon = &img_batt_50 },	// 50%
			{ .threshold = 3.693, .icon = &img_batt_25 },	// 25%
			{ .threshold = 0.000, .icon = &img_batt_0 },		// 0%
			{ .threshold = -1.000, .icon = &img_batt_0 },	// < 0%
	};

	float bat_v = 3.8; //todo read battery
	int icon_index = 0;
	static int icon_index_current = 0;


/*	if(task_main_battery_charge_status() > 0)
	{
		UG_ImageSetBMP( window_1, id, &img_batt_charging );
	}
	else*/
	{
		for(int i=1; i<BATT_NUM_OF_THRESHOLDS+1; i++)
		{
			if( bat_v > bat_thresholds[i].threshold )
			{
				icon_index = i;
				break;
			}
		}


		if( icon_index_current == 0 )				//first time
		{
			icon_index_current = icon_index;
		}
		else if( icon_index < icon_index_current )	//higher voltage
		{
			if( bat_v > (bat_thresholds[icon_index_current].threshold + BATT_HYSTERESIS) )
			{
				icon_index_current = icon_index;
			}
		}
		else if( icon_index > icon_index_current )	//lower voltage
		{
			if( bat_v < (bat_thresholds[icon_index_current].threshold - BATT_HYSTERESIS) )
			{
				icon_index_current = icon_index;
			}
		}

		UG_ImageSetBMP( window_1, id, bat_thresholds[icon_index_current].icon );
	}


}



