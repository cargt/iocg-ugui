/*
 * msg_frame.h
 *
 *  Created on: Sep 29, 2018
 *      Author: Graham
 */

#ifndef MAIN_MSG_FRAME_H_
#define MAIN_MSG_FRAME_H_

#include <stdint.h>

#include "CAN.h"



typedef struct {
    uint32_t    timestamp;
    uint32_t    pktCount;
    CAN_frame_t can_frame;
} msg_frame_t;

#endif /* MAIN_MSG_FRAME_H_ */
