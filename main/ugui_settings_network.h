#include "ugui.h"
#include "ugui_config.h"

#ifndef __UGUI_SETTINGS_NETWORK_H
#define __UGUI_SETTINGS_NETWORK_H

UG_RESULT UG_SettingsNetworkCreate();
UG_RESULT UG_SettingsNetworkShow();
UG_RESULT UG_SettingsNetworkHide();

#endif //#ifndef __UGUI_SETTINGS_NETWORK_H
