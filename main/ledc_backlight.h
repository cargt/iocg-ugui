/*
 * ledc_backlight.h
 *
 *  Created on: Jan 3, 2018
 *      Author: Graham
 */

#ifndef MAIN_LEDC_BACKLIGHT_H_
#define MAIN_LEDC_BACKLIGHT_H_

#include <stdint.h>

void ledc_init(uint8_t gpioNum);

void ledc_setDutyCycle(uint8_t percent);


#endif /* MAIN_LEDC_BACKLIGHT_H_ */
