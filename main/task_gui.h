/*
 * task_gpio.h
 *
 *  Created on: Nov 15, 2017
 *      Author: cargt
 */

#ifndef MAIN_TASK_GUI_H_
#define MAIN_TASK_GUI_H_

#include "sdkconfig.h"

#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "esp_event.h"

#include "tft_config.h"

#define TASK_GUI_PORTRAIT 			0
#define TASK_GUI_LANDSCAPE 			1
#define TASK_GUI_PORTRAIT_FLIPPED	2
#define TASK_GUI_LANDSCAPE_FLIPPED 	3

#define TASK_GUI_DISPLAY_WIDTH 320
#define TASK_GUI_DISPLAY_HEIGHT 240
#define TASK_GUI_ORIENTATION TASK_GUI_LANDSCAPE

#if (TASK_GUI_PORTRAIT == TASK_GUI_ORIENTATION) || (TASK_GUI_PORTRAIT_FLIPPED == TASK_GUI_ORIENTATION)
	#define WIDTH  TASK_GUI_DISPLAY_HEIGHT
	#define HEIGHT TASK_GUI_DISPLAY_WIDTH
#elif TASK_GUI_LANDSCAPE == TASK_GUI_ORIENTATION || TASK_GUI_LANDSCAPE_FLIPPED == TASK_GUI_ORIENTATION
	#define WIDTH  TASK_GUI_DISPLAY_WIDTH
	#define HEIGHT TASK_GUI_DISPLAY_HEIGHT
#endif


// ==================================================
// Define which spi bus to use VSPI_HOST or HSPI_HOST
#define SPI_BUS HSPI_HOST
// ==================================================

enum {
    // Status bits stay set for as long as the condition is active
    TG_STATUS_UNUSED_BIT = BIT0
};

/* NOTE: The event bits should not be used by external functions
 * This is static use only.
 * However, they are defined here for visibility.
 * For defensive programming make status and event bits have different values.
 * This will highlight errors quicker if the wrong group is accidentally used.
 * Event groups are up to 24bits
 */
enum {
    // Event bits are set until the event has been acted on
    TG_EVNT_BIT_GUI_TICK        		= BIT8,
    TG_EVNT_BIT_SCAN_COMPLETE   		= BIT9,
	TG_EVNT_BIT_WIFI_CONNECT_SUCCESS 	= BIT10,
	TG_EVNT_BIT_WIFI_CONNECT_FAIL 		= BIT11,
};


//EventGroupHandle_t gui_status_group;

/******************************************************************************
 * gui_event_handler
 * This event handler is to be called by the central event handler
 * There can only be ONE system event handler
 */
esp_err_t gui_event_handler(void *ctx, system_event_t *event);

int task_gui_init(void);
void task_gui_init_early(void);


#endif /* MAIN_TASK_GUI_H_ */
