#ifndef __UGUI_KEYBOARD_H
#define __UGUI_KEYBOARD_H

#include "ugui.h"

typedef enum
{
	UGUI_KEYBOARD_KEYS_STD,
	UGUI_KEYBOARD_KEYS_EMAIL,
}ugui_keyboard_keys_t;

#define KB_TYPE_QWERTY_LOWER	0
#define KB_TYPE_QWERTY_UPPER	1
#define KB_TYPE_NUMBERS			2
#define KB_TYPE_NUMBERS_2		3

typedef void (*keyboard_callback)(void);


/* Keyboard functions */
UG_RESULT UG_KeyboardCreate();
void UG_KeyboardShow(ugui_keyboard_keys_t keyboard, char* init_string, char* ret_string, UG_U32 stringlen, keyboard_callback callback );

#endif //#ifndef __UGUI_KEYBOARD_H
