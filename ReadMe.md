# Cargt uGUI Demo for ESP32 Touchscreen
This is a quick demo with a home screen and basic settings to get you connected to Wi-Fi.

This UI is based on an open source project called uGUI (http://embeddedlightning.com/ugui/).

##Build Info (Linux)
###Get Source
Compiler and ESP-IDF are included.
```
$ git clone https://bitbucket.org/cargt/ugui-demo.git
$ cd ugui-demo
$ git submodule update --init --recursive
```
###Build Option 1: Command Line
1. Open new terminal and change directory to ugui-demo
2. Run environment script provided in root folder 

```
  $ source environment-setup
```
3. Build using make 

```
  $ make
  $ make flash
  $ make monitor
```
###Build Option 2: Eclipse
The repo contains an eclipse project.  The project is setup to build and flash the device, but will not monitor.

---

##Demo Screens
The main screen: Shows a header with status icons and settings.  The large button in the middle shows a button with a background image and 3 strings with different fonts and font alignments.

![alt text](https://bitbucket.org/cargt/ugui-demo/raw/56406706a52c076ffb003ab0b2439d062675357a/doc/images/main.png "Main")

![alt text](https://bitbucket.org/cargt/ugui-demo/raw/56406706a52c076ffb003ab0b2439d062675357a/doc/images/settings.png)

![alt text](https://bitbucket.org/cargt/ugui-demo/raw/56406706a52c076ffb003ab0b2439d062675357a/doc/images/wifi.png)

![alt text](https://bitbucket.org/cargt/ugui-demo/raw/56406706a52c076ffb003ab0b2439d062675357a/doc/images/keyboard.png)

![alt text](https://bitbucket.org/cargt/ugui-demo/raw/56406706a52c076ffb003ab0b2439d062675357a/doc/images/wifi-info.png)

![alt text](https://bitbucket.org/cargt/ugui-demo/raw/56406706a52c076ffb003ab0b2439d062675357a/doc/images/msg.png)


###Webserver
A demo webserver can be accessed using a webbrowser after connecting to the network.  The device's ip address can be found in the settings menu->WiFi->Info

The demo shows how to 

1. scan and connect to WiFi networks
2. Push firmware updates
3. View and Capture the screen via websocket

---
##Editing the UI
###Add/Edit Fonts
I modified a project used to create bitmap fonts for use in GPU textures.  The modfied version with instructions can be found here https://bitbucket.org/cargt/ubfg

To create a font file that can be used in this project you need to 

1. Set "Output Format" to "C Code"
2. The output file will be named 
```
  fnt_(the font you used)(size)pt.c 
  i.e. fnt_opensanssemibold8pt.c
```
3. Copy the c file to the ugui-demo/main folder
4. Enable the font and add an extern var in ugui_config.h
```cpp
  #define USE_FNT_OPENSANSSEMIBOLD8PT
  extern const UG_FONT FNT_OPENSANSSEMIBOLD8PT;
```

###Add images
Images are located in ugui-demo/main/images

A python script is provide to convert png files to a single source and header file.  The script uses ImageMagick's convert tool to export the raw data from the png to rgba then xxd to convert to c file.

```
You will need to install ImageMagick. 

You must tell it to install convert !

https://imagemagick.org/script/download.php
```

1. Copy your png files to ugui-demo/main/images
2. run python script
'''
python png2c.py
'''
3. review output files images.c and images.h
4. Do not move this files, the makefiles expect them in main/images


###Add pages
1. To add a page, copy and existing one.  Both source and header.
2. Include header in task_gui.c
3. Call your new create function in task_gui.c
4. Call your new show function to view page


